import sys

from PyQt5 import QtWidgets, QtCore, QtGui

from static import local_paths
from window_handlers.window_manager import MainWindow
version = "1.4.1"

# Main launcher window that launches the GUI
# The user can choose to directly launch OR add extra custom network files and then launch


class LauncherWindow(QtWidgets.QMainWindow):
    def __init__(self):
        # basic QT window setup
        super(LauncherWindow, self).__init__()
        self.setWindowTitle("GraphNet Launcher v" + version)
        self.graphnet = None

        self.launch_button = QtWidgets.QPushButton("Launch graphnet")
        self.launch_button.clicked.connect(self.launch)

        # add widgets for launch options and buttons.
        self.network_label = QtWidgets.QLabel("edit path for: network.pickle")
        self.routing_label = QtWidgets.QLabel("edit path for: routing_tables.pickle")
        self.conflicts_label = QtWidgets.QLabel("edit path for: conflicts.pickle")
        self.network_textbox = QtWidgets.QLineEdit("pickles/network.pickle")
        self.routing_textbox = QtWidgets.QLineEdit("pickles/routing_tables.pickle")
        self.conflicts_textbox = QtWidgets.QLineEdit("pickles/conflicts.pickle")
        self.network_file_browser = QtWidgets.QPushButton()
        self.routing_file_browser = QtWidgets.QPushButton()
        self.conflicts_file_browser = QtWidgets.QPushButton()

        # browse button to select file
        style = QtWidgets.QApplication.instance().style()
        self.network_file_browser.setIcon(style.standardIcon(QtWidgets.QStyle.SP_DialogOpenButton))
        self.routing_file_browser.setIcon(style.standardIcon(QtWidgets.QStyle.SP_DialogOpenButton))
        self.conflicts_file_browser.setIcon(style.standardIcon(QtWidgets.QStyle.SP_DialogOpenButton))

        self.network_file_browser.clicked.connect(self.open_browser_network)
        self.routing_file_browser.clicked.connect(self.open_browser_routing)
        self.conflicts_file_browser.clicked.connect(self.open_browser_conflicts)

        self.network_file_layout = QtWidgets.QHBoxLayout()
        self.routing_file_layout = QtWidgets.QHBoxLayout()
        self.conflicts_file_layout = QtWidgets.QHBoxLayout()

        self.network_file_layout.addWidget(self.network_textbox)
        self.network_file_layout.addWidget(self.network_file_browser)
        self.routing_file_layout.addWidget(self.routing_textbox)
        self.routing_file_layout.addWidget(self.routing_file_browser)
        self.conflicts_file_layout.addWidget(self.conflicts_textbox)
        self.conflicts_file_layout.addWidget(self.conflicts_file_browser)

        self.save_paths_button = QtWidgets.QPushButton("Set Paths and Launch")
        self.back_button = QtWidgets.QPushButton("Back")

        # connect buttons to functions
        self.save_paths_button.clicked.connect(self.save_and_launch)
        self.back_button.clicked.connect(self.reset)

        self.set_files_button = QtWidgets.QPushButton("Select Network Files")
        self.set_files_button.clicked.connect(self.set_files)

        self.setMinimumWidth(500)
        self.setMaximumHeight(70)

        self.central_widget = QtWidgets.QWidget()
        self.setCentralWidget(self.central_widget)

        # add widgets to the screen
        self.main_layout = QtWidgets.QVBoxLayout()
        self.main_layout.addWidget(self.launch_button)
        self.main_layout.addWidget(self.set_files_button)
        self.main_layout.setAlignment(QtCore.Qt.AlignTop)

        self.central_widget.setLayout(self.main_layout)

        self.show()

    # open a file explorer window to select a file
    def open_browser_network(self):
        file_ = QtWidgets.QFileDialog.getOpenFileName(None, 'Select a File (network):')
        self.set_path(file_[0], self.network_textbox)

    # open a file explorer window to select a file
    def open_browser_routing(self):
        file_ = QtWidgets.QFileDialog.getOpenFileName(None, 'Select a File (routing):')
        self.set_path(file_[0], self.routing_textbox)

    # open a file explorer window to select a file
    def open_browser_conflicts(self):
        file_ = QtWidgets.QFileDialog.getOpenFileName(None, 'Select a File (conflicts):')
        self.set_path(file_[0], self.conflicts_textbox)

    # set path after getting a file path
    def set_path(self, txt, box):
        if txt.endswith(".pickle"):
            box.setText(txt)
        else:
            s = QtWidgets.QMessageBox()
            s.setIcon(QtWidgets.QMessageBox.Warning)
            s.setText("*.pickle files only")
            s.setStandardButtons(QtWidgets.QMessageBox.Ok)
            s.exec_()

    # if the user chooses to launch with custom files, this function will edit the file paths and then
    # call for the launch
    def set_files(self):
        self.main_layout.removeWidget(self.launch_button)
        # self.main_layout.removeWidget(self.set_files_button)
        self.main_layout.addWidget(self.network_label)
        self.main_layout.addLayout(self.network_file_layout)

        self.main_layout.addWidget(self.routing_label)
        self.main_layout.addLayout(self.routing_file_layout)

        self.main_layout.addWidget(self.conflicts_label)
        self.main_layout.addLayout(self.conflicts_file_layout)
        self.main_layout.addWidget(self.save_paths_button)
        self.main_layout.addWidget(self.back_button)

    # just re launch the window if the user presses back
    def reset(self):
        self.close()
        self.__init__()

    # save edited paths
    def save_and_launch(self):
        local_paths.network_path = self.network_textbox.text()
        local_paths.routing_tables_path = self.routing_textbox.text()
        local_paths.conflicts_path = self.conflicts_textbox.text()
        self.launch()

    def launch(self):
        self.graphnet = MainWindow()
        self.graphnet.setWindowState(QtCore.Qt.WindowMaximized)
        self.graphnet.show()
        self.close()


def init():
    app = QtWidgets.QApplication(sys.argv)

    # set custom font
    fd = QtGui.QFontDatabase()
    id1 = fd.addApplicationFont('assets/fonts/Roboto-Regular.ttf')
    fstr = QtGui.QFontDatabase.applicationFontFamilies(id1)
    app.setFont(QtGui.QFont(fstr[0]))

    app_icon = QtGui.QIcon()
    app_icon.addFile('assets/app_icons/ico@0.25x.png', QtCore.QSize(128, 128))
    app_icon.addFile('assets/app_icons/ico@0.06x.png', QtCore.QSize(32, 32))
    app_icon.addFile('assets/app_icons/ico@0.5x.png', QtCore.QSize(256, 256))
    app.setWindowIcon(app_icon)

    main = LauncherWindow()
    sys.exit(app.exec())
