import datetime
from PyQt5 import QtWidgets, QtGui, QtCore
import time

from static import local_paths
from core.conflicts_manager import ConflictsManager
from core.route_manager import RouteManager
from network_objects.network_item import NetworkItem
# from network_link import NetworkLink DISCARDED IDEA REFACTORED TO NetworkConnection
from core.network_manager import NetworkManager
from static.network_item_types import NetworkItemTypes
from network_objects.network_connection import NetworkConnection
from custom_widgets.widgets import CQGraphicsView, CQCheckBox, CQPushButton, CQToolBar, CQPopUpSettings, \
    AppSettings, ConflictsManagerWindow, RouteManagerWindow, HelpWindow
from custom_widgets.effects import CQDropShadow
from static.notification_types import NotificationTypes

from core.de_pickler import UnPickleNetwork


class Animator:
    # this class animates the notification errors, makes it less abrupt
    # other buttons and stuff were not animated.
    def __init__(self, widget):
        self.widget = widget
        self.effect = QtWidgets.QGraphicsOpacityEffect()
        self.animation = QtCore.QPropertyAnimation(self.effect, b"opacity")
        self.widget.setGraphicsEffect(self.effect)

    def fadeout(self):
        self.animation.setDuration(150)
        self.animation.setStartValue(1)
        self.animation.setEndValue(0)
        self.animation.start()

    def fadein(self):
        self.animation.setDuration(150)
        self.animation.setStartValue(0)
        self.animation.setEndValue(1)
        self.animation.start()


class QErrorNotification(QtWidgets.QLabel):
    # uses a QTimer thread to 'flash' a notification and fade it out after x milliseconds.
    # user can input the notification type, message and duration (x)
    dismissError = QtCore.pyqtSignal(int, name="displayChange")

    # create timer and set colors based on type
    def __init__(self, msg, type, duration):
        self.timer = QtCore.QTimer()
        self.show = True
        self.duration = duration
        super(QErrorNotification, self).__init__(msg)
        q = CQDropShadow()
        self.setVisible(False)
        self.setGraphicsEffect(q)
        self.append = 'background: rgba(140, 140, 140, 0.6);'
        if type == NotificationTypes.ERROR:
            self.append = 'background: rgba(214, 69, 73, 0.6);'
        elif type == NotificationTypes.INFO:
            self.append = 'background: rgba(140, 140, 140, 0.6);'
        elif type == NotificationTypes.WARNING:
            self.append = 'background: rgba(255, 224, 102, 0.6);'
        self.setStyleSheet(
            self.append + ' opacity: 50%; font-size: 15px; border-radius: 5px;')
        self.setContentsMargins(10, 7, 10, 7)
        self.setText(msg)
        self.animator = Animator(self)
        self.timer.timeout.connect(self.hide)

    # double click to dismiss
    def mouseDoubleClickEvent(self, a0: QtGui.QMouseEvent) -> None:
        self.fasthide()

    def unhide(self):
        self.setVisible(True)
        self.animator.fadein()
        self.timer.start(self.duration)

    def fasthide(self):
        self.show = False
        self.setVisible(False)
        self.timer.stop()
        self.dismissError.emit(1)

    def hide(self):
        self.show = False
        self.animator.fadeout()
        self.timer.stop()
        self.dismissError.emit(1)


class SpacerWidget(QtWidgets.QLabel):
    # small line that can be seen on the toolbar
    # no logical significance, just helps visually separate tools
    def __init__(self, color):
        super(SpacerWidget, self).__init__()
        self.setStyleSheet('height: 1px; border-top: 1px solid ' + color)
        self.setFixedHeight(1)


class MainWindow(QtWidgets.QWidget):
    # main class that runs the GUI
    # contains *most* of the logic that runs the UI
    # drawing and other calculations happen in other classes
    def __init__(self):
        super().__init__()
        self.currentConflict = None
        self.viewMode = 0
        self.live_hcas = []
        self.marked = []
        self.hca_set_is_live = False
        self.disableClearHCAs = False
        self.conflictsManagerOpen = False
        self.disableSelections = False
        st = time.time()
        # init graphics scene
        self.viewButtonGroup = QtWidgets.QButtonGroup()
        self.scene = QtWidgets.QGraphicsScene()
        self.graphicsView = CQGraphicsView()
        self.graphicsView.setRenderHint(QtGui.QPainter.Antialiasing)
        self.settings = AppSettings()
        self.helpDialog = HelpWindow(self)

        # signal - graphicView
        self.graphicsView.jumpSignal.connect(self.jump_to_current_selection)

        # routing elements
        self.routeManagerLayout = QtWidgets.QVBoxLayout()
        self.proxyLabels = []
        self.infoSourceLabel = QtWidgets.QLabel("Source")
        self.infoDestinationLabel = QtWidgets.QLabel("Destination")
        self.sourceLabel = QtWidgets.QLabel("0 Routing Selections")
        self.desintationLabel = QtWidgets.QLabel("now select a destination")
        self.setButtonsLayout = QtWidgets.QHBoxLayout()
        self.setSourceButton = CQPushButton("Set SRC", 0)
        self.setButtonsLayout.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom)
        self.setDestinationButton = CQPushButton("Set DST", 0)
        self.startButton = CQPushButton("Start Routing", 0)
        self.routeManager = RouteManager()
        self.desintationLabel.setFixedWidth(300)
        self.setSourceButton.setFixedWidth(100)
        self.setDestinationButton.setFixedWidth(100)
        self.startButton.setFixedWidth(110)

        # connections
        self.routeManagerWindow = RouteManagerWindow(self)
        self.routeManagerWindow.routeSelected.connect(self.route_list_selected)
        self.routeManagerWindow.clearScreen.connect(self.route_manager_clear_screen)
        self.routeManagerWindow.routeAdded.connect(self.remove_hcas)
        self.routeManagerWindow.listentingForSelection.connect(self.router_manager_listening)

        self.setSourceButton.clicked.connect(self.set_route_source)
        self.setDestinationButton.clicked.connect(self.set_route_destination)
        self.startButton.clicked.connect(self.start_routing)

        self.routingDoneDialog = QtWidgets.QMessageBox()
        self.routingDoneDialog.setIcon(QtWidgets.QMessageBox.Information)
        self.routingDoneDialog.setText("Routing Complete")
        self.routingDoneDialog.setWindowTitle("Routing Status")
        self.routingDoneDialog.setStandardButtons(QtWidgets.QMessageBox.Ok)

        # toolbar actions
        self.topToolBarActionGroup = QtWidgets.QActionGroup(self.graphicsView)
        self.panTool = QtWidgets.QAction(self.topToolBarActionGroup)
        self.zoomTool = QtWidgets.QAction(self.topToolBarActionGroup)
        self.selectionTool = QtWidgets.QAction(self.topToolBarActionGroup)
        self.hcaSelectTool = QtWidgets.QAction()

        self.deselectTool = QtWidgets.QToolButton()
        self.topToolSpacer = SpacerWidget("#b8b8b8")

        self.panTool.triggered.connect(self.set_pan_tool)
        self.zoomTool.triggered.connect(self.set_zoom_tool)
        self.selectionTool.triggered.connect(self.set_arrow_tool)
        self.hcaSelectTool.triggered.connect(self.set_hca_tool)

        # bottom tools
        self.settingsTool = QtWidgets.QAction()
        self.helpTool = QtWidgets.QAction()
        self.settingsDialog = CQPopUpSettings(self)
        self.settingsTool.triggered.connect(self.open_settings_dialog)

        self.helpTool.triggered.connect(self.show_help_dialog)

        # conflicts
        self.conflictsManagerDialog = ConflictsManagerWindow(self)
        self.conflictsManagerDialog.conflictManagerClosed.connect(self.close_conflict_manager)
        self.conflictsManagerDialog.routeSelected.connect(self.draw_conflict)

        # overlays
        self.infoBox = QtWidgets.QLabel("no active selections", self.graphicsView)
        self.infoBox.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse);
        self.helpBox = QtWidgets.QLabel()
        self.errorBox = QtWidgets.QLabel()
        self.topToolBar = CQToolBar()
        self.bottomToolBar = CQToolBar()
        self.topToolBar.setOrientation(QtCore.Qt.Vertical)
        self.jumpSource = CQPushButton("JumpToSource", 0)
        self.jumpDestination = CQPushButton("JumpToDestination", 0)
        self.clearHCAs = CQPushButton("Clear HCAs", 0)
        self.clearHCAs.setVisible(False)
        self.clearHCAs.clicked.connect(self.handle_deselect)
        self.jumpSource.clicked.connect(self.jump_to_source)
        self.jumpDestination.clicked.connect(self.jump_to_destination)
        self.networkView = CQCheckBox("Network", 0)
        self.routingView = CQCheckBox("Routing", 1)
        self.conflictsView = CQCheckBox("Conflicts", 2)

        self.showHelp = False

        # internals
        self.network = UnPickleNetwork()
        self.routing_tables = self.routeManager.routingTables
        self.currentRoute = None
        self.network_manager = NetworkManager()
        self.errorQueue = []
        self.logger = []
        self.network_manager.resizeScene.connect(self.auto_resize)

        # Error Queue Timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.error_collector)
        # self.timer.start(1000)

        # animations and effects (test)
        self.opacityE = QtWidgets.QGraphicsOpacityEffect(self.infoBox)
        self.opacityE.setOpacity(0.5)
        self.infoBox.setGraphicsEffect(self.opacityE)
        self.infoBox.setAutoFillBackground(True)

        # set overlay layouts
        self.globalGraphicsLayout = QtWidgets.QVBoxLayout(self.graphicsView)

        # overlay layouts #2
        self.infoBoxLayout = QtWidgets.QVBoxLayout()
        self.helpBoxLayout = QtWidgets.QVBoxLayout()
        self.viewBoxLayout = QtWidgets.QHBoxLayout()
        self.errorBoxLayout = QtWidgets.QVBoxLayout()
        self.oneLayout = QtWidgets.QHBoxLayout()
        self.zeroLayout = QtWidgets.QHBoxLayout()
        self.jumpLayout = QtWidgets.QHBoxLayout()

        self.infoBoxLayout.setContentsMargins(10, 10, 15, 10)
        self.viewBoxLayout.setContentsMargins(10, 10, 15, 10)
        self.helpBoxLayout.setContentsMargins(10, 10, 15, 10)
        self.errorBoxLayout.setContentsMargins(10, 10, 15, 10)
        self.jumpLayout.setContentsMargins(10, 10, 10, 10)
        self.oneLayout.setContentsMargins(0, 0, 30, 0)
        self.jumpLayout.setSpacing(10)

        # add stuff to te mainlayouts
        # and add nested layouts to other parent layouts
        # a few layers of nesting is needed to properly align elements
        self.mainGrid = QtWidgets.QGridLayout()

        self.mainGrid.addLayout(self.zeroLayout, 0, 0)
        self.mainGrid.addLayout(self.helpBoxLayout, 1, 0)
        self.mainGrid.addLayout(self.viewBoxLayout, 1, 1)
        self.mainGrid.addLayout(self.infoBoxLayout, 0, 1)

        self.viewBoxLayout.addLayout(self.oneLayout)
        self.helpBoxLayout.addWidget(self.bottomToolBar)

        self.jumpLayout.addWidget(self.jumpSource)
        self.jumpLayout.addWidget(self.jumpDestination)
        self.jumpLayout.addWidget(self.clearHCAs)
        self.zeroLayout.addWidget(self.topToolBar)
        self.zeroLayout.setAlignment(QtCore.Qt.AlignLeft)
        self.zeroLayout.addLayout(self.errorBoxLayout)

        self.globalGraphicsLayout.addLayout(self.mainGrid)

        self.conflicts_manager = None

        self.init_tool_bar()
        self.init_route_manager()
        self.init_route_manager_layouts()
        self.set_router_manager_visibility(False)

        self.current_selected = None
        self.current_second_selection = None

        # define dimensions
        self.window_height, self.window_width = 768, 1366
        self.graphics_height, self.graphics_width = 100, 100

        # generate sample network
        # self.sample_network()

        # construct network from parsed ibnetparsed
        self.construct_network()

        self.init_conflict_manager()

        # experimental darkmode feature, currently disabled
        # current default is darkmode, light mode to be added in a future release
        self.darkMode = True
        # // 4c4c4c
        self.defaultQss = '''
        QGraphicsView {
                background: #2b2b2b;    
            }
        '''
        self.lightQss = '''
                QGraphicsView {
                        background: #ffffff;    
                    }
                    '''
        # styling
        self.graphicsView.setStyleSheet(self.defaultQss +
                                        '''
            QToolTip {
                color: #000000;
                background: #ffffff;
                font: 10px;
            }
            '''
                                        )
        # init overlay GUIs
        self.init_info_box()
        self.init_help_box()
        self.init_view_switcher()
        self.init_error_box()

        # init mainwindow
        print("getting Network from: " + local_paths.network_path)
        print("getting Routing Tables from: " + local_paths.routing_tables_path)
        print("getting Conflicts from: " + local_paths.conflicts_path)
        self.setWindowTitle("GraphNet")
        vb = QtWidgets.QVBoxLayout()
        vb.setContentsMargins(0, 0, 0, 0)
        menu = QtWidgets.QMenuBar()
        self.init_graphics()
        vb.addWidget(self.graphicsView)
        self.update_scene_rect(1000000, 10000000)
        self.setLayout(vb)
        self.init()

        # calculate render time (currently disabled)
        et = time.time()
        print("Render Time: " + str(et - st))
        self.scene.selectionChanged.connect(self.on_state_change)
        self.graphicsView.setProperty("darkmode", True)

    ### ROUTERMANAGERWINDOW FUNCTIONS ###

    # this method is called when RouterManagerWindow's list selection is changed
    # listens for a signal
    # this then uses the new selection to draw
    def route_list_selected(self):
        if self.currentRoute:
            self.route_cleanup(self.currentRoute)
        if self.routeManagerWindow.currentSelectedRoute:
            self.currentRoute = self.routeManagerWindow.currentSelectedRoute
            self.draw_routes(self.currentRoute)

    # this method is called when the clear screen signal is emitted by RouterManagerWindow
    # these methods live in the main class because it has access to the objects on the screen
    def route_manager_clear_screen(self):
        if self.currentRoute:
            self.route_cleanup(self.currentRoute)
            self.currentRoute = None

    # NEW router manager which opens in a separate window
    def open_route_manager_window(self):
        self.hcaSelectTool.setChecked(True)
        self.disableSelections = True
        self.routeManagerWindow.set_network(self.network.network)
        self.routeManagerWindow.set_routing_tables(self.routing_tables)
        self.routeManagerWindow.show()

    # NEW router manager which opens in a separate window
    def close_route_manager_window(self):
        self.disableSelections = False
        self.routeManagerWindow.clear_screen()
        self.hcaSelectTool.setChecked(False)
        self.routeManagerWindow.close()

    # set source for the routing path
    # sets source through the graphicsview selection
    # for the routing tab
    def finish_route_source(self, src):
        self.update_routing_infobox(self.sourceLabel, src)
        self.setSourceButton.setText("src done")
        self.routeManager.sourceListening = False
        self.routeManager.setSource(src)
        self.infoSourceLabel.setVisible(True)
        self.desintationLabel.setVisible(True)

    # set source for the routing path
    # sets source through the graphicsview selection
    # for the routing tab
    def finish_route_destination(self, dst):
        self.update_routing_infobox(self.desintationLabel, dst)
        self.setDestinationButton.setText("dst done")
        self.routeManager.destinationListening = False
        self.routeManager.setDestination(dst)
        self.infoDestinationLabel.setVisible(True)

    ###################

    #### CONFLICTMANAGER FUNCTIONS ####

    # conflict manager, draws conflicts using draw_routes
    def draw_conflict(self):
        if self.currentConflict:
            self.route_cleanup(self.currentConflict)
        if self.conflictsManagerDialog.currentSelectedRoute:
            self.currentConflict = self.conflictsManagerDialog.currentSelectedRoute
            self.draw_routes(self.currentConflict)

    # pre-opening preperation for the conflicts tab
    # clear screens and push notifications
    def open_conflict_manager(self):
        self.remove_hcas()
        self.scene.clearSelection()
        self.conflictsManagerOpen = True
        self.push_notification("Conflicts Manager: Selections Disabled", NotificationTypes.WARNING, 3000)
        self.disableClearHCAs = True
        self.conflictsManagerDialog.show()
        self.conflictsManagerDialog.populate_conflicts(self.conflicts_manager)

    # post close code, to clean up after the conflicts tab is closed
    def close_conflict_manager(self):
        self.remove_hcas()
        self.networkView.click()
        self.route_cleanup(self.currentConflict) if self.currentConflict else None
        self.conflictsManagerOpen = False
        self.disableClearHCAs = False
        idx = self.viewButtonGroup.checkedId()
        if idx == 0:
            self.networkView.click()
        elif idx == 1:
            self.routingView.click()

    # start the conflcit manager
    def init_conflict_manager(self):
        self.conflicts_manager = ConflictsManager()
        self.conflicts_manager.load_node_dict(self.network_manager.node_dict)
        self.conflicts_manager.create_routes()

    ################

    #### OLD ROUTING ####

    # set source for the routing path
    # sets source through the graphicsview selection
    def set_route_source(self):
        self.hcaSelectTool.setChecked(True)
        self.setSourceButton.setText("Listening..")
        self.routeManager.sourceListening = True

    # set dest for the routing path
    # sets dest through the graphicsview selection
    def set_route_destination(self):
        self.hcaSelectTool.setChecked(True)
        self.setDestinationButton.setText("Listening..")
        self.routeManager.destinationListening = True

    # starts routing when the start button is clicked
    # this is a part of the old single route routing module
    # can be enabled in function switch_views if needed
    def start_routing(self):
        if self.routeManager.resetState:
            self.routeManager.resetState = False
            self.startButton.setText("Start Routing")
            self.routeManager.source = None
            self.routeManager.destination = None
            self.setSourceButton.setEnabled(True)
            self.setDestinationButton.setEnabled(True)
            self.init_route_manager()
            self.route_cleanup(self.routeManager)
            self.scene.clearSelection()
            self.remove_hcas()
            return
        if self.routeManager.source is None or self.routeManager.destination is None:
            self.push_notification("Routing Error: select a source and destination first", NotificationTypes.ERROR,
                                   3000)
            return
        self.routeManager.start()
        self.setSourceButton.setEnabled(False)
        self.setDestinationButton.setEnabled(False)
        self.routeManager.set_simple_route_list()
        self.remove_hcas()
        self.draw_routes(self.routeManager)
        self.startButton.setText("Reset")
        self.routeManager.resetState = True
        # print("---")
        self.routingDoneDialog.exec_()

    # this is called when the (OLD) route manager is called
    # creates a widget set for adding routes
    def init_route_manager(self):
        q = CQDropShadow()
        self.sourceLabel.setGraphicsEffect(q)
        self.sourceLabel.setStyleSheet('background: rgba(140, 140, 140, 0.8); opacity: 50%; font-size: 15px; '
                                       'border-radius: 5px; padding: 8px')
        self.desintationLabel.setGraphicsEffect(q)
        self.desintationLabel.setStyleSheet('background: rgba(140, 140, 140, 0.8); opacity: 50%; font-size: 15px; '
                                            'border-radius: 5px; padding: 8px')
        self.infoSourceLabel.setStyleSheet('font-size: 20px; color: #FFFFFF;')
        self.infoDestinationLabel.setStyleSheet('font-size: 20px; color: #FFFFFF;')
        self.sourceLabel.setText("0 routing selections")
        self.desintationLabel.setText("select a destination")
        self.setDestinationButton.setText("set dst")
        self.setSourceButton.setText("set src")

        self.infoSourceLabel.setVisible(False)
        self.infoDestinationLabel.setVisible(False)
        self.desintationLabel.setVisible(False)

    # OLD route manager layout
    def init_route_manager_layouts(self):
        self.routeManagerLayout.addWidget(self.infoSourceLabel)
        self.routeManagerLayout.addWidget(self.sourceLabel)
        self.routeManagerLayout.addWidget(self.infoDestinationLabel)
        self.routeManagerLayout.addWidget(self.desintationLabel)
        self.setButtonsLayout.addWidget(self.setSourceButton)
        self.setButtonsLayout.addWidget(self.setDestinationButton)
        self.routeManagerLayout.addLayout(self.setButtonsLayout)
        self.routeManagerLayout.addWidget(self.startButton)
        self.helpBoxLayout.addLayout(self.routeManagerLayout)
        self.helpBoxLayout.addWidget(self.bottomToolBar)

    # used to toggle route manager, the OLD one
    def set_router_manager_visibility(self, v):
        items = (self.routeManagerLayout.itemAt(i) for i in range(self.routeManagerLayout.count()))
        for w in items:
            if w is not None and w.widget() is not None:
                w.widget().setVisible(v)
        items = (self.setButtonsLayout.itemAt(i) for i in range(self.setButtonsLayout.count()))
        for w in items:
            if w is not None and w.widget() is not None:
                w.widget().setVisible(v)
        if v:
            self.infoSourceLabel.setVisible(False)
            self.infoDestinationLabel.setVisible(False)
            self.desintationLabel.setVisible(False)

    #############

    # takes a RouterManager object which has a route path array
    # uses the array to trace a route on screen
    # generate arrows and stuff, with flags
    def draw_routes(self, route_manager):
        self.disableClearHCAs = True
        h1 = self.network_manager.node_dict.get(route_manager.source.ports[0].PortGUID)
        h1.ports_list[0].mark()
        h1a = h1.ports_list[0].generate_arrows(True, simple=True)
        route_manager.proxyLabels.append(h1a)
        self.add_network_item(h1a)
        h1.setSelected(False)
        h1.mark()
        h1a, h1b = h1.annotate("Source")
        if not h1b:
            route_manager.proxyLabels.append(self.scene.addWidget(h1a))
        h2 = self.network_manager.node_dict.get(route_manager.destination.ports[0].PortGUID)
        h2.ports_list[0].mark()
        h2.setSelected(False)
        h2.mark()
        h2a = h2.ports_list[0].generate_arrows(False, simple=True)
        route_manager.proxyLabels.append(h2a)
        self.add_network_item(h2a)
        h2a, h2b = h2.annotate("Destination")
        if not h2b:
            route_manager.proxyLabels.append(self.scene.addWidget(h2a))
        route_manager.marked += [h1, h2]
        self.live_hcas = []
        self.live_hcas.append(h1)
        self.live_hcas.append(h2)
        self.add_hcas()
        route_manager.goingUp = True
        for entry in route_manager.simpleRouteList:
            m = self.network_manager.node_dict.get(entry[0])
            m.mark()
            route_manager.marked.append(m)
            conn = m.ports_list[entry[1] - 1]
            if not conn:
                route_manager = self.invert_arrows(m, route_manager)
                continue
            conn.mark()
            p = conn.generate_arrows(route_manager.goingUp, simple=False)
            self.scene.addItem(p)
            route_manager.proxyLabels.append(p)
            route_manager.marked.append(conn)
            route_manager = self.invert_arrows(m, route_manager)
        if route_manager.conflict_object:
            m = self.network_manager.node_dict.get(route_manager.conflict_object.nodeGUID)
            m.custom_mark(QtGui.QColor(77, 227, 117))

    # invert arrow direction for the routing arrows
    # these arrows are drawn with alegbra calculations and a bool flag switches the angle
    # (which inverts the direction)
    def invert_arrows(self, m, rm):
        if isinstance(m, NetworkItem):
            if m.node_type == NetworkItemTypes.ROOT_SWITCH:
                rm.goingUp = False
        return rm

    # clear current route and return to a fresh network screen
    def route_cleanup(self, route_manager):
        self.remove_hcas()
        for label in route_manager.proxyLabels:
            if label.scene():
                self.scene.removeItem(label)
        route_manager.proxyLabels = []
        for m in route_manager.marked:
            m.unmark()
        route_manager.marked = []

    # settings dialog, in progress.
    # the internals need to be completed later
    def open_settings_dialog(self):
        self.settingsDialog.show()

    # error collector disposes notification objects after the timer ends
    # the QTimer object calls this after a timeout
    def error_collector(self):
        for item in self.errorQueue:
            if not item.show:
                self.oneLayout.removeWidget(item)

    # this is called when the zoom icon is clicked
    def set_zoom_tool(self):
        self.zoomTool.setChecked(True)
        self.graphicsView.zoomMode = True
        self.graphicsView.zoom_cursor()
        self.unset_pan_tool()

    # this is called when the zoom icon is clicked
    def unset_zoom_tool(self):
        self.zoomTool.setChecked(False)
        self.graphicsView.zoomMode = False
        self.graphicsView.reset_cursor()

    # this is called when the pan icon is clicked
    def set_pan_tool(self):
        self.panTool.setChecked(True)
        self.graphicsView.setDragMode(True)
        self.unset_zoom_tool()

    # this is called when the pan icon is clicked
    def unset_pan_tool(self):
        self.panTool.setChecked(False)
        self.graphicsView.setDragMode(False)

    # this is called when the arrow (select) icon is clicked
    def set_arrow_tool(self):
        self.selectionTool.setChecked(True)
        self.unset_pan_tool()
        self.unset_zoom_tool()

    # handle deselect - called when the deselect tool button is pushed
    def handle_deselect(self):
        if self.clearHCAs.isVisible():
            self.clearHCAs.setVisible(False)
        self.scene.clearSelection()
        self.remove_hcas()
        self.hca_set_is_live = False
        self.current_second_selection = None

    def show_help_dialog(self):
        self.helpDialog.show()

    # initialisation methods to initialise on screen tools and components
    # pyqt has extra functions and methods for everything
    def init_tool_bar(self):
        self.topToolBar.setFixedHeight(198)
        self.bottomToolBar.setFixedWidth(61)
        self.oneLayout.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom)

        self.graphicsView.zoomSignal.connect(self.set_zoom_tool)
        self.graphicsView.panSignal.connect(self.set_pan_tool)
        self.graphicsView.arrowSignal.connect(self.set_arrow_tool)

        self.bottomToolBar.setIconSize(QtCore.QSize(16, 16))

        self.settingsTool.setIcon(QtGui.QIcon("assets/images/icons/settings.png"))
        self.bottomToolBar.addAction(self.settingsTool)
        self.settingsTool.setToolTip("Preferences")

        self.helpTool.setIcon(QtGui.QIcon("assets/images/icons/help.png"))
        self.bottomToolBar.addAction(self.helpTool)
        self.helpTool.setToolTip("Show Help")

        self.selectionTool.setIcon(QtGui.QIcon("assets/images/icons/selection.png"))
        self.selectionTool.setCheckable(True)
        self.selectionTool.setChecked(True)
        self.topToolBar.addAction(self.selectionTool)
        self.selectionTool.setToolTip("Selection Tool")

        self.zoomTool.setIcon(QtGui.QIcon("assets/images/icons/zoom.png"))
        self.zoomTool.setCheckable(True)
        self.topToolBar.addAction(self.zoomTool)
        self.zoomTool.setToolTip("Zoom")

        self.panTool.setIcon(QtGui.QIcon("assets/images/icons/pan.png"))
        self.panTool.setCheckable(True)
        self.topToolBar.addAction(self.panTool)
        self.panTool.setToolTip("Pan")

        self.hcaSelectTool.setIcon(QtGui.QIcon("assets/images/icons/showhcas.png"))
        self.hcaSelectTool.setCheckable(True)
        self.topToolBar.addAction(self.hcaSelectTool)
        self.hcaSelectTool.setToolTip("Show HCAs when a leaf switch is selected")

        self.topToolBar.addWidget(self.topToolSpacer)

        self.deselectTool.setIcon(QtGui.QIcon("assets/images/icons/deselect.png"))
        self.topToolBar.addWidget(self.deselectTool)
        self.deselectTool.clicked.connect(self.handle_deselect)
        self.deselectTool.setToolTip("Deselect All Items")

    # show help dialog
    def show_help(self):
        self.push_notification("Error: Test Error")
        if self.showHelp:
            self.showHelp = not self.showHelp
            self.helpButton.setText("Show Help")
            self.helpBox.setVisible(False)
        else:
            self.showHelp = not self.showHelp
            self.helpButton.setText("Collapse")
            self.helpBox.setVisible(True)

    # set help dialog parameters
    def init_help_box(self):
        self.helpBoxLayout.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom)
        self.helpBox.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom)
        q = CQDropShadow()
        self.helpBox.setVisible(False)
        self.helpBox.setGraphicsEffect(q)
        self.helpBox.setStyleSheet('background: rgba(140, 140, 140, 0.6); font-size: 15px; border-radius: 5px;')
        self.helpBox.setContentsMargins(10, 7, 10, 7)
        self.helpBox.setText("Zoom: ctrl + scroll\nPan: alt + drag (hold left click and drag)")
        self.helpBoxLayout.addWidget(self.helpBox)

    def init_error_box(self):
        self.errorBoxLayout.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)

    # set info box parameters -> displays info when an item(ndoe, link) is selected
    def init_info_box(self):
        # styling and layout for float box
        self.infoBoxLayout.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTop)
        self.infoBoxLayout.setSpacing(0)
        q = CQDropShadow()
        self.infoBox.setGraphicsEffect(q)
        self.infoBox.setStyleSheet('background: rgba(140, 140, 140, 0.6); opacity: 50%; font-size: 15px; '
                                   'border-radius: 5px;')
        self.infoBox.setContentsMargins(10, 7, 10, 7)
        self.infoBoxLayout.addWidget(self.infoBox)
        self.infoBoxLayout.addLayout(self.jumpLayout)
        self.show_jumps(False)

    def init_view_switcher(self):
        self.networkView.setChecked(True)

        self.viewButtonGroup.buttonClicked.connect(self.switch_views)

        self.viewButtonGroup.addButton(self.networkView, 0)
        self.viewButtonGroup.addButton(self.routingView, 1)
        self.viewButtonGroup.addButton(self.conflictsView, 2)

        self.viewBoxLayout.addWidget(self.networkView)
        self.viewBoxLayout.addWidget(self.routingView)
        self.viewBoxLayout.addWidget(self.conflictsView)

        self.viewBoxLayout.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignBottom)
        self.viewBoxLayout.setSpacing(0)

    ### INTERNAL LOGIC FUNCTIONS ###

    # used to handle data, and other program logic

    # general method used to recursively clear a PyQt layout
    # implemented from https://stackoverflow.com/a/45790404
    def clearLayout(self, layout):
        if layout is not None:
            while layout.count():
                item = layout.takeAt(0)
                widget = item.widget()
                if widget is not None:
                    widget.setVisisble(False)
                else:
                    self.clearLayout(item.layout())

    # view switcher method, used to switch tabs and context
    # viewmode int corresponds to tabs in the app
    # perform basic cleanup between switches and switch views with init and close methods of each 'manager'
    def switch_views(self):
        self.scene.clearSelection()
        idx = self.viewButtonGroup.checkedId()
        if idx == 0:
            if self.viewMode == 1:
                # self.set_router_manager_visibility(False)
                self.close_route_manager_window()
                self.routeManager.reset()
            if self.viewMode == 2:
                self.conflictsManagerDialog.close()
                self.route_cleanup(self.currentConflict) if self.currentConflict else None
            self.viewMode = 0
        if idx == 1:
            # self.routeManager.reset()
            # self.set_router_manager_visibility(True)
            if self.viewMode == 2:
                self.conflictsManagerDialog.close()
                self.route_cleanup(self.currentConflict) if self.currentConflict else None
            self.open_route_manager_window()
            self.viewMode = 1
        if idx == 2:
            self.route_cleanup(self.currentConflict) if self.currentConflict else None
            if self.viewMode == 1:
                # self.set_router_manager_visibility(False)
                self.close_route_manager_window()
                self.routeManager.reset()
            if self.viewMode == 0:
                self.scene.clearSelection()
            self.open_conflict_manager()
            self.viewMode = 2

    def init(self):
        self.graphicsView.fitInView(0, self.network_manager.left_extreme, 5000, 5000, QtCore.Qt.KeepAspectRatio)
        # self.graphicsView.scale()
        self.show()

    # construct a logcial network from the network files
    # this just creates QT objects that live in memory until later when its added to the screen
    def construct_network(self):
        max_ports = self.network.root_switches[0].max_ports if self.network.root_switches is not None else 0
        idxl = []
        self.network_manager.set_widget(self)
        self.network_manager.set_network(self.network)
        idxl.append(self.network_manager.add_layer_from_list(self.network.root_switches))
        idxl.append(self.network_manager.add_layer_from_list(self.network.leaf_switches))
        self.add_network_items(self.network_manager.layer_mat[0])
        self.add_network_items(self.network_manager.layer_mat[1])
        self.network_manager.re_calculate(0, 1)
        self.network_manager.generate_connections(0, 1)
        self.add_network_items(self.network_manager.connections_list)

    # create a sample network for testing
    def sample_network(self):
        self.default_infobox()
        network_manager = NetworkManager()
        network_manager.add_layer(9, 40, NetworkItemTypes.SWITCH, self.network_manager.generate_guids(9))
        network_manager.add_layer(18, 40, NetworkItemTypes.SWITCH, self.network_manager.generate_guids(18))
        self.add_network_items(network_manager.layer_mat[0])
        self.add_network_items(network_manager.layer_mat[1])
        network_manager.re_calculate(0, 1)
        for i in range(0, 9):
            for j in range(0, 18):
                nc = NetworkConnection(network_manager.layer_mat[0][i], j, network_manager.layer_mat[1][j], 0)
                nc.source.add_connection(nc)
                nc.destination.add_connection(nc)
                nc.setZValue(-1)
                self.add_network_item(nc)

    # push a notification that shows up on the bottom right
    def push_notification(self, msg, type, duration):
        self.logger.append(str(datetime.datetime.now()) + ": " + msg)
        x = QErrorNotification(msg, type, duration)
        self.oneLayout.addWidget(x)
        x.unhide()
        self.errorQueue.append(x)
        x.dismissError.connect(self.error_collector)

    # auto resize the screen
    def auto_resize(self):
        self.update_scene_rect(self.network_manager.max_width, self.network_manager.max_height)

    # show jump buttons (when a link is selected)
    def show_jumps(self, yn):
        self.jumpSource.setVisible(yn)
        self.jumpDestination.setVisible(yn)

    # remove a list of objects from the screen
    def remove_network_items(self, l):
        for item in l:
            if item.scene():
                self.scene.removeItem(item)

    # the hca object is independently stored above which is cleared by this method
    # it removed objects from the screen with a loop
    def remove_hcas(self):
        self.hca_set_is_live = False
        self.clearHCAs.setVisible(False)
        self.remove_network_items(self.live_hcas)
        for obj in self.live_hcas:
            for connection in obj.hca_connections:
                connection.set_unselected()
                if connection.destinationPortLabel.scene():
                    self.remove_network_items([connection.destinationPortLabel])
            self.remove_network_items(obj.hca_connections)

    # add hcas to the main HCA object
    # and then to the screen
    def add_hcas(self):
        if self.hca_set_is_live:
            return
        self.hca_set_is_live = True
        if not self.disableClearHCAs:
            self.clearHCAs.setVisible(True)
        self.add_network_items(self.live_hcas)
        for obj in self.live_hcas:
            self.add_network_items(obj.hca_connections)
            for conn in obj.hca_connections:
                self.add_network_item(conn.destinationPortLabel)

    # if a selection is related to an HCA object, return True
    # this helps NOT destroy the object which would otherwise create errors and crash
    def is_live_selection_hca(self, sel):
        if sel is not None and isinstance(sel, NetworkConnection):
            if sel.destination.node_type == NetworkItemTypes.HCA:
                return True
        if sel is not None and isinstance(sel, NetworkItem):
            if sel.node_type == NetworkItemTypes.HCA:
                return True
        else:
            return False

    # set hca tool
    def set_hca_tool(self):
        if self.current_selected is not None and isinstance(self.current_selected, NetworkItem):
            if self.current_selected.node_type == NetworkItemTypes.LEAF_SWITCH and len(self.scene.selectedItems()) > 0:
                self.live_hcas = self.network_manager.layer_mat[
                    self.network_manager.hca_to_layer.get(self.current_selected.guid)]
                self.add_hcas()

    def router_manager_listening(self):
        self.route_manager_clear_screen()
        self.disableSelections = False

    # highlight links on selection, change states, etc
    # is called everytime an object is selected or unselected
    # does some logical testing to determine selection type and context and then colors or decolors links accordingly
    def on_state_change(self):
        if self.routeManager.resetState or self.conflictsManagerOpen or self.disableSelections:
            return
        if len(self.scene.selectedItems()) > 0:
            if self.current_selected is not None and isinstance(self.current_selected, NetworkConnection):
                self.current_selected.set_unselected()
            elif self.current_selected is not None and isinstance(self.current_selected, NetworkItem):
                self.current_selected.set_unselected()
                for conn in self.current_selected.ports_list:
                    conn.unhighlight() if conn else None
                if self.hca_set_is_live and not self.is_live_selection_hca(self.scene.selectedItems()[0]):
                    self.remove_hcas()

            if len(self.scene.selectedItems()) == 0:
                self.push_notification("Error: something went wrong -> 0 items selected", NotificationTypes.ERROR, 3000)
            else:
                self.current_selected = self.scene.selectedItems()[0]

            if isinstance(self.current_selected, NetworkConnection):
                self.current_selected.set_selected()
                self.update_infobox_link(self.current_selected)
                self.show_jumps(True)

            elif isinstance(self.current_selected, NetworkItem):
                self.show_jumps(False)
                self.current_selected.set_selected()
                _ = self.current_selected
                self.update_infobox_node(self.infoBox, self.current_selected)
                for conn in self.current_selected.ports_list:
                    conn.highlight() if conn else None
                if self.hcaSelectTool.isChecked():
                    if self.current_selected.node_type == NetworkItemTypes.LEAF_SWITCH:
                        self.live_hcas = self.network_manager.layer_mat[
                            self.network_manager.hca_to_layer.get(self.current_selected.guid)]
                        self.add_hcas()
                    if self.current_selected.node_type == NetworkItemTypes.HCA:
                        if self.routeManager.sourceListening:
                            self.finish_route_source(self.current_selected)
                        if self.routeManager.destinationListening:
                            self.finish_route_destination(self.current_selected)
                        if self.routeManagerWindow.sourceListening:
                            self.routeManagerWindow.finish_source(self.current_selected)
                            self.disableSelections = True
                        if self.routeManagerWindow.destListening:
                            self.routeManagerWindow.finish_destination(self.current_selected)
                            self.disableSelections = True

        else:
            if self.hca_set_is_live:
                pass
                # self.remove_hcas()
            self.default_infobox()
            if isinstance(self.current_selected, NetworkConnection):
                self.current_selected.set_unselected()
                self.show_jumps(False)

            elif isinstance(self.current_selected, NetworkItem):
                self.current_selected.set_unselected()
                for conn in self.current_selected.ports_list:
                    conn.set_unselected() if conn else None

    # implements the jump button which jumps to the x, y of the source of the selected connection
    # uses qt's center method to center the screen on the top left x,y of the source object
    def jump_to_source(self):
        if self.current_selected is not None and isinstance(self.current_selected, NetworkConnection):
            # cur = self.graphicsView.mapToScene()
            center_x = (self.current_selected.source.x + (
                    self.current_selected.source.x + self.current_selected.source.w)) / 2
            center_y = self.current_selected.source.y
            self.graphicsView.centerOn(center_x, center_y)
            self.graphicsView.resetTransform()
            self.graphicsView.scale(0.8, 0.8)

    def jump_to_current_selection(self):
        if self.current_selected is not None and isinstance(self.current_selected, NetworkItem):
            self.graphicsView.fitInView(self.current_selected.x - (self.current_selected.w/2), self.current_selected.y,
                                        2000, 200, QtCore.Qt.KeepAspectRatio)

    # same as above but for the destination
    def jump_to_destination(self):
        if self.current_selected is not None and isinstance(self.current_selected, NetworkConnection):
            # cur = self.graphicsView.mapToScene()
            center_x = (self.current_selected.destination.x + (
                    self.current_selected.destination.x + self.current_selected.destination.w)) / 2
            center_y = self.current_selected.destination.y
            self.graphicsView.centerOn(center_x, center_y)
            self.graphicsView.resetTransform()
            self.graphicsView.scale(0.5, 0.5)

    # reset infobox (top right) to default
    def default_infobox(self):
        self.infoBox.setText("no items currently selected")

    # update infobox when a link is selected
    def update_infobox_link(self, conn: NetworkConnection):
        s = "Selected: Network Link\n"
        s += "LinkID: " + conn.connect_id + "\n"
        s += "SourceID: " + str(conn.source.guid) + " [" + conn.source.name + "]\n"
        s += "DestinationID: " + str(conn.destination.guid) + " [" + conn.destination.name + "]\n"
        s += "Source Port: " + str(conn.source_port + 1) + "\n"
        s += "Destination Port: " + str(conn.destination_port + 1) + "\n"
        self.infoBox.setText(s[:len(s) - 1])

    # update infobox when a route is selected
    def update_routing_infobox(self, label: QtWidgets.QLabel, item: NetworkItem):
        s = "GUID: " + str(item.guid) + "\n"
        s += "Name: " + item.name + "\n"
        s += "Connected Ports: " + str(item.ports_list[0].source_port + 1) + "\nConnected to: " + item.ports_list[
            0].source.name + "\n"
        label.setText(s[:len(s) - 1])

    # update infobox when a node is selected
    def update_infobox_node(self, label, node: NetworkItem):
        s = "Selected: Network Node\n"
        s += "Name: " + node.name + "\n"
        s += "GUID: " + str(node.guid) + "\n"
        s += "Connected Ports: " + str(len(node.internal_object.ports)) + "\n"
        s += "Device Type: " + str(node.node_type) + "\n"
        label.setText(s[:len(s) - 1])

    # basic internal methods to initialize PyQT
    def init_graphics(self):
        self.graphicsView.setScene(self.scene)
        self.graphicsView.setSceneRect(0, 0, self.graphics_width, self.graphics_height)

    def add_network_item(self, item):
        return self.scene.addItem(item)

    def update_scene_rect(self, x, y):
        self.graphicsView.setSceneRect(0, 0, x, y)

    def add_network_items(self, items):
        if len(items) <= 0:
            return
        for item in items:
            self.scene.addItem(item)

    def add_link(self, n):
        self.scene.addItem(n)
