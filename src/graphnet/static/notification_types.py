from enum import Enum

# notification types, enum
class NotificationTypes(Enum):
    INFO = 0
    ERROR = 1
    WARNING = 3
