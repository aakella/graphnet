from enum import Enum

class ViewModes(Enum):
    # view modes, tabs, views
    # these are values used to check which mode the tab currently is in
    NETWORK = 0
    ROUTING = 1
    CONFLICTS = 2
