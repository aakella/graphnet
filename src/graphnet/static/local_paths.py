

# vars with paths to the pickle objects
# the core data for graphnet
network_path = "pickles/network.pickle"
routing_tables_path = "pickles/routing_tables.pickle"
conflicts_path = "pickles/conflicts.pickle"
