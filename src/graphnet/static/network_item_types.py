from enum import Enum


class NetworkItemTypes(Enum):
    # enum with network object types
    ROOT_SWITCH = 1
    LEAF_SWITCH = 2
    HCA = 3
    OTHER = 4
    NONE = 5
    LINK = 6
    SWITCH = 7
