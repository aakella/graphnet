from static.network_item_types import NetworkItemTypes
from PyQt5 import QtGui


# default color scheme for the on-screen objects
# the colors are in RGB(X, Y, Z)
# X, Y, Z range {0, 255}
colors = {
    NetworkItemTypes.ROOT_SWITCH: {
        "default": QtGui.QColor(110, 127, 125),
        "hover": QtGui.QColor(171, 186, 184),
        "selected": QtGui.QColor(106, 158, 158),
        "highlight": QtGui.QColor(255, 25, 0)
    },
    NetworkItemTypes.LEAF_SWITCH: {
        "default": QtGui.QColor(110, 127, 125),
        "hover": QtGui.QColor(171, 186, 184),
        "selected": QtGui.QColor(106, 158, 158),
        "highlight": QtGui.QColor(255, 25, 0),
        "partial": QtGui.QColor()
    },
    NetworkItemTypes.SWITCH: {
        "default": QtGui.QColor(110, 127, 125),
        "hover": QtGui.QColor(171, 186, 184),
        "selected": QtGui.QColor(106, 158, 158),
        "highlight": QtGui.QColor(255, 25, 0)
    },
    NetworkItemTypes.HCA: {
        "default": QtGui.QColor(110, 127, 125),
        "hover": QtGui.QColor(171, 186, 184),
        "selected": QtGui.QColor(106, 158, 158)
    },
    NetworkItemTypes.OTHER: {
        "default": QtGui.QColor(110, 127, 125),
        "hover": QtGui.QColor(110, 127, 125),
        "selected": QtGui.QColor(110, 127, 125)
    },
    NetworkItemTypes.LINK: {
        "default": QtGui.QColor(127, 127, 127),
        "hover": QtGui.QColor(147, 139, 201),
        "selected": QtGui.QColor(34, 0, 255),
        "highlight": QtGui.QColor(34, 0, 255)
    },
    NetworkItemTypes.NONE: {
        "default": QtGui.QColor(110, 127, 125),
        "hover": QtGui.QColor(110, 127, 125),
        "selected": QtGui.QColor(110, 127, 125)
    },
    "general": {
        "mark": QtGui.QColor(255, 25, 0),
    }

}