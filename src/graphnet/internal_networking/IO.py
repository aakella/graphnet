import pickle
#import gnosis.xml.pickle
#import gnosis.xml.pickel
#from dict2xml import dict2xml
#import json
#import jsonpickle
from internal_networking.Hca import *
from internal_networking.Node import *
from internal_networking.Port import Port
from internal_networking.Switch import *
from internal_networking.Guid2LidTable import *
from internal_networking.RoutingTable import *

import re


class IO():


    @staticmethod
    def write_to_simple_file_from_network(outFile, network):
        text_file = open(outFile, "w")

        #text_file.write("\n\n")

        #Put all HCAs first
        for node in network:
            if type(node).__name__=="Hca":
                text_file.write(
                    "NODE->HCA \n")
                for port in node.ports:
                    text_file.write(
                        "  PORT_GUID: "
                        + hex(port.PortGUID)
                        + " PORT_NO: "
                        + str(port.PortNo)
                        + " NEIGHBOUR_GUID: "
                        + hex(port.NeighborGUID)
                        + " NEIGHBOR_PORT_NO: "
                        + str(port.NeighborPortNo)
                        + "\n")

        text_file.write("\n\n")
        #Now service switches
        for node in network:
            if type(node).__name__=="Switch":
                text_file.write("NODE->SWITCH GUID: " + hex(node.NodeGUID) + "\n")
                for port in node.ports:
                    text_file.write(
                        "  PORT_NO: "
                        + str(port.PortNo)
                        + " NEIGHBOR_GUID: "
                        + hex(port.NeighborGUID)
                        + " NEIGHBOR_PORT_NO: "
                        + str(port.NeighborPortNo)
                        + "\n")
            pass

        pass
        text_file.close()

    '''
        @staticmethod
        def write_binary_to_file(setup,outFile):
            pickle.dump(setup, outFile, protocol=None)

        @staticmethod
        def read_binary_from_file(inFile):
            input = pickle.load(inFile)
            return input
    '''

    # def write_xml_to_file(setup,outFile):
    #    pickle.dump(setup, outFile)

    # @staticmethod
    # def read_xml_from_file(inFile):
    #    input = pickle.load(inFile)
    #    return input
    '''
        @staticmethod
        def read_json_from_file(inFile):
            jsonpickle.set_encoder_options('simplejson', indent=2)
            text_file = open(inFile, "r")
            output = text_file.read()
            text_file.close()
            output_json = jsonpickle.decode(output)
            return output_json

        def write_json_to_file(setup,outFile):
            jsonpickle.set_encoder_options('simplejson', indent=2)
            json_string = jsonpickle.encode(setup)
            print("JSON STRING:")
            print(json_string)
            #result = jsonpickle.decode(json_string)

            text_file = open(outFile, "w")
            text_file.write(json_string)
            text_file.close()
            #json_string = jsonpickle.encode(setup)
            #my_json_object = json.dumps(setup)
            #print(my_json_object)
            #json.dump(setup, outFile)
    '''

    @staticmethod
    def get_network_from_simple_file(inFile):
        network = []
        text_file = open(inFile, "r")
        raw_text = text_file.read()

        node_divided_text = raw_text.split("NODE->")

#Do all the HCAs
        for entry in node_divided_text:
            if entry.find("HCA") != -1:
                line_split_entry=entry.split("\n")
                ports_entries = []
                for line_entry in line_split_entry:
                    if line_entry.find("PORT_GUID") != -1:
                        values = line_entry.split(" ")
                        port_guid = int(values[3], 16)
                        port_no = int(values[5])
                        n_guid = int(values[7], 16)
                        n_port_no = int(values[9])

                        ports_entries.append(
                            Port(PortNo=port_no, PortGUID=port_guid,
                                NeighborGUID=n_guid, NeighborPortNo=n_port_no))

                network.append(Hca(ports=ports_entries, max_ports=len(ports_entries)))

# Now do all the Switches
        for entry in node_divided_text:
            if entry.find("SWITCH") != -1:
                line_split_entry=entry.split("\n")
                switch_info_line = line_split_entry[0].split(" ")
                switch_guid = int(switch_info_line[2], 16)

                ports_entries = []
                for line_entry in line_split_entry:

                    if line_entry.find("PORT_NO") != -1:
                        values = line_entry.split(" ")
                        port_no = int(values[3])
                        n_guid = int(values[5], 16)
                        n_port_no = int(values[7])

                        ports_entries.append(
                            Port(PortNo=port_no, PortGUID=switch_guid,
                                 NeighborGUID=n_guid, NeighborPortNo=n_port_no))

                network.append(Switch(
                    node_guid = switch_guid,
                    ports=ports_entries,
                    max_ports=len(ports_entries)))

        text_file.close()

        return network


    def get_roots_from_file(inFile):
        text_file = open(inFile, "r")
        raw_text = text_file.read()
        text_file.close()
        roots_array = raw_text.split("\n")
        roots_array.pop()
        roots=[]
        for root_entry in roots_array:
            roots.append(int(root_entry, 16))
            #int("e41d2d0300bfb960", 16)
        return roots

    def get_network_from_ibnetdiscover_file(inFile):
        text_file = open(inFile, "r")
        raw_text = text_file.read()
        text_file.close()
        network=[]

        nodes_array = raw_text.split("vendid")
        del(nodes_array[0])

        for node_entry in nodes_array:
            node_line_split=node_entry.split("\n")
            type_info=node_line_split[4].split()
            if(type_info[0]=='Switch'):
                guid_full=node_line_split[3].replace("switchguid=","")
                switch_guid = re.sub('.*\(','',guid_full)
                switch_guid = re.sub('\)', '', switch_guid)
                switch_guid_int=int(switch_guid, 16)

                #GUID done, now ports
                ports_entries = []
                port_lines=[n for n in node_line_split if len(n)!=0 and n[0]=="["]
                switch_is_root = False
                switch_port_count = 0
                for port_line in port_lines:
                    print(port_line)
                    ports = re.search(r"\[(.*)\].*\[(.*)\]",port_line)
                    host_port_no = int(ports.group(1))


                    remote_port_no = int(ports.group(2))
                    remote_guid=re.search(r"\((.*)\)",port_line)

                    if remote_guid==None:
                        print("NO GUID FOUND. ASSUMING SWITCH. GETTING FROM NAME INSTEAD")
                        switch_port_count += 1
                        remote_guid = re.search(r"\"S-(.*?)\"", port_line)

                    remote_guid_int=int(remote_guid.group(1), 16)

                    if host_port_no==41:
                        switch_port_count += 1
                        print("PORT 41. INGORING")
                    else:
                        ports_entries.append(
                            Port(PortNo=host_port_no, PortGUID=switch_guid_int,
                                NeighborGUID=remote_guid_int, NeighborPortNo=remote_port_no))

                #check if all ports are connected to another switch
                if(switch_port_count == len(port_lines)):
                    switch_is_root = True

                network.append(Switch(node_guid=switch_guid_int, ports=ports_entries, max_ports=40, is_root=switch_is_root))
            elif(type_info[0]=="Ca"):
                if node_line_split[4].find("Mellanox Technologies Aggregation Node") != -1:
                    print("FOUND AGGREGATION NODE. SKIPPING")
                else:
                    ports_entries = []
                    port_lines = [n for n in node_line_split if len(n) != 0 and n[0] == "["]
                    for port_line in port_lines:
                        print(port_line)
                        ports = re.search(r"(\[.*\]).*(\[.*\])", port_line)
                        #TODO - bugfix
                        host_port_no_str = re.findall('[0-9]+', (ports.group(1)))
                        remote_port_no_str = re.findall('[0-9]+',(ports.group(2)))
                        host_port_no = int(host_port_no_str[0])
                        remote_port_no = int(remote_port_no_str[0])
                        local_guid = re.search(r"\((.*)\)", port_line)
                        local_guid_int = int(local_guid.group(1), 16)
                        remote_guid_whole=re.search('S-(.*)\"\[', port_line)
                        remote_guid=remote_guid_whole.group(1)
                        remote_guid_int=int(remote_guid,16)

                        ports_entries.append(
                            Port(PortNo=host_port_no, PortGUID=local_guid_int,
                                 NeighborGUID=remote_guid_int, NeighborPortNo=remote_port_no))

                    network.append(Hca(ports=ports_entries, max_ports=1))
        return network


    @staticmethod
    def produce_guid_2_lid_table(inFile):

        guid_2_lid_table = Guid2LidTable()

        lid_file_desc=open(inFile, "r")
        raw_lids_text = lid_file_desc.read()
        lid_file_desc.close()
        lids_split = raw_lids_text.split("\n\n")
        lids_split.pop()
        for entry in lids_split:
            guid_lid = entry.split(" ")
            guid_2_lid_table.add_entry(int(guid_lid[0],0), int(guid_lid[1],0))

        return guid_2_lid_table

    @staticmethod
    def produce_incomplete_lft_tables(inFile):

        lft_file_desc=open(inFile, "r")
        raw_lfts_text = lft_file_desc.read()
        lft_file_desc.close()

        routing_tables = []
        switches_split = raw_lfts_text.split("Unicast")
        switches_split.pop(0)

        for switch_entry in switches_split:
            all_switch_entry_lines = switch_entry.split("\n")
            first_line = all_switch_entry_lines[0]
            first_line_split = first_line.split(" ")
            #switch_guid_string = first_line_split[8]
            #switch_lid_string = first_line_split[6]

            switch_guid = int(first_line_split[8],0)
            switch_lid = int(first_line_split[6],0)


            #clean the list out of all that is not an lft entry
            all_switch_entry_lines.pop(0)
            all_switch_entry_lines.pop()
            all_switch_entry_lines.pop()

            rtable = RoutingTable(switch_guid)
            for ltf_entry in all_switch_entry_lines:
                index_hca = ltf_entry.find("HCA")
                index_sw = ltf_entry.find("switch")
                lft_entry_split = ltf_entry.split(" ")

                lft_destination_LID = int(lft_entry_split[0],0)
                lft_local_hop_port_no = int(lft_entry_split[1])
                #local port 0 means the local node in which we are now
                #ignore that entry
                if lft_local_hop_port_no == 0:
                    continue

                if index_sw != -1:
                    lft_type="Switch"
                elif index_hca != -1:
                    lft_type = "Hca"
                else:
                    lft_type="Unknown"

                lft_entry = RoutingTableEntry(
                    destination_GUID=-1,
                    destination_LID=lft_destination_LID,
                    destination_TYPE=lft_type,
                    next_hop_GUID=-1, #TODO
                    next_hop_LID=-1, #TODO
                    local_hop_port=-1, #TODO
                    remote_hop_port=-1, #TODO
                    local_hop_port_no=lft_local_hop_port_no,
                    remote_hop_port_no=-1, #TODO
                    is_for_place_keeper=False)
                rtable.routingTableEntries.append(lft_entry)
            routing_tables.append(rtable)
        return routing_tables


    @staticmethod
    def produce_shift_pattern_order(inFile):
        pattern_order=[]
        pattern_file_desc=open(inFile, "r")
        pattern_text = pattern_file_desc.read()

        pattern_file_desc.close()

        pattern_text_split = pattern_text.split("\n")
        pattern_text_split.pop()


        for node in pattern_text_split:
            #if node == "DUMMY":
            #    #-1 FOR ALL DUMMIES
            #    pattern_order.append(-1)
            #    pass
            #else:
            node_split = node.split("\t")
            lid=node_split[0]
            pattern_order.append(int(lid,0))

        return pattern_order


    @staticmethod
    def produce_dump_fts_compliant_file(outFile, routingTables):
        file_desc=open(outFile, "w")

        for routingTable in routingTables:
            print("producing output for switch GUID: " + str(routingTable.switch_GUID)
                  + " (HEX): " + hex(routingTable.switch_GUID))
            file_desc.write("Unicast lids guid " + hex(routingTable.switch_GUID) + "  \n")
            for lftEntry in routingTable.routingTableEntries:
                #Format
                #<LID> <PORT_NO> : portguid <dest_port_guid_in_hex>
                #0x0001 015: (Channel Adapter portguid 0x7cfe9003005845f
                file_desc.write(hex(lftEntry.destination_LID) + "  ")

                file_desc.write(str(lftEntry.local_hop_port_no)  + " ")
                file_desc.write(" portguid ")
                #port GUID number has to be incremented
                #this is because of the OpenSM quirk to distinct for
                #HCA GUID and its ports GUIDs
                #according to formula HCA_GUID + port_no

                if lftEntry.destination_TYPE == "Hca":
                    file_desc.write(hex(lftEntry.destination_GUID+ 1))
                else:
                    file_desc.write(hex(lftEntry.destination_GUID))

                file_desc.write(" \n")


        file_desc.close()

    @staticmethod
    def produce_shift_file(outFile, shift_array):

        file_desc=open(outFile, "w")
        file_desc.write("LID" + "   " + "GUID\n")
        for shift_entry in shift_array:
            if shift_entry == "DUMMY":
                file_desc.write(str("0xFFFF") + "   " + str("DUMMY"))
                file_desc.write(" \n")
            else:
                file_desc.write(str(shift_entry.ports[0].PortLID) + "   " + hex(shift_entry.ports[0].PortGUID))
                file_desc.write(" \n")

        file_desc.close()


    def get_hosts_array_from_ibnodes_file(inFile):
        file_desc=open(inFile, "r")
        pattern_text = file_desc.read()
        file_desc.close()

        pattern_text_lines=pattern_text.split("\n")
        hosts = []

        for line in pattern_text_lines:
            line_array=line.split(" ")
            if line_array[0].find("Ca")!= -1:
                print("Found Ca")

                hostname = line_array[4]
                hostname=hostname[1:]
                if hostname.find("Quantum Mellanox Technologies")!=-1:
                    print("FOUND AGGREGATION NODE. LEAVING")
                    continue
                GUID = int(line_array[1],16)
                ib_no_string = line_array[5]
                ib_no_string = ib_no_string[:-1]
                if ib_no_string=="HCA-1" or ib_no_string=="mlx5_0":
                    ib_int_no=0
                elif ib_no_string=="HCA-2" or ib_no_string=="mlx5_1":
                    ib_int_no=1
                else:
                    print("HOST NUMBER UNKNOWN")
                    ib_int_no = -1
                hosts.append((hostname, ib_int_no, GUID))
            pass

        return hosts


# from lxml import etree
# from lxml import objectify
#
# xml = '''
# <dataset>
#   <statusthing>success</statusthing>
#   <datathing gabble="sent">joe@email.com</datathing>
#   <datathing gabble="not sent"></datathing>
# </dataset>
# '''
#
# class XmlObjectifier():
# 	@staticmethod
# 	def readNetworkFromXml():
# 		root = objectify.fromstring(xml)
#
# 		print (root.tag)
# 		print (root.text)
# 		print (root.attrib)
#
# 		for e in root.datathing:
# 			print (e.tag)
# 			print (e.text)
# 			print (e.attrib)
# 			print (e.attrib['gabble'])
#
# 	@staticmethod
# 	def writeNetworkToXml(network):
# 		xml_new = etree.tostring(network, pretty_print=True)
# 		print (xml_new)
