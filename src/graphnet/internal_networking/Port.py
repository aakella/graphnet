class Port:
    LID = -1
    PortGUID = 0
    def __init__(self,PortNo, PortGUID=-1, NeighborGUID=-1,NeighborPortNo=0):
        self.PortGUID=PortGUID
        self.PortLID=-1
        #could be port GUID or node GUID, depending on the device
        self.NeighborGUID=NeighborGUID
        self.NeighborPortNo = NeighborPortNo
        self.PortNo=PortNo

