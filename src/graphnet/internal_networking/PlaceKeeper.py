from internal_networking.Node import *

class PlaceKeeper(Node):
    def __init__(self, ports=[], DummyLID=-1, DummyGUID=-1, ParentGUID=0):
        Node.__init__(self,ports,max_ports=1)
        self.DummyLID=DummyLID
        self.DummyGUID=DummyGUID
        self.ParentGUID=ParentGUID