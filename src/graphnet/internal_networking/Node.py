class Node:
    ports = []

    def __init__(self, ports=[],max_ports=1):
        self.ports=ports
        self.max_ports=max_ports

    def get_port_by_guid(self, GUID):
        for port in self.ports:
            if port.PortGUID==GUID :
                return port
        return []

    def get_port_by_lid(self, LID):
        for port in self.ports:
            if port.PortLID==LID :
                return port
        return []

    def get_port_by_num(self, PortNo):
        for port in self.ports:
            if port.PortNo==PortNo :
                return port
        return []