from internal_networking.RoutingTableEntry import *

class RoutingTable():
    def __init__(self,switch_GUID):
        self.switch_GUID=switch_GUID
        self.routingTableEntries=[]
    @staticmethod
    def find_switch_lft_by_guid(routingtables,GUID):
        for switch_lft in routingtables:
            if switch_lft.switch_GUID==GUID:
                return switch_lft

    def find_lft_dest_entry(routing_tables,GUID,dest_GUID):
        lft =  RoutingTable.find_switch_lft_by_guid(routing_tables,GUID)
        for lft_entry in lft.routingTableEntries:
            if lft_entry.destination_GUID==dest_GUID:
                return lft_entry
        print("NO LFT ENTRY FOUND")
        return []
