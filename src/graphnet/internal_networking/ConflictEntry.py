class ConflictEntry:
    def __init__(self, shift_distance=-1, node=None, nodeGuid=0, nodePortNo=-1, conflictingRoutes=[]):
        self.shift_distance=shift_distance

        self.node=node
        self.nodeGUID=nodeGuid
        self.nodePortNo=nodePortNo

        #LIst of GUID pairs of SRC and DST that were co-using this port
        self.conflictingRoutes=conflictingRoutes
