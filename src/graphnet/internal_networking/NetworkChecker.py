class NetworkChecker:
    @staticmethod
    def print_network(network):
        for node in network:
            print("Node type:")
            print(type(node).__name__)
            print("Ports:", flush=True)
            print(node.max_ports)
            for index, port in enumerate(node.ports):
                print("Port", index,"out of",node.ports)
                print("Port GUID", port.PortGUID)
                print("Port LID", port.LID)
                print("Port Neighbor Port Number", port.NeighborPortNo)
                print("Neighbor GUID", port.NeighborGUID)

            #print(dict(node))

    ##class to check if the existing network is coherent with the actual network
