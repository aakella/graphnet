#the LUT routing table entries contain typically the following fields:
#destination LID
#port to reach destination LID
#number of hops
#if optimal

#such structure can be retrieved with the following steps:
#running sudo ibdiagnet --routing
#reading file located by default under /var/tmp/ibdiagnet/ibdiagnet2.fdbs for unicast

#this structure does not contain optimal field-not needed
#instead it conteins additional auxiliary fields for debugging
class RoutingTableEntry():
    def __init__(self,destination_GUID=-1,destination_LID=-1, destination_TYPE="Hca"
                 ,next_hop_GUID=-1,next_hop_LID=-1,local_hop_port=-1,remote_hop_port=-1,
                 local_hop_port_no=-1, remote_hop_port_no=-1,is_for_place_keeper=False, keeper_switch_GUID=-1):

        self.destination_GUID=destination_GUID
        self.destination_LID=destination_LID
        self.destination_TYPE=destination_TYPE
        self.next_hop_GUID=next_hop_GUID
        self.next_hop_LID=next_hop_LID
        self.local_hop_port=local_hop_port
        #self.remote_hop_port=remote_hop_port
        self.local_hop_port_no=local_hop_port_no
        self.remote_hop_port_no = remote_hop_port_no
        self.is_for_place_keeper=is_for_place_keeper
        self.keeper_switch_GUID=keeper_switch_GUID