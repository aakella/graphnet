from internal_networking.Node import *
from internal_networking.PortMeta import *

class NodeMeta:
    def __init__(self, node, isSpine=False,level=-1):
        self.ports_meta_list=[]
        self.node=node
        self.isSpine=isSpine
        self.level=level
        self.type=type(node).__name__
        self.placeKeepers=[]
        for port in node.ports:
            self.ports_meta_list.append(PortMeta(port))
    def find_meta_by_port(self, port):
        for port_meta in self.ports_meta_list:
            if port_meta.port == port:
                print("FOUND PORT METADATA ENTRY")
                return port_meta

    @staticmethod
    def find_node_meta_by_lid(LID, all_meta):
        for meta_node in all_meta:
            if meta_node.type == "Switch":
                if meta_node.node.NodeLID == LID:
                   return meta_node, meta_node.node.NodeGUID
            if meta_node.type == "Hca":
                port = meta_node.node.get_port_by_lid(LID)
                if  port != []:
                    return meta_node, port.PortGUID
        print("NODE NOT FOUND")
        return -1,-1

    @staticmethod
    def find_node_meta_by_guid(GUID, all_meta):
        for meta_node in all_meta:
            if meta_node.type == "Switch":
                if meta_node.node.NodeGUID == GUID:
                   return meta_node
            if meta_node.type == "Hca":
                node = meta_node.node
                port = node.get_port_by_guid(GUID)
                if  port != []:
                    return meta_node
        print("NODE NOT FOUND")
        return -1
