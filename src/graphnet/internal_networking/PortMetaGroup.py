from internal_networking.Port import *
from internal_networking.PortMeta import *

class PortMetaGroup():
    def __init__(self, GUID):
        self.GUID=GUID
        self.ports_metas=[]

    def get_n_check_ports_level(self):
        #level=0
        ref_level = self.ports_metas[0].neighbor_level

        for meta in self.ports_metas:
            if meta.neighbor_level != ref_level :
                print("FATAL. DIFFERENT LEVELS OF LINKS IN THE GROUP. ABORTING")
                exit()

        return ref_level

    @staticmethod
    def find_by_guid(PortMetaGroupArray, GUID):
        for group in PortMetaGroupArray:
            if group.GUID == GUID:
                return group
        return []



    @staticmethod
    def find_least_loaded_port_meta(PortMetaGroup):
        min_load_port_meta=PortMetaGroup.ports_metas[0]
        for port_meta in PortMetaGroup.ports_metas:
            if port_meta.usedForUp<min_load_port_meta.usedForUp:
                min_load_port_meta=port_meta
        return min_load_port_meta

    #if empty
