from enum import Enum

class Function(Enum):
    SENDING=0
    RECEIVING=1

class TrafficEntry:
    #def __init__(self, source_GUID,destination_GUID,
    #             local_switch_GUID,local_switch_port_no,remote_switch_GUID,remote_switch_port_no):
        #self.source_GUID=source_GUID
        #self.destination_GUID=destination_GUID
        #self.local_switch_GUID=local_switch_GUID
        #self.local_switch_port_no=local_switch_port_no
        #self.remote_switch_GUID=remote_switch_GUID
        #self.remote_switch_port_no=remote_switch_port_no

    def __init__(self, source_GUID,destination_GUID,
                 switch_GUID,switch_port_no, function):
        self.source_GUID=source_GUID
        self.destination_GUID=destination_GUID
        self.switch_GUID=switch_GUID
        self.switch_port_no=switch_port_no
        #function for sendint/receiving
        #added because the links support duplex
        self.function=function
        #self.remote_switch_GUID=remote_switch_GUID
        #self.remote_switch_port_no=remote_switch_port_no

    @staticmethod
    def check_if_port_used(GUID, port_no, network_usage_list, function):
        for entry in network_usage_list:
            if entry.switch_GUID==GUID and entry.switch_port_no==port_no and entry.function==function:
                #print ("PORT ALREADY USED !")
                #print ("CONFLICT ! PORT USED FOR ROUTING FROM ", entry.source_GUID, " TO ", entry.destination_GUID)
                #print ("ROLE: ", entry.function)
                return True, entry
        return False, None
