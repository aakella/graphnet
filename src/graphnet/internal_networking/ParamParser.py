from internal_networking.Hca import *
from internal_networking.Node import *
from internal_networking.Switch import *
from internal_networking.FatTreeAlg import *
from internal_networking.NetworkChecker import *
from internal_networking.IO import *
from internal_networking.DummyNetworksHolder import *
from internal_networking.NetworkTrafficTester import *
import argparse
import pickle
import json


def ParamParser():
    print("PARSING PARAMS")
    parser = argparse.ArgumentParser()

    parser.add_argument('-nit', default='i', help='program input file type: i(bnetdiscover) s(imple)')

    parser.add_argument('-ni', help='program network input file', required=True)

    parser.add_argument('-v', help='be verbose', action='store_true')

    parser.add_argument('-no', help='optional output file directory with simple network format')

    parser.add_argument('-r', help='do routing', action='store_true')

    parser.add_argument('-t', help='do network test', action='store_true')

    parser.add_argument('-lo', help='optional output file with lfts for OpenSM')

    parser.add_argument('-gl', help='optional guid2lid assignment in external file')

    parser.add_argument('-lfts', help='optional lfts from external file (done instead of routing)')

    parser.add_argument('-so', help='optional shift order file for network test')

    parser.add_argument('-sf', help='optional shift order output file')

    parser.add_argument('-rl', help='root switches list', required=True)

    parser.add_argument('-nf', help='optional ibnodes file')

    parser.add_argument('-pickles', default='./pickles/', help='directory path to save pickle files')

    args = parser.parse_args()


## READ ROOTS LIST

    root_switches = IO.get_roots_from_file(args.rl)

    if args.nit == 'i':
        network = IO.get_network_from_ibnetdiscover_file(
            args.ni)
    elif args.nit == 's':
        network = IO.get_network_from_simple_file(
            args.ni)
    else:
        print("UNKNOWN input file format")
        return

    if args.nf:
        nodes = IO.get_hosts_array_from_ibnodes_file(args.nf)
        pass
    if args.gl:
        FatTreeAlg.update_network_LIDs_from_file(network, args.gl)
    else:
        FatTreeAlg.set_network_LIDs(network)

    if args.v :
        NetworkChecker.print_network(network)
        with open(args.pickles + "network.pickle", "wb") as filehandle:
            pickle.dump(network, filehandle)

    if args.no != None:
        IO.write_to_simple_file_from_network(
            args.no,
            network)


    if args.r:
        routing_tables,network_metadata = FatTreeAlg.do_fat_tree_routing(
            network,root_switches)

    elif args.gl and args.lfts:
        routing_tables,network_metadata = FatTreeAlg.set_lids_lfts_shift_from_file(
            network,
            args.lfts,
            root_switches)
    else:
        print("ERROR- Neither routing nor LFTs selected")
        return

    if args.v:
        FatTreeAlg.print_routing_tables(routing_tables, True)
        with open(args.pickles + "routing_tables.pickle", "wb") as filehandle:
            pickle.dump(routing_tables, filehandle)

    if args.lo:
        IO.produce_dump_fts_compliant_file(args.lo,routing_tables)

    if args.so:
        shift_order = FatTreeAlg.produce_shift_pattern_from_file(args.so, network_metadata)
    else:
        shift_order = FatTreeAlg.produce_shift_pattern_from_network(network_metadata)

    if args.sf:
        IO.produce_shift_file(args.sf, shift_order)

    if args.t:
        if shift_order == None:
            print("ERROR - NO SHIFT PATTERN")
            return
        conflicts = NetworkTrafficTester.check_linear_shift(network,network_metadata,routing_tables,
            shift_order)
    if(conflicts == []):
        print("NO CONFLICTS")
    else:
        with open(args.pickles + "conflicts.pickle", "wb") as filehandle:
            pickle.dump(conflicts, filehandle)
        print("CONFLICTS OCCURRED")


if __name__ == '__main__':
    ParamParser()
