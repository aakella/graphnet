from internal_networking.Hca import *
from internal_networking.Node import *
from internal_networking.Switch import *
from internal_networking.FatTreeAlg import *
from internal_networking.NetworkChecker import *
from internal_networking.IO import *
from internal_networking.DummyNetworksHolder import *
from internal_networking.NetworkTrafficTester import *
import argparse

def wrapper_get_lfts_and_shift(topo_file, roots_file, nodes_list, ibnodes_file):

    network = IO.get_network_from_ibnetdiscover_file(topo_file)
    root_switches = IO.get_roots_from_file(roots_file)
    ibnodes = IO.get_hosts_array_from_ibnodes_file(ibnodes_file)


    routing_tables, network_metadata = FatTreeAlg.do_fat_tree_routing(
        network, root_switches)

    shift_order = FatTreeAlg.produce_shift_pattern_from_network(network_metadata)

    shift_pattern_tuples=FatTreeAlg.produce_shift_pattern_tuples_subset(shift_order,nodes_list,ibnodes)


    return routing_tables, shift_pattern_tuples

if __name__ == '__main__':
    topo_file="../input_files/topo/frankfurt_trimmed/topo_test_set_1.txt"
    roots_file="../input_files/routing/frankfurt_trimmed/set_1/roots.txt"
    nodes_list=[('node030', 0), ('node034', 0), ('node035', 0)]
    ibnodes_file="../input_files/topo/frankfurt_trimmed/ibnodes_set_1.txt"
    routing_tables, shift_pattern_tuples = wrapper_get_lfts_and_shift(
        topo_file, roots_file, nodes_list, ibnodes_file)