from internal_networking.Port import *

class PortMeta():
    def __init__(self, port):
        # in this configuration the "down link" is used only once
        self.usedForDown=0
        #For the up links we need a counter
        self.usedForUp = 0
        self.port=port
        self.neighbor_level=-1
        #TODO -necessary ?
        #self.neighborLevel=-1
    #TODO
    #def get_neighbor_level(self, PortsMeta):
    #    pass
    #TODO
    #@staticmethod
    #def get_meta_by_guid(self, PortsMeta):
    #    pass

    @staticmethod
    def get_meta_by_port_number(portNumber,switchMeta):
        for port_meta in switchMeta.ports_meta_list:
            if port_meta.port.PortNo==portNumber:
                return port_meta
        return -1