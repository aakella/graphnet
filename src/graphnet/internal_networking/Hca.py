from internal_networking.Node import Node


class Hca(Node):
    def __init__(self, ports=[],max_ports=1):
        Node.__init__(self,ports,max_ports)

    # add 1 just as opensm does it (for...reasons)
    def transform_port_guid_topo_to_opensm(self, port_no):
        port = self.get_port_by_num(port_no)
        return self.transform_guid_topo_to_opensm(port.PortGUID)

    def transform_guid_topo_to_opensm(self, GUID):
        return GUID + 1