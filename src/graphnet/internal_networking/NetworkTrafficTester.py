from internal_networking.Node import *
from internal_networking.NodeMeta import *
from internal_networking.FatTreeAlg import *
from internal_networking.TrafficEntry import *
from internal_networking.ConflictEntry import *

from internal_networking.FatTreeAlg import FatTreeAlg


class NetworkTrafficTester:

    @staticmethod
    def check_linear_shift(network,network_metadata,routing_tables, shift_pattern):
        #create network metadata array to track usage
        conflicts = []

        hcas_n_keepers=shift_pattern
        for shift_distance in range(1, len(hcas_n_keepers)):
            print("TESTING FOR LINEAR SHIFT: ", shift_distance)
            network_usage_list = []
            shift_conflict=False
            #TODO - test for more than one port of the hcas
            for idx,shift_node in enumerate(hcas_n_keepers):
                #if shift_node == "DUMMY":
                if type(shift_node).__name__ == "PlaceKeeper"  or  shift_node == "DUMMY":
                    #print("THIS NODE IS A DUMMY. IGNORING")
                    continue
                shift_node_index = idx
                neighbor_index = (shift_node_index+shift_distance)%len(hcas_n_keepers)
                neighbor=hcas_n_keepers[neighbor_index]
                #if neighbor == "DUMMY":
                if type(neighbor).__name__ == "PlaceKeeper" or  neighbor== "DUMMY":
                    #print("THIS NODE WOULD SEND TO A DUMMY. IGNORING")
                    continue
                local_conflicts = NetworkTrafficTester.simulate_send_to_hca(shift_node,neighbor,
                                                                         network,routing_tables,
                                                                         network_metadata,network_usage_list)
                if local_conflicts!=[]:
                    for conflict in local_conflicts:
                        conflict.shift_distance=shift_distance
                        conflicts.append(conflict)
                    shift_conflict=True
            if shift_conflict==False:
                print("NO CONFLICT FOR SHIFT DISTANCE: ", shift_distance)
            else:
                print("CONFLICT OCCURRENCE FOR SHIFT DISTANCE: ", shift_distance)
        return conflicts

    @staticmethod
    def simulate_send_to_hca(source_node,dest_node,network, routing_tables,network_metadata, network_usage_list):
        conflicts=[]
        if type(source_node).__name__ == "Hca":
            source_GUID = source_node.ports[0].PortGUID
            current_switch=FatTreeAlg.find_node_by_GUID(source_node.ports[0].NeighborGUID, network)
        else:
            source_GUID = source_node.DummyGUID
            current_switch=FatTreeAlg.find_node_by_GUID(source_node.ParentGUID, network)
        if type(dest_node).__name__ == "Hca":
            dest_GUID = dest_node.ports[0].PortGUID
        else:
            dest_GUID = dest_node.DummyGUID
        #simulate routing of data to destination
        #and keep track on used ports on switches
        #current_switch=source

        while True:
            current_switch_lft_entry = RoutingTable.find_lft_dest_entry(routing_tables,current_switch.NodeGUID,dest_GUID)

            hop_remote_GUID=current_switch_lft_entry.next_hop_GUID
            if(hop_remote_GUID==dest_GUID):
                break
            else:
                #create routing entries


                hop_port_no_current_switch=current_switch_lft_entry.local_hop_port_no
                hop_port_no_remote_switch=current_switch_lft_entry.remote_hop_port_no
                #current_switch_GUID=
                #TODO
                local_entry=TrafficEntry(source_GUID=source_GUID,destination_GUID=dest_GUID,
                                     switch_GUID=current_switch.NodeGUID,switch_port_no=hop_port_no_current_switch,
                                         function=Function.SENDING)
                remote_entry=TrafficEntry(source_GUID=source_GUID,destination_GUID=dest_GUID,
                                     switch_GUID=hop_remote_GUID,switch_port_no=hop_port_no_remote_switch,
                                          function=Function.RECEIVING)

                #lid_current_switch = local_entry.NodeLID
                #lid_remote_switch = remote_entry.NodeLID
                lid_current_switch = "NA"
                lid_remote_switch = "NA"

                #print("TRAFFIC: SOURCE", hex(source_GUID), "DEST", hex(dest_GUID),
                #      "LOCAL SW", hex(current_switch.NodeGUID), "LID", lid_current_switch, "PORT", hop_port_no_current_switch,
                #      "REMOTE SW", hex(hop_remote_GUID), "LID", lid_remote_switch,  "PORT", hop_port_no_remote_switch)
                local_used, coflicting_entry_l = TrafficEntry.check_if_port_used(local_entry.switch_GUID,
                                                             local_entry.switch_port_no,network_usage_list,
                                                             Function.SENDING)
                remote_used, coflicting_entry_r = TrafficEntry.check_if_port_used(remote_entry.switch_GUID,
                                                              remote_entry.switch_port_no,network_usage_list,
                                                              Function.RECEIVING)

                current_switch=FatTreeAlg.find_node_by_GUID(hop_remote_GUID, network)
                #check if the entries aren't already in the taken ports.
                if local_used==True:
                    #print ("PORTS IN LOCAL SWITCH ALREADY USED. CONFLICT DETECTED !")
                    conflicting_routes=[]
                    conflicting_routes.append((local_entry.source_GUID, local_entry.destination_GUID))
                    conflicting_routes.append((coflicting_entry_l.source_GUID, coflicting_entry_l.destination_GUID))
                    conflict = ConflictEntry(node=local_entry, nodeGuid=local_entry.switch_GUID, nodePortNo=local_entry.switch_port_no, conflictingRoutes=conflicting_routes)
                    conflicts.append(conflict)

                network_usage_list.append(local_entry)

                if remote_used==True:
                    #print ("PORTS IN REMOTE SWITCH ALREADY USED. CONFLICT DETECTED !")
                    conflicting_routes=[]
                    conflicting_routes.append((remote_entry.source_GUID, remote_entry.destination_GUID))
                    conflicting_routes.append((coflicting_entry_r.source_GUID, coflicting_entry_r.destination_GUID))
                    conflict = ConflictEntry(node=remote_entry, nodeGuid=remote_entry.switch_GUID, nodePortNo=remote_entry.switch_port_no, conflictingRoutes=conflicting_routes)
                    conflicts.append(conflict)

                network_usage_list.append(remote_entry)

        return conflicts

'''
    @staticmethod
    def get_all_hcas_and_keepers(network_metadata):
        hcas_keepers_list =[]
        for meta in network_metadata:
            if type(meta.node).__name__=="Switch":
                for port in meta.node.ports:TESTING FOR LINEAR SHIFT
                        neighbor_meta=FatTreeAlg.find_node_meta_by_GUID(port.NeighborGUID,network_metadata)
                        if type(neighbor_meta.node).__name__=="Hca":
                            hcas_keepers_list.append(neighbor_meta.node)
                        pass
                for keeper in meta.placeKeepers :
                    hcas_keepers_list.append(keeper)
        return hcas_keepers_list
'''