from internal_networking.Hca import *
from internal_networking.Node import *
from internal_networking.Switch import *


class DummyNetworksHolder():

    @staticmethod
    def get_fixed_dummy_test_network():
        network=[]

        ports_node_1=[]

        #print ("PORTS ARRAY LENGTH: ", len(ports_node_1))
        ports_node_1.append(Port(PortNo=len(ports_node_1),PortGUID=10000,NeighborGUID=30000,NeighborPortNo=0))
        node_1=Hca(ports=ports_node_1,max_ports=1)
        network.append(node_1)

        #print ("PORTS ARRAY LENGTH: ", len(ports_node_1))

        ports_node_2 = []
        ports_node_2.append(Port(PortNo=len(ports_node_2),PortGUID=10001,NeighborGUID=30000,NeighborPortNo=1))
        node_2 = Hca(ports=ports_node_2, max_ports=1)
        network.append(node_2)

        ports_node_3 = []
        ports_node_3.append(Port(PortNo=len(ports_node_3),PortGUID=20000,NeighborGUID=40000,NeighborPortNo=0))
        node_3 = Hca(ports=ports_node_3, max_ports=1)
        network.append(node_3)

        ports_node_4 = []
        ports_node_4.append(Port(PortNo=len(ports_node_4),PortGUID=20001,NeighborGUID=40000,NeighborPortNo=1))
        node_4 = Hca(ports=ports_node_4, max_ports=1)
        network.append(node_4)

        ports_node_5 = []
        ports_node_5.append(Port(PortNo=len(ports_node_5),PortGUID=30000,NeighborGUID=10000,NeighborPortNo=0))
        ports_node_5.append(Port(PortNo=len(ports_node_5),PortGUID=30000,NeighborGUID=10001,NeighborPortNo=1))
        ports_node_5.append(Port(PortNo=len(ports_node_5),PortGUID=30000,NeighborGUID=50000,NeighborPortNo=0))
        ports_node_5.append(Port(PortNo=len(ports_node_5),PortGUID=30000,NeighborGUID=50000,NeighborPortNo=2))
        node_5 = Switch(NodeGUID=30000,ports=ports_node_5, max_ports=40)
        network.append(node_5)

        ports_node_6 = []
        ports_node_6.append(Port(PortNo=len(ports_node_6),PortGUID=40000,NeighborGUID=20000,NeighborPortNo=0))
        ports_node_6.append(Port(PortNo=len(ports_node_6),PortGUID=40000,NeighborGUID=20001,NeighborPortNo=1))
        ports_node_6.append(Port(PortNo=len(ports_node_6),PortGUID=40000,NeighborGUID=50000,NeighborPortNo=1))
        ports_node_6.append(Port(PortNo=len(ports_node_6),PortGUID=40000,NeighborGUID=50000,NeighborPortNo=3))
        node_6 = Switch(NodeGUID=40000,ports=ports_node_6, max_ports=40)
        network.append(node_6)

    #spine
        ports_node_7 = []
        ports_node_7.append(Port(PortNo=len(ports_node_7),PortGUID=50000,NeighborGUID=30000,NeighborPortNo=2))
        ports_node_7.append(Port(PortNo=len(ports_node_7),PortGUID=50000,NeighborGUID=40000,NeighborPortNo=2))
        ports_node_7.append(Port(PortNo=len(ports_node_7),PortGUID=50000,NeighborGUID=30000,NeighborPortNo=3))
        ports_node_7.append(Port(PortNo=len(ports_node_7),PortGUID=50000,NeighborGUID=40000,NeighborPortNo=3))
        node_7 = Switch(NodeGUID=50000,ports=ports_node_7, max_ports=40)
        network.append(node_7)

        return network

    @staticmethod
    def get_fixed_incomplete_dummy_test_network():
        network=[]

        ports_node_1=[]

        #print ("PORTS ARRAY LENGTH: ", len(ports_node_1))
        ports_node_1.append(Port(PortNo=len(ports_node_1),PortGUID=10000,NeighborGUID=30000,NeighborPortNo=0))
        node_1=Hca(ports=ports_node_1,max_ports=1)
        network.append(node_1)

        #print ("PORTS ARRAY LENGTH: ", len(ports_node_1))

        ports_node_2 = []
        ports_node_2.append(Port(PortNo=len(ports_node_2),PortGUID=10001,NeighborGUID=30000,NeighborPortNo=1))
        node_2 = Hca(ports=ports_node_2, max_ports=1)
        network.append(node_2)

        ports_node_3 = []
        ports_node_3.append(Port(PortNo=len(ports_node_3),PortGUID=20000,NeighborGUID=40000,NeighborPortNo=0))
        node_3 = Hca(ports=ports_node_3, max_ports=1)
        network.append(node_3)


        ports_node_5 = []
        ports_node_5.append(Port(PortNo=len(ports_node_5),PortGUID=30000,NeighborGUID=10000,NeighborPortNo=0))
        ports_node_5.append(Port(PortNo=len(ports_node_5),PortGUID=30000,NeighborGUID=10001,NeighborPortNo=1))
        ports_node_5.append(Port(PortNo=len(ports_node_5),PortGUID=30000,NeighborGUID=50000,NeighborPortNo=0))
        ports_node_5.append(Port(PortNo=len(ports_node_5),PortGUID=30000,NeighborGUID=50000,NeighborPortNo=2))
        node_5 = Switch(NodeGUID=30000,ports=ports_node_5, max_ports=40)
        network.append(node_5)

        ports_node_6 = []
        ports_node_6.append(Port(PortNo=len(ports_node_6),PortGUID=40000,NeighborGUID=20000,NeighborPortNo=0))
        ports_node_6.append(Port(PortNo=2,PortGUID=40000,NeighborGUID=50000,NeighborPortNo=1))
        ports_node_6.append(Port(PortNo=3,PortGUID=40000,NeighborGUID=50000,NeighborPortNo=3))
        node_6 = Switch(NodeGUID=40000,ports=ports_node_6, max_ports=40)
        network.append(node_6)

    #spine
        ports_node_7 = []
        ports_node_7.append(Port(PortNo=len(ports_node_7),PortGUID=50000,NeighborGUID=30000,NeighborPortNo=2))
        ports_node_7.append(Port(PortNo=len(ports_node_7),PortGUID=50000,NeighborGUID=40000,NeighborPortNo=2))
        ports_node_7.append(Port(PortNo=len(ports_node_7),PortGUID=50000,NeighborGUID=30000,NeighborPortNo=3))
        ports_node_7.append(Port(PortNo=len(ports_node_7),PortGUID=50000,NeighborGUID=40000,NeighborPortNo=3))
        node_7 = Switch(NodeGUID=50000,ports=ports_node_7, max_ports=40)
        network.append(node_7)

        return network

    @staticmethod
    def get_fixed_dummy_test_network_large():
        network=[]

#HCAs
        ports_node_0=[]
        ports_node_0.append(Port(PortNo=len(ports_node_0),PortGUID=10000,NeighborGUID=20000,NeighborPortNo=0))
        node_0=Hca(ports=ports_node_0,max_ports=1)
        network.append(node_0)

        ports_node_1=[]
        ports_node_1.append(Port(PortNo=len(ports_node_1),PortGUID=10001,NeighborGUID=20000,NeighborPortNo=1))
        node_1=Hca(ports=ports_node_1,max_ports=1)
        network.append(node_1)

        ports_node_2=[]
        ports_node_2.append(Port(PortNo=len(ports_node_2),PortGUID=10002,NeighborGUID=20000,NeighborPortNo=2))
        node_2=Hca(ports=ports_node_2,max_ports=1)
        network.append(node_2)

        ports_node_3=[]
        ports_node_3.append(Port(PortNo=len(ports_node_3),PortGUID=10003,NeighborGUID=20000,NeighborPortNo=3))
        node_3=Hca(ports=ports_node_3,max_ports=1)
        network.append(node_3)
###
        ports_node_4=[]
        ports_node_4.append(Port(PortNo=len(ports_node_4),PortGUID=11000,NeighborGUID=21000,NeighborPortNo=0))
        node_4=Hca(ports=ports_node_4,max_ports=1)
        network.append(node_4)

        ports_node_5=[]
        ports_node_5.append(Port(PortNo=len(ports_node_5),PortGUID=11001,NeighborGUID=21000,NeighborPortNo=1))
        node_5=Hca(ports=ports_node_5,max_ports=1)
        network.append(node_5)

        ports_node_6=[]
        ports_node_6.append(Port(PortNo=len(ports_node_6),PortGUID=11002,NeighborGUID=21000,NeighborPortNo=2))
        node_6=Hca(ports=ports_node_6,max_ports=1)
        network.append(node_6)

        ports_node_7=[]
        ports_node_7.append(Port(PortNo=len(ports_node_7),PortGUID=11003,NeighborGUID=21000,NeighborPortNo=3))
        node_7=Hca(ports=ports_node_7,max_ports=1)
        network.append(node_7)
###
        ports_node_8=[]
        ports_node_8.append(Port(PortNo=len(ports_node_8),PortGUID=12000,NeighborGUID=22000,NeighborPortNo=0))
        node_8=Hca(ports=ports_node_8,max_ports=1)
        network.append(node_8)

        ports_node_9=[]
        ports_node_9.append(Port(PortNo=len(ports_node_9),PortGUID=12001,NeighborGUID=22000,NeighborPortNo=1))
        node_9=Hca(ports=ports_node_9,max_ports=1)
        network.append(node_9)

        ports_node_10=[]
        ports_node_10.append(Port(PortNo=len(ports_node_10),PortGUID=12002,NeighborGUID=22000,NeighborPortNo=2))
        node_10=Hca(ports=ports_node_10,max_ports=1)
        network.append(node_10)

        ports_node_11=[]
        ports_node_11.append(Port(PortNo=len(ports_node_11),PortGUID=12003,NeighborGUID=22000,NeighborPortNo=3))
        node_11=Hca(ports=ports_node_11,max_ports=1)
        network.append(node_11)
###
        ports_node_12=[]
        ports_node_12.append(Port(PortNo=len(ports_node_12),PortGUID=13000,NeighborGUID=23000,NeighborPortNo=0))
        node_12=Hca(ports=ports_node_12,max_ports=1)
        network.append(node_12)

        ports_node_13=[]
        ports_node_13.append(Port(PortNo=len(ports_node_13),PortGUID=13001,NeighborGUID=23000,NeighborPortNo=1))
        node_13=Hca(ports=ports_node_13,max_ports=1)
        network.append(node_13)

        ports_node_14=[]
        ports_node_14.append(Port(PortNo=len(ports_node_14),PortGUID=13002,NeighborGUID=23000,NeighborPortNo=2))
        node_14=Hca(ports=ports_node_14,max_ports=1)
        network.append(node_14)

        ports_node_15=[]
        ports_node_15.append(Port(PortNo=len(ports_node_15),PortGUID=13003,NeighborGUID=23000,NeighborPortNo=3))
        node_15=Hca(ports=ports_node_15,max_ports=1)
        network.append(node_15)
#### leaf switches

        ports_node_16=[]
        ports_node_16.append(Port(PortNo=len(ports_node_16),PortGUID=20000,NeighborGUID=10000,NeighborPortNo=0))
        ports_node_16.append(Port(PortNo=len(ports_node_16),PortGUID=20000,NeighborGUID=10001,NeighborPortNo=0))
        ports_node_16.append(Port(PortNo=len(ports_node_16),PortGUID=20000,NeighborGUID=10002,NeighborPortNo=0))
        ports_node_16.append(Port(PortNo=len(ports_node_16),PortGUID=20000,NeighborGUID=10003,NeighborPortNo=0))

        ports_node_16.append(Port(PortNo=len(ports_node_16), PortGUID=20000, NeighborGUID=30000, NeighborPortNo=0))
        ports_node_16.append(Port(PortNo=len(ports_node_16), PortGUID=20000, NeighborGUID=31000, NeighborPortNo=0))
        ports_node_16.append(Port(PortNo=len(ports_node_16), PortGUID=20000, NeighborGUID=30000, NeighborPortNo=4))
        ports_node_16.append(Port(PortNo=len(ports_node_16), PortGUID=20000, NeighborGUID=31000, NeighborPortNo=4))

        node_16 = Switch(NodeGUID=20000, ports=ports_node_16, max_ports=40)
        network.append(node_16)
###
        ports_node_17=[]
        ports_node_17.append(Port(PortNo=len(ports_node_17),PortGUID=21000,NeighborGUID=11000,NeighborPortNo=0))
        ports_node_17.append(Port(PortNo=len(ports_node_17),PortGUID=21000,NeighborGUID=11001,NeighborPortNo=0))
        ports_node_17.append(Port(PortNo=len(ports_node_17),PortGUID=21000,NeighborGUID=11002,NeighborPortNo=0))
        ports_node_17.append(Port(PortNo=len(ports_node_17),PortGUID=21000,NeighborGUID=11003,NeighborPortNo=0))

        ports_node_17.append(Port(PortNo=len(ports_node_17), PortGUID=21000, NeighborGUID=30000, NeighborPortNo=1))
        ports_node_17.append(Port(PortNo=len(ports_node_17), PortGUID=21000, NeighborGUID=31000, NeighborPortNo=1))
        ports_node_17.append(Port(PortNo=len(ports_node_17), PortGUID=21000, NeighborGUID=30000, NeighborPortNo=5))
        ports_node_17.append(Port(PortNo=len(ports_node_17), PortGUID=21000, NeighborGUID=31000, NeighborPortNo=5))

        node_17 = Switch(NodeGUID=21000, ports=ports_node_17, max_ports=40)
        network.append(node_17)
###
        ports_node_18=[]
        ports_node_18.append(Port(PortNo=len(ports_node_18),PortGUID=22000,NeighborGUID=12000,NeighborPortNo=0))
        ports_node_18.append(Port(PortNo=len(ports_node_18),PortGUID=22000,NeighborGUID=12001,NeighborPortNo=0))
        ports_node_18.append(Port(PortNo=len(ports_node_18),PortGUID=22000,NeighborGUID=12002,NeighborPortNo=0))
        ports_node_18.append(Port(PortNo=len(ports_node_18),PortGUID=22000,NeighborGUID=12003,NeighborPortNo=0))
        node_18 = Switch(NodeGUID=22000, ports=ports_node_18, max_ports=40)

        ports_node_18.append(Port(PortNo=len(ports_node_18), PortGUID=22000, NeighborGUID=30000, NeighborPortNo=2))
        ports_node_18.append(Port(PortNo=len(ports_node_18), PortGUID=22000, NeighborGUID=31000, NeighborPortNo=2))
        ports_node_18.append(Port(PortNo=len(ports_node_18), PortGUID=22000, NeighborGUID=30000, NeighborPortNo=6))
        ports_node_18.append(Port(PortNo=len(ports_node_18), PortGUID=22000, NeighborGUID=31000, NeighborPortNo=6))

        network.append(node_18)

        ports_node_19=[]
        ports_node_19.append(Port(PortNo=len(ports_node_19),PortGUID=23000,NeighborGUID=13000,NeighborPortNo=0))
        ports_node_19.append(Port(PortNo=len(ports_node_19),PortGUID=23000,NeighborGUID=13001,NeighborPortNo=0))
        ports_node_19.append(Port(PortNo=len(ports_node_19),PortGUID=23000,NeighborGUID=13002,NeighborPortNo=0))
        ports_node_19.append(Port(PortNo=len(ports_node_19),PortGUID=23000,NeighborGUID=13003,NeighborPortNo=0))

        ports_node_19.append(Port(PortNo=len(ports_node_19), PortGUID=23000, NeighborGUID=30000, NeighborPortNo=3))
        ports_node_19.append(Port(PortNo=len(ports_node_19), PortGUID=23000, NeighborGUID=31000, NeighborPortNo=3))
        ports_node_19.append(Port(PortNo=len(ports_node_19), PortGUID=23000, NeighborGUID=30000, NeighborPortNo=7))
        ports_node_19.append(Port(PortNo=len(ports_node_19), PortGUID=23000, NeighborGUID=31000, NeighborPortNo=7))

        node_19 = Switch(NodeGUID=23000, ports=ports_node_19, max_ports=40)

        network.append(node_19)
###spine switches


        ports_node_20=[]
        ports_node_20.append(Port(PortNo=len(ports_node_20),PortGUID=30000,NeighborGUID=20000,NeighborPortNo=4))
        ports_node_20.append(Port(PortNo=len(ports_node_20),PortGUID=30000,NeighborGUID=21000,NeighborPortNo=4))
        ports_node_20.append(Port(PortNo=len(ports_node_20),PortGUID=30000,NeighborGUID=22000,NeighborPortNo=4))
        ports_node_20.append(Port(PortNo=len(ports_node_20),PortGUID=30000,NeighborGUID=23000,NeighborPortNo=4))

        ports_node_20.append(Port(PortNo=len(ports_node_20), PortGUID=30000, NeighborGUID=20000, NeighborPortNo=6))
        ports_node_20.append(Port(PortNo=len(ports_node_20), PortGUID=30000, NeighborGUID=21000, NeighborPortNo=6))
        ports_node_20.append(Port(PortNo=len(ports_node_20), PortGUID=30000, NeighborGUID=22000, NeighborPortNo=6))
        ports_node_20.append(Port(PortNo=len(ports_node_20), PortGUID=30000, NeighborGUID=23000, NeighborPortNo=6))

        node_20 = Switch(NodeGUID=30000, ports=ports_node_20, max_ports=40)

        network.append(node_20)


        ports_node_21=[]
        ports_node_21.append(Port(PortNo=len(ports_node_21),PortGUID=31000,NeighborGUID=20000,NeighborPortNo=5))
        ports_node_21.append(Port(PortNo=len(ports_node_21),PortGUID=31000,NeighborGUID=21000,NeighborPortNo=5))
        ports_node_21.append(Port(PortNo=len(ports_node_21),PortGUID=31000,NeighborGUID=22000,NeighborPortNo=5))
        ports_node_21.append(Port(PortNo=len(ports_node_21),PortGUID=31000,NeighborGUID=23000,NeighborPortNo=5))

        ports_node_21.append(Port(PortNo=len(ports_node_21), PortGUID=31000, NeighborGUID=20000, NeighborPortNo=7))
        ports_node_21.append(Port(PortNo=len(ports_node_21), PortGUID=31000, NeighborGUID=21000, NeighborPortNo=7))
        ports_node_21.append(Port(PortNo=len(ports_node_21), PortGUID=31000, NeighborGUID=22000, NeighborPortNo=7))
        ports_node_21.append(Port(PortNo=len(ports_node_21), PortGUID=31000, NeighborGUID=23000, NeighborPortNo=7))

        node_21 = Switch(NodeGUID=31000, ports=ports_node_21, max_ports=40)

        network.append(node_21)

        return network

    @staticmethod
    def get_fixed_dummy_test_network_large_incomplete():
        network = []

        # 2 HCAs for the first leaf switch
        ports_node_0 = []
        ports_node_0.append(Port(PortNo=len(ports_node_0), PortGUID=10000, NeighborGUID=20000, NeighborPortNo=0))
        node_0 = Hca(ports=ports_node_0, max_ports=1)
        network.append(node_0)

        ports_node_1 = []
        ports_node_1.append(Port(PortNo=len(ports_node_1), PortGUID=10001, NeighborGUID=20000, NeighborPortNo=1))
        node_1 = Hca(ports=ports_node_1, max_ports=1)
        network.append(node_1)

        ###4 HCAs on leaf 2
        ports_node_4 = []
        ports_node_4.append(Port(PortNo=len(ports_node_4), PortGUID=11000, NeighborGUID=21000, NeighborPortNo=0))
        node_4 = Hca(ports=ports_node_4, max_ports=1)
        network.append(node_4)

        ports_node_5 = []
        ports_node_5.append(Port(PortNo=len(ports_node_5), PortGUID=11001, NeighborGUID=21000, NeighborPortNo=1))
        node_5 = Hca(ports=ports_node_5, max_ports=1)
        network.append(node_5)

        ports_node_6 = []
        ports_node_6.append(Port(PortNo=len(ports_node_6), PortGUID=11002, NeighborGUID=21000, NeighborPortNo=2))
        node_6 = Hca(ports=ports_node_6, max_ports=1)
        network.append(node_6)

        ports_node_7 = []
        ports_node_7.append(Port(PortNo=len(ports_node_7), PortGUID=11003, NeighborGUID=21000, NeighborPortNo=3))
        node_7 = Hca(ports=ports_node_7, max_ports=1)
        network.append(node_7)
        ### 1 HCA on leaf 3

        ports_node_11 = []
        ports_node_11.append(Port(PortNo=len(ports_node_11), PortGUID=12003, NeighborGUID=22000, NeighborPortNo=3))
        node_11 = Hca(ports=ports_node_11, max_ports=1)
        network.append(node_11)

        ### 3 HCAs on leaf 4
        ports_node_12 = []
        ports_node_12.append(Port(PortNo=len(ports_node_12), PortGUID=13000, NeighborGUID=23000, NeighborPortNo=0))
        node_12 = Hca(ports=ports_node_12, max_ports=1)
        network.append(node_12)

        ports_node_14 = []
        ports_node_14.append(Port(PortNo=len(ports_node_14), PortGUID=13002, NeighborGUID=23000, NeighborPortNo=2))
        node_14 = Hca(ports=ports_node_14, max_ports=1)
        network.append(node_14)

        ports_node_15 = []
        ports_node_15.append(Port(PortNo=len(ports_node_15), PortGUID=13003, NeighborGUID=23000, NeighborPortNo=3))
        node_15 = Hca(ports=ports_node_15, max_ports=1)
        network.append(node_15)
        #### leaf switches

        ports_node_16 = []
        ports_node_16.append(Port(PortNo=0, PortGUID=20000, NeighborGUID=10000, NeighborPortNo=0))
        ports_node_16.append(Port(PortNo=1, PortGUID=20000, NeighborGUID=10001, NeighborPortNo=0))

        ports_node_16.append(Port(PortNo=4, PortGUID=20000, NeighborGUID=30000, NeighborPortNo=0))
        ports_node_16.append(Port(PortNo=5, PortGUID=20000, NeighborGUID=31000, NeighborPortNo=0))
        ports_node_16.append(Port(PortNo=6, PortGUID=20000, NeighborGUID=30000, NeighborPortNo=4))
        ports_node_16.append(Port(PortNo=7, PortGUID=20000, NeighborGUID=31000, NeighborPortNo=4))

        node_16 = Switch(NodeGUID=20000, ports=ports_node_16, max_ports=40)
        network.append(node_16)
        ###
        ports_node_17 = []
        ports_node_17.append(Port(PortNo=len(ports_node_17), PortGUID=21000, NeighborGUID=11000, NeighborPortNo=0))
        ports_node_17.append(Port(PortNo=len(ports_node_17), PortGUID=21000, NeighborGUID=11001, NeighborPortNo=0))
        ports_node_17.append(Port(PortNo=len(ports_node_17), PortGUID=21000, NeighborGUID=11002, NeighborPortNo=0))
        ports_node_17.append(Port(PortNo=len(ports_node_17), PortGUID=21000, NeighborGUID=11003, NeighborPortNo=0))

        ports_node_17.append(Port(PortNo=len(ports_node_17), PortGUID=21000, NeighborGUID=30000, NeighborPortNo=1))
        ports_node_17.append(Port(PortNo=len(ports_node_17), PortGUID=21000, NeighborGUID=31000, NeighborPortNo=1))
        ports_node_17.append(Port(PortNo=len(ports_node_17), PortGUID=21000, NeighborGUID=30000, NeighborPortNo=5))
        ports_node_17.append(Port(PortNo=len(ports_node_17), PortGUID=21000, NeighborGUID=31000, NeighborPortNo=5))

        node_17 = Switch(NodeGUID=21000, ports=ports_node_17, max_ports=40)
        network.append(node_17)
        ###
        ports_node_18 = []
        ports_node_18.append(Port(PortNo=3, PortGUID=22000, NeighborGUID=12003, NeighborPortNo=0))
        node_18 = Switch(NodeGUID=22000, ports=ports_node_18, max_ports=40)

        ports_node_18.append(Port(PortNo=4, PortGUID=22000, NeighborGUID=30000, NeighborPortNo=2))
        ports_node_18.append(Port(PortNo=5, PortGUID=22000, NeighborGUID=31000, NeighborPortNo=2))
        ports_node_18.append(Port(PortNo=6, PortGUID=22000, NeighborGUID=30000, NeighborPortNo=6))
        ports_node_18.append(Port(PortNo=7, PortGUID=22000, NeighborGUID=31000, NeighborPortNo=6))

        network.append(node_18)

        ports_node_19 = []
        ports_node_19.append(Port(PortNo=0, PortGUID=23000, NeighborGUID=13000, NeighborPortNo=0))
        ports_node_19.append(Port(PortNo=2, PortGUID=23000, NeighborGUID=13002, NeighborPortNo=0))
        ports_node_19.append(Port(PortNo=3, PortGUID=23000, NeighborGUID=13003, NeighborPortNo=0))

        ports_node_19.append(Port(PortNo=4, PortGUID=23000, NeighborGUID=30000, NeighborPortNo=3))
        ports_node_19.append(Port(PortNo=5, PortGUID=23000, NeighborGUID=31000, NeighborPortNo=3))
        ports_node_19.append(Port(PortNo=6, PortGUID=23000, NeighborGUID=30000, NeighborPortNo=7))
        ports_node_19.append(Port(PortNo=7, PortGUID=23000, NeighborGUID=31000, NeighborPortNo=7))

        node_19 = Switch(NodeGUID=23000, ports=ports_node_19, max_ports=40)

        network.append(node_19)
        ###spine switches

        ports_node_20 = []
        ports_node_20.append(Port(PortNo=len(ports_node_20), PortGUID=30000, NeighborGUID=20000, NeighborPortNo=4))
        ports_node_20.append(Port(PortNo=len(ports_node_20), PortGUID=30000, NeighborGUID=21000, NeighborPortNo=4))
        ports_node_20.append(Port(PortNo=len(ports_node_20), PortGUID=30000, NeighborGUID=22000, NeighborPortNo=4))
        ports_node_20.append(Port(PortNo=len(ports_node_20), PortGUID=30000, NeighborGUID=23000, NeighborPortNo=4))

        ports_node_20.append(Port(PortNo=len(ports_node_20), PortGUID=30000, NeighborGUID=20000, NeighborPortNo=6))
        ports_node_20.append(Port(PortNo=len(ports_node_20), PortGUID=30000, NeighborGUID=21000, NeighborPortNo=6))
        ports_node_20.append(Port(PortNo=len(ports_node_20), PortGUID=30000, NeighborGUID=22000, NeighborPortNo=6))
        ports_node_20.append(Port(PortNo=len(ports_node_20), PortGUID=30000, NeighborGUID=23000, NeighborPortNo=6))

        node_20 = Switch(NodeGUID=30000, ports=ports_node_20, max_ports=40)

        network.append(node_20)

        ports_node_21 = []
        ports_node_21.append(Port(PortNo=len(ports_node_21), PortGUID=31000, NeighborGUID=20000, NeighborPortNo=5))
        ports_node_21.append(Port(PortNo=len(ports_node_21), PortGUID=31000, NeighborGUID=21000, NeighborPortNo=5))
        ports_node_21.append(Port(PortNo=len(ports_node_21), PortGUID=31000, NeighborGUID=22000, NeighborPortNo=5))
        ports_node_21.append(Port(PortNo=len(ports_node_21), PortGUID=31000, NeighborGUID=23000, NeighborPortNo=5))

        ports_node_21.append(Port(PortNo=len(ports_node_21), PortGUID=31000, NeighborGUID=20000, NeighborPortNo=7))
        ports_node_21.append(Port(PortNo=len(ports_node_21), PortGUID=31000, NeighborGUID=21000, NeighborPortNo=7))
        ports_node_21.append(Port(PortNo=len(ports_node_21), PortGUID=31000, NeighborGUID=22000, NeighborPortNo=7))
        ports_node_21.append(Port(PortNo=len(ports_node_21), PortGUID=31000, NeighborGUID=23000, NeighborPortNo=7))

        node_21 = Switch(NodeGUID=31000, ports=ports_node_21, max_ports=40)

        network.append(node_21)

        return network