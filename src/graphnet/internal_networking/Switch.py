from internal_networking.Node import *


class Switch(Node):
    def __init__(self, node_guid, ports=[], max_ports=40, is_root=False):
        Node.__init__(self,ports,max_ports)
        self.NodeGUID=node_guid
        self.NodeLID=-1
        self.is_root = is_root

    def set_root_switch(self, is_root):
        self.is_root = is_root

    def is_root_switch(self):
        return self.is_root