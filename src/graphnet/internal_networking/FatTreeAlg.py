from internal_networking.NodeMeta import *
from internal_networking.Switch import *
from internal_networking.RoutingTable import *

from internal_networking.RoutingTableEntry import *
from internal_networking.PortMetaGroup import *
from internal_networking.PlaceKeeper import *
from internal_networking.IO import *

class FatTreeAlg:
    @staticmethod
    def get_meta(network, spine_switches_GUID_list=[]):
        #max_hcas_per_switch=0
        network_metadata=[]
        for node in network :
            isSwitch=(type(node).__name__=="Switch")
            if isSwitch:
                print("FOUND SWITCH. ITS GUID:")
                #TODO - replace with cast
                print(node.NodeGUID)

            isSpine=False
            level=-1
            # TODO - replace with cast
            if isSwitch and node.NodeGUID in spine_switches_GUID_list:
                print("FOUND SPINE")
                isSpine=True
                level=0


            metaEntry=NodeMeta(node,isSpine=isSpine,level=level)
            network_metadata.append(metaEntry)
        #spines switches are found and level seto to 0 at this point
        #now set the other nodes
        FatTreeAlg.set_nodes_levels(network_metadata)
        FatTreeAlg.set_neighbors_ports_levels(network_metadata)
        max_hcas_per_switch=FatTreeAlg.get_max_hcas_per_switch(network_metadata)

        return network_metadata, max_hcas_per_switch

    @staticmethod
    def set_nodes_levels(network_metadata):
        for meta_node in network_metadata :
            if meta_node.isSpine :
                level_1=1
                print("GOING THROUGH SWITCHES")
                for neighbor in meta_node.node.ports :
                    if type(meta_node.node).__name__ != "Switch":
                        print ("ERROR. 1st level device should always be the switch")
                    print("Spine Neighbor GUID:", neighbor.NeighborGUID, " and port no ", neighbor.NeighborPortNo )
                    print("Setting switch Level :", level_1)
                    neighbor_meta_node=FatTreeAlg.find_node_meta_by_GUID(neighbor.NeighborGUID,network_metadata)
                    neighbor_meta_node.level=level_1
#Now go for the 2nd level nodes which should be HCAs
                print("GOING THROUGH HCAs")
                for neighbor in meta_node.node.ports :
                    neighbor_meta_node=FatTreeAlg.find_node_meta_by_GUID(neighbor.NeighborGUID,network_metadata)
                    for second_neighbor in neighbor_meta_node.node.ports :
                        print("LOOKING FOR AN HCA WITH GUID: ", second_neighbor.NeighborGUID,
                              "(HEX)", hex(second_neighbor.NeighborGUID))
                        second_neighbor_meta_node=FatTreeAlg.find_node_meta_by_GUID(second_neighbor.NeighborGUID, network_metadata)
                        if type(second_neighbor_meta_node.node).__name__=="Hca" and second_neighbor_meta_node.level==(-1):
                            print("SETTING HCA LEVEL 2")
                            second_neighbor_meta_node.level=2
                    #neighbor_node=next(obj for obj in network_metadata if type(obj.node).__name__=="Switch" if obj.node.NodeGUID==neighbor.NeighborGUID)
                    #for hca_port in neighbor_node.ports:
                    #    pass
                        #TODO - CHECK IF EMPTY
                        #hca_node= next(obj for obj in network_metadata if obj.node.==hca_port.NeighborGUID if obj.level==-1)
                        #if type(hca_node.node).__name__ != "Hca":
                        #    print("ERROR. 2nd level device should always be the HCA")


    @staticmethod
    def set_neighbors_ports_levels(network_metadata):
        for meta_node in network_metadata :
            for port_meta in meta_node.ports_meta_list :
                n_guid = port_meta.port.NeighborGUID
                n_meta=FatTreeAlg.find_node_meta_by_GUID(n_guid, network_metadata)
                n_level = n_meta.level
                port_meta.neighbor_level = n_level


    @staticmethod
    def set_network_LIDs(network):
        next_lid=1
        for node in network:
            if type(node).__name__=="Switch" :
                node.NodeLID=next_lid
                for port in node.ports:
                    port.PortLID=next_lid
                next_lid+=1
            if type(node).__name__ == "Hca":
                for port in node.ports :
                    port.PortLID=next_lid
                    next_lid+=1

    @staticmethod
    # find neughbor node of a port in metadata
    def find_node_meta_by_GUID(searchedGUID, network_metadata):
 # check switches first
        for meta_node in network_metadata :
            if type(meta_node.node).__name__=="Switch" and meta_node.node.NodeGUID == searchedGUID :
                #print("FOUND A SWITCH WITH A SOUGHT GUID: ", searchedGUID)
                return meta_node
        for meta_node in network_metadata :
            if type(meta_node.node).__name__ == "Hca" :
                for port in meta_node.node.ports :
                    if port.PortGUID == searchedGUID :
                        #print("FOUND AN HCA WITH SOUGHT GUID: ", searchedGUID)
                        return meta_node
        print("NO NODE FOUND")
        return None

    @staticmethod
    # find neughbor node of a port in metadata
    def find_node_by_GUID(searchedGUID, network):
        # check switches first
        for node in network:
            if type(node).__name__ == "Switch" and node.NodeGUID == searchedGUID:
                #print("FOUND A SWITCH WITH A SOUGHT GUID: ", searchedGUID)
                return node
        for node in network:
            if type(node).__name__ == "Hca":
                for port in node.ports:
                    if port.PortGUID == searchedGUID:
                        #print("FOUND AN HCA WITH SOUGHT GUID: ", searchedGUID)
                        return node
        print("NO NODE FOUND")
        return None

    @staticmethod
    def print_meta(network_metadata):
        for index,meta_node in enumerate(network_metadata):
            print("Node metadata entry no", index)
            print("Type:", meta_node.type)
            print("Is spine:", meta_node.isSpine)
            print("Level:", meta_node.level)
            if type(meta_node.node).__name__=="Switch" :
                print("SwitchNode  LID: ", meta_node.node.NodeLID)
                print("Number of place keepers : ",len(meta_node.placeKeepers))
                for keeper in meta_node.placeKeepers:
                    print ("keeper entry for GUID: ", keeper.ParentGUID,
                           " dummy GUID: ", keeper.DummyGUID,
                           " dummy LID: ", keeper.DummyLID)

            if type(meta_node.node).__name__=="Hca" :
                for port in meta_node.node.ports :
                    print("HCA Port LID: ", port.PortLID)


    @staticmethod
    def do_fat_tree_routing(network, spine_switches_GUID_list=[]):
        print("******do_fat_tree_routing-STARTING ALGORITHM")
        print("******do_fat_tree_routing-CALLING get_meta")
        all_meta, max_hcas_per_switch=FatTreeAlg.get_meta(network,spine_switches_GUID_list)
        FatTreeAlg.set_place_keepers(all_meta,max_hcas_per_switch)
        print("max hcas per switch",max_hcas_per_switch )
        print("******do_fat_tree_routing-CALLING print_meta")
        FatTreeAlg.print_meta(all_meta)

        print("******do_fat_tree_routing-GENERATING empty routing table")

        routing_tables=[]
        for node_meta in all_meta :
            if type(node_meta.node).__name__=="Switch":
                Table = RoutingTable(node_meta.node.NodeGUID)
                routing_tables.append(Table)

        print("******do_fat_tree_routing-CALLING main_algorithm")
        FatTreeAlg.main_algorithm(network, all_meta, routing_tables)
        return routing_tables, all_meta


    @staticmethod
    def main_algorithm(network, networkMetadata, routingTables):
        #foreach leaf switch
        for meta_entry in networkMetadata:
            if type(meta_entry.node).__name__=="Switch" and meta_entry.level==1 :
                print("found a leaf switch")
                #foreach compute node
                for switch_port in meta_entry.node.ports:
                    # If neighbor is an HCA
                    hca_meta=FatTreeAlg.find_node_meta_by_GUID(switch_port.NeighborGUID,networkMetadata)
                    if type(hca_meta.node).__name__=="Hca":
                        # Obtain the LID of the compute node
                        hca_port = hca_meta.node.get_port_by_guid(switch_port.NeighborGUID)
                        hca_lid=hca_port.PortLID
                        hca_guid=hca_port.PortGUID
                        #print("Found an HCA with a port LID: ",hca_lid, "and GID: ", "hca_gid",hca_guid)
                        # Set local LFT(LID) of the port connecting to compute node

                        # (Get switch connected to the HCA)

                        lft_entry = RoutingTableEntry(destination_GUID=hca_guid,destination_LID=hca_lid,
                                                      next_hop_GUID=hca_guid,next_hop_LID=hca_lid,
                                                      local_hop_port=switch_port, local_hop_port_no=switch_port.PortNo, remote_hop_port_no=hca_port.PortNo)
                        lft = RoutingTable.find_switch_lft_by_guid(routingTables, meta_entry.node.NodeGUID)
                        #print("appending table entry in main algorithm for node GID: ",meta_entry.node.NodeGUID, " next hop GUID: ",
                        #      hca_guid, "DEST GUID: ", hca_guid,
                        #      " local port hop: ", lft_entry.local_hop_port_no)
                        lft.routingTableEntries.append(lft_entry)
                        meta_port = meta_entry.find_meta_by_port(switch_port)
                        meta_port.usedForMeta=True
                        #call assign-down-going-port-by-ascending
                        if meta_entry.isSpine == False:
                            print("*********main_algorithm-CALLING assign_down_going_port_by_ascending")
                            FatTreeAlg.assign_down_going_port_by_ascending(network,networkMetadata,routingTables, hca_guid,hca_lid, meta_entry)
                    else:
                        continue
                #now do the same thing for the place keepers
                for keeper in meta_entry.placeKeepers:
                    # (Get switch connected to the HCA)
                    lft_entry = RoutingTableEntry(destination_GUID=keeper.DummyGUID,destination_LID=keeper.DummyLID,
                                                  next_hop_GUID=keeper.DummyGUID,next_hop_LID=keeper.DummyLID,
                                                  local_hop_port=-1, local_hop_port_no=-1, remote_hop_port_no=-1,
                                                  is_for_place_keeper=True, keeper_switch_GUID=meta_entry.node.NodeGUID)
                    lft = RoutingTable.find_switch_lft_by_guid(routingTables, meta_entry.node.NodeGUID)
                    #print("appending table entry in main algorithm for KEEPER for GID: ",
                    #      meta_entry.node.NodeGUID,
                    #      " next hop GUID: ", keeper.DummyGUID,
                    #      " DEST GUID: ", keeper.DummyGUID,
                    #      " local port hop: ", lft_entry.local_hop_port_no)
                    lft.routingTableEntries.append(lft_entry)
                    #meta_port = meta_entry.find_meta_by_port(switch_port)
                    #meta_port.usedForMeta=True
                    ##call assign-down-going-port-by-ascending
                    if meta_entry.isSpine == False:
                        print("*********main_algorithm-CALLING assign_down_going_port_by_ascending")
                        FatTreeAlg.assign_down_going_port_by_ascending(network,networkMetadata,routingTables,
                                                                       keeper.DummyGUID,keeper.DummyLID, meta_entry,True)
                    pass

        pass
    @staticmethod
    def assign_up_going_port_by_descending(network, networkMetadata,
                                           routingTables, destinationGuid,
                                           destinationLid, switchMeta,
                                           is_place_keeper=False):
        #print("IN assign_up_going_port_by_descending for the switch GUID", switchMeta.node.NodeGUID)
        #find groups
        downlink_port_meta_groups = FatTreeAlg.get_downlink_port_meta_groups(networkMetadata, switchMeta)
        #foreach down-going-port-group
        for meta_group in downlink_port_meta_groups:

            #skip this group if the LFT(LID) port is part of this group
            assigned_port, assigned_port_no = FatTreeAlg.get_lft_lid_port(routingTables, switchMeta, destinationGuid, destinationLid)
            if assigned_port_no != -1:
                link_in_group=False
                for port_meta in meta_group.ports_metas:
                    if assigned_port is port_meta.port:
                        link_in_group=True
                if link_in_group==True:
                    #print("Port of this group already in the LID, skipping")
                    continue

            #skip this group if the remote node is a compute node
            if len(meta_group.ports_metas) == 1 :
                neighbor_guid = meta_group.ports_metas[0].port.NeighborGUID
                neighbor_meta = FatTreeAlg.find_node_meta_by_GUID(neighbor_guid, networkMetadata)
                if type(neighbor_meta.node).__name__=="Hca" :
                    #print("Neighbor is an HCA, skipping the group")
                    continue


            #find the least loaded port in the group (scan in indexing order)

            min_load_port_meta = PortMetaGroup.find_least_loaded_port_meta(meta_group)


            #r-port is the remote port connected to it
            r_switch_meta=FatTreeAlg.find_node_meta_by_GUID(min_load_port_meta.port.NeighborGUID,networkMetadata)
            r_port_no=min_load_port_meta.port.NeighborPortNo
            r_port_meta=PortMeta.get_meta_by_port_number(r_port_no,r_switch_meta)

            #assign the remote switch node LFT(LID) to r-port
            if(is_place_keeper==True):
                switch_keeper_GUID=FatTreeAlg.find_keeper_switch(networkMetadata,destinationGuid)
            else :
                switch_keeper_GUID = -1

            lft_entry = RoutingTableEntry(destination_GUID=destinationGuid, destination_LID=destinationLid,
                                          next_hop_GUID=switchMeta.node.NodeGUID, next_hop_LID=switchMeta.node.NodeLID,
                                          local_hop_port=r_port_meta.port, local_hop_port_no=r_port_no,
                                          remote_hop_port_no=min_load_port_meta.port.PortNo,
                                          is_for_place_keeper=is_place_keeper,
                                          keeper_switch_GUID=switch_keeper_GUID)
            #uplink_port_meta.
            lft = RoutingTable.find_switch_lft_by_guid(routingTables, r_switch_meta.node.NodeGUID)

            #if(lft.switch_GUID==50000):
            #    print("IN 50000")

            #print("appending table entry in assign_up_going_port_by_descending algorithm for node GID: ", r_switch_meta.node.NodeGUID,
            #      " next hop GUID: ", switchMeta.node.NodeGUID, "DEST GUID: ", destinationGuid,
            #      " local port hop: ", lft_entry.local_hop_port_no)
            lft.routingTableEntries.append(lft_entry)

            #increase r-port usage counter
            r_port_meta.usedForUp+=1
            min_load_port_meta.usedForUp+=1
            #assign assign_up_going_port_by_descending rot the r-port-node
            #TODO
            FatTreeAlg.assign_up_going_port_by_descending(network, networkMetadata,
                                                          routingTables, destinationGuid, destinationLid, r_switch_meta, is_place_keeper)

    @staticmethod
    def assign_down_going_port_by_ascending(network, networkMetadata,
                                            routingTables, destinationGuid,
                                            destinationLid, switchMeta,
                                            is_place_keeper=False):
        #print("IN assign_down_going_port_by1_ascending")
        #print("IN assign_down_going_port_by_ascending with destination Hca GUID ", destinationGuid)

        #hca_destination = FatTreeAlg.find_node_meta_by_GUID(destinationGuid, networkMetadata)
        #Given: a switch and an LID
        #In this method a switch metadata and an LID GUID are used

        #Find a least loaded port of all the groups
        #this practically means find any up-link that is free
        #with additional stop dondition for a spine for all up-going routines
        if switchMeta.isSpine == False:
            uplink_port_meta, uplink_node_meta = FatTreeAlg.find_least_used_remote_downlink_port(networkMetadata, switchMeta)
            #(now get the remote_port on the remote node)
            neighbor_port_no = uplink_port_meta.port.NeighborPortNo
            remote_node_link_port_meta=PortMeta.get_meta_by_port_number(neighbor_port_no,uplink_node_meta)
            #assign LFT(LID) of the remote switch  to that port
            #TODO
            #uplink_node_meta
            if(is_place_keeper==True):
                switch_keeper_GUID=FatTreeAlg.find_keeper_switch(networkMetadata,destinationGuid)
            else :
                switch_keeper_GUID = -1

            lft_entry = RoutingTableEntry(destination_GUID=destinationGuid, destination_LID=destinationLid,
                                          next_hop_GUID=switchMeta.node.NodeGUID, next_hop_LID=switchMeta.node.NodeLID,
                                          local_hop_port=remote_node_link_port_meta.port,
                                          local_hop_port_no=remote_node_link_port_meta.port.PortNo,
                                          remote_hop_port_no=uplink_port_meta.port.PortNo,
                                          is_for_place_keeper=is_place_keeper,
                                          keeper_switch_GUID=switch_keeper_GUID)
            #uplink_port_meta
            lft = RoutingTable.find_switch_lft_by_guid(routingTables, uplink_node_meta.node.NodeGUID)

            #if(lft.switch_GUID==50000):
            #    print("IN 50000")

            #print("appending table entry in assign_down_going_port_by_ascending node GID: ", uplink_node_meta.node.NodeGUID,
            #      " next hop GUID: ", switchMeta.node.NodeGUID, "DEST GUID: ", destinationGuid,
            #      " local port hop: ",lft_entry.local_hop_port_no)
            lft.routingTableEntries.append(lft_entry)

            #track that port usage
            #uplink_port_meta.usedForMeta=True
            uplink_port_meta.usedForDown+=1
            # assign_down_going_port_by_ascending on remote switch
            FatTreeAlg.assign_down_going_port_by_ascending(network, networkMetadata,
                                            routingTables, destinationGuid,
                                            destinationLid, uplink_node_meta,
                                            is_place_keeper)
        #else :
        #    print("SPINE SWITCH, NO FURTHER GOING UP NEEDED")
        #assign_up_going_port_by_descending on current switch
        FatTreeAlg.assign_up_going_port_by_descending(network, networkMetadata,
                                            routingTables, destinationGuid,
                                            destinationLid, switchMeta,
                                                      is_place_keeper)

    @staticmethod
    def find_least_used_remote_downlink_port(networkMetadata, switchMeta):
        #if switchMeta.node.NodeGUID==20000:
        #    print("IN 20000")
        least_used_counter=-1
        least_used_port_meta=-1
        least_used_neighbor_meta=-1
        for port_meta in switchMeta.ports_meta_list :
            #in unused ports find neighbor and check if it is a switch
            neighbor_GUID=port_meta.port.NeighborGUID
            neighbor_meta=FatTreeAlg.find_node_meta_by_GUID(neighbor_GUID,networkMetadata)
            if(type(neighbor_meta.node).__name__=="Switch"):
                #print("LEaf switch found unused port to the spine switch")
                #sanity check if the neighbor has a lowe level
                #should not happen with only 2 levels of switches
                if(switchMeta.level<=neighbor_meta.level):
                    print("ERROR. LEVELS MISMATCH. THIS SHOULD NOT HAPPEN")
                if least_used_counter == -1 or least_used_counter > port_meta.usedForDown:
                    least_used_counter=port_meta.usedForDown
                    least_used_port_meta=port_meta
                    least_used_neighbor_meta=neighbor_meta
        return least_used_port_meta, least_used_neighbor_meta

    def get_lft_lid_port(routingTables, switchMeta, destinationHcaGuid, destinationHcaLid):
        switch_lft = RoutingTable.find_switch_lft_by_guid(routingTables,switchMeta.node.NodeGUID)
        for entry in switch_lft.routingTableEntries:
            if entry.destination_GUID == destinationHcaGuid:
                return entry.local_hop_port,entry.local_hop_port_no
        return -1,-1

    @staticmethod
    def get_downlink_port_meta_groups(networkMetadata,switchMeta):
        down_link_ports=[]
        switch_level=switchMeta.level
        port_meta_groups=FatTreeAlg.get_port_meta_groups(switchMeta)
        for group in port_meta_groups:
            if switch_level < group.get_n_check_ports_level():
                down_link_ports.append(group)
        return down_link_ports


    @staticmethod
    def get_port_meta_groups(switchMeta):
        port_meta_groups=[]
        #first make groups by getting all neighbors GUIDs
        for port_meta in switchMeta.ports_meta_list :
            #check if the neighbor switch ID is in the list
            group = PortMetaGroup.find_by_guid(port_meta_groups, port_meta.port.NeighborGUID)
            if(group ==[]):
                port_meta_groups.append(PortMetaGroup(port_meta.port.NeighborGUID))
                group = port_meta_groups[-1]
            group.ports_metas.append(port_meta)
        return port_meta_groups

    @staticmethod
    def print_routing_tables(routingTables, printKeepers=False):
        for switch_table in routingTables:
            print("PRINTING LFT's for switch GUID: ", switch_table.switch_GUID)
            for entry in switch_table.routingTableEntries:
                if entry.is_for_place_keeper==False :
                    print("HOST SWITCH GUID: ",switch_table.switch_GUID,"DEST GUID: ",
                        entry.destination_GUID,"DEST LID: ",entry.destination_LID,
                        "NEXT HOP GUID:", entry.next_hop_GUID,"NEXT HOP LID: ", entry.next_hop_LID,
                        "LOCAL PORT HOP NO:", entry.local_hop_port_no,"REMOTE PORT HOP NO:", entry.remote_hop_port_no)
                elif printKeepers==True :
                    print("HOST SWITCH GUID: ",switch_table.switch_GUID,"DEST (KEEPER DUMMY) GUID: ",
                        entry.destination_GUID,"DEST LID: ",entry.destination_LID,
                        "NEXT HOP GUID:", entry.next_hop_GUID,"NEXT HOP LID: ", entry.next_hop_LID,
                        "LOCAL PORT HOP NO:", entry.local_hop_port_no,"REMOTE PORT HOP NO:", entry.remote_hop_port_no,
                          "SWITCH GUID OF KEEPER: ", entry.keeper_switch_GUID)
    @staticmethod
    def get_max_hcas_per_switch(switch_metadata):
        #print("IN get_max_hcas_per_switch")
        max_hcas_per_switch=0
        for meta_node in switch_metadata:
            if type(meta_node.node).__name__=="Switch":
                local_hcas_no=0
                print("found switch in meta nodes")
                for port in meta_node.node.ports:
                    print("port for switch node")
                    neighbor_node_meta=FatTreeAlg.find_node_meta_by_GUID(port.NeighborGUID,switch_metadata)
                    if type(neighbor_node_meta.node).__name__=="Hca":
                        local_hcas_no+=1
                if local_hcas_no > max_hcas_per_switch:
                    max_hcas_per_switch=local_hcas_no

        return max_hcas_per_switch

    def set_place_keepers(all_meta, max_hcas_per_switch):

        print("IN set_place_keepers")
        #This function is made to implement the function of dealing with uneven
        #population of switch with nodes
        next_place_keeper_guid=-1
        next_place_keeper_lid=-1

        for meta_node in all_meta:
            if type(meta_node.node).__name__=="Switch":
                local_hcas_no=0
                print("found switch in meta nodes. GUID:", meta_node.node.NodeGUID)
                for port in meta_node.node.ports:
                    neighbor_node_meta=FatTreeAlg.find_node_meta_by_GUID(port.NeighborGUID,all_meta)
                    if type(neighbor_node_meta.node).__name__=="Hca":
                        local_hcas_no+=1
                print("SWITCH GUID ", meta_node.node.NodeGUID," HAS ", local_hcas_no, " HCAs")
                if local_hcas_no < max_hcas_per_switch:
                    print("SWITCH HAS ", local_hcas_no, " HACs INSTEAD OF ", max_hcas_per_switch)
                    if meta_node.isSpine==True :
                        print("Spine switch. No HCAs place keepers needed. Ignoring.")
                        continue
                    else :
                    #add place keepers
                        how_many_place_keepers = max_hcas_per_switch-local_hcas_no
                        for i in range(how_many_place_keepers) :
                            #dummy_port=Port(PortNo=0,PortGUID=next_place_keeper_guid,
                            #                NeighborGUID=meta_node.node.NodeGUID,NeighborPortNo=-1)
                            place_keeper=PlaceKeeper(DummyLID=next_place_keeper_lid,
                                                     DummyGUID=next_place_keeper_guid,
                                                     ParentGUID=meta_node.node.NodeGUID)
                            meta_node.placeKeepers.append(place_keeper)
                            next_place_keeper_guid-=1
                            next_place_keeper_lid-=1

        print("DONE WITH set_place_keepers")

    @staticmethod
    def find_keeper_switch(network_meta,destinationGuid):
        for meta_node in network_meta:
            if type(meta_node.node).__name__=="Switch" :
                for keeper in meta_node.placeKeepers :
                    if(keeper.DummyGUID==destinationGuid) :
                        return meta_node.node.NodeGUID
        return -1


    @staticmethod
    def set_lids_lfts_shift_from_file(network,lft_file, spine_switches_GUID_list=[]):

        print("******do_fat_tree_routing-STARTING ALGORITHM")
        print("******do_fat_tree_routing-CALLING get_meta")
        all_meta, max_hcas_per_switch=FatTreeAlg.get_meta(network,spine_switches_GUID_list)
        FatTreeAlg.set_place_keepers(all_meta,max_hcas_per_switch)
        print("max hcas per switch",max_hcas_per_switch )
        print("******do_fat_tree_routing-CALLING print_meta")
        FatTreeAlg.print_meta(all_meta)

        routing_tables = []

        #guid_2_lid_table= IO.produce_guid_2_lid_table(lid_file)

        #FatTreeAlg.update_lid_in_network(guid_2_lid_table,network)

        routing_tables = IO.produce_incomplete_lft_tables(lft_file)


        FatTreeAlg.do_complete_ltfs_from_incomplete(routing_tables,all_meta)

        return routing_tables, all_meta

    @staticmethod
    def produce_shift_pattern_from_file(shift_file, network_metadata):
        shift_list = IO.produce_shift_pattern_order(shift_file)
        shift_nodes = FatTreeAlg.assign_shift_list_to_real_nodes(shift_list, network_metadata)

        return shift_nodes

    @staticmethod
    def produce_shift_pattern_from_network(network_metadata):
        hcas_n_keepers_list = []
        for meta in network_metadata:
            if type(meta.node).__name__ == "Switch":
                for port in meta.node.ports:
                    neighbor_meta = FatTreeAlg.find_node_meta_by_GUID(port.NeighborGUID, network_metadata)
                    if type(neighbor_meta.node).__name__ == "Hca":
                        hcas_n_keepers_list.append(neighbor_meta.node)
                for keeper in meta.placeKeepers:
                    hcas_n_keepers_list.append("DUMMY")
                    # hcas_n_keepers_list.append(keeper)
        return hcas_n_keepers_list

    @staticmethod
    def do_complete_ltfs_from_incomplete(routing_tables,all_meta):
        print("FILLING MISSING FIELDS IN METADATA")
        for meta_entry in all_meta:
            print("FOUND " + meta_entry.type)
            if meta_entry.type == "Switch":
                print("found a switch with a GUID: " + str(meta_entry.node.NodeGUID))
                print("GUID in hex: " + hex(meta_entry.node.NodeGUID))
                #finding

                lft = RoutingTable.find_switch_lft_by_guid(routing_tables, meta_entry.node.NodeGUID)

                for entry in lft.routingTableEntries:
                    FatTreeAlg.update_incomplete_lft_entry(entry, meta_entry, all_meta)

        pass

    @staticmethod
    def update_lid_in_network(guid_2_lid_table, network):
        print("FILLING MISSING FIELDS IN METADATA")
        for node_entry in network:
            print("FOUND " + type(node_entry).__name__)
            if type(node_entry).__name__ == "Switch":
                print("found a SWITCH with a GUID: " + str(node_entry.NodeGUID))
                lid=guid_2_lid_table.get_lid_by_guid(node_entry.NodeGUID)
                node_entry.NodeLID=lid
                print("SWITCH GUID in hex: " + hex(node_entry.NodeGUID) + " new LID: " + str(node_entry.NodeLID) + " IN HEX: " + hex(node_entry.NodeLID))
            if type(node_entry).__name__ == "Hca":
                print("found an Hca: ")
                for port in node_entry.ports:
                    hca_opensm_guid = node_entry.transform_port_guid_topo_to_opensm(port.PortNo)
                    lid=guid_2_lid_table.get_lid_by_guid(hca_opensm_guid)
                    port.PortLID=lid
                    print("Hca port (OPENSM) GUID in hex: " + hex(hca_opensm_guid) + " new LID: " + str(port.PortLID) + " IN HEX: " + hex(port.PortLID))


            pass
        pass

    @staticmethod
    def update_incomplete_lft_entry(lft_entry, parent_meta, all_meta):

        meta_dest, dest_guid = NodeMeta.find_node_meta_by_lid(lft_entry.destination_LID, all_meta)
        if meta_dest==-1 and dest_guid==-1:
            print("ltf entry to non-existent node. skipping.")
            return
        local_port_no = lft_entry.local_hop_port_no
        neighbor_guid = parent_meta.node.get_port_by_num(local_port_no).NeighborGUID
        neighbor_port_no = parent_meta.node.get_port_by_num(local_port_no).NeighborPortNo
        

        neighbor_meta = NodeMeta.find_node_meta_by_guid(neighbor_guid, all_meta)

        neighbor_port = neighbor_meta.node.get_port_by_num(neighbor_port_no)
        neighbor_port_lid = neighbor_port.PortLID

        #neighbor_port_lid = parent_meta.node.ports[local_port_no].PortLID
        #neighbor_port = lft_entry.next_hop_LID = parent_meta.node.ports[local_port_no]

        lft_entry.destination_GUID= dest_guid
        lft_entry.next_hop_GUID = neighbor_guid
        lft_entry.next_hop_LID = neighbor_port_lid
        lft_entry.remote_hop_port = neighbor_port
        lft_entry.remote_hop_port_no = neighbor_port_no


    @staticmethod
    def assign_shift_list_to_real_nodes(shift_lids_list, network_metadata):
        shift_nodes = []

        #last known switch
        #this is to assign dummy to existing place keeper
        #in a given switch
        #this code relies on the fact that keepers are assigned at the end of the code
        for lid_entry in shift_lids_list:
            #print(entry)
            if lid_entry == int(0xFFFF):
                keepers_switch_meta = NodeMeta.find_node_meta_by_guid(last_used_switch_guid,network_metadata)

                shift_nodes.append(keepers_switch_meta.placeKeepers[used_keepers])
                used_keepers+=1
                #shift_nodes.append("DUMMY")
                pass
            else:
                entry_meta, entry_GUID = NodeMeta.find_node_meta_by_lid(lid_entry,network_metadata)
                shift_nodes.append(entry_meta.node)
                #produce_shift_pattern_from_network
                last_used_switch_guid= entry_meta.node.get_port_by_guid(entry_GUID).NeighborGUID
                used_keepers=0
        return shift_nodes

    @staticmethod
    def update_network_LIDs_from_file(network, lid_file):
        guid_2_lid_table= IO.produce_guid_2_lid_table(lid_file)

        for entry in guid_2_lid_table.table:
            GUID = entry[0]
            LID = entry[1]
            for node in network:
                if type(node).__name__ == "Switch":
                    if node.NodeGUID == GUID:
                        node.NodeLID=LID
                        break
                if type(node).__name__ == "Hca":
                    port = node.get_port_by_guid(GUID)
                    if port !=[]:
                        port.PortLID=LID
                        break

    """
            destination_GUID = -1,
            destination_LID = lft_destination_LID,
            next_hop_GUID = -1,  # TODO
            next_hop_LID = -1,  # TODO
            local_hop_port = -1,  # TODO
            remote_hop_port = -1,  # TODO
            local_hop_port_no = lft_local_hop_port,
            remote_hop_port_no = -1,  # TODO
            is_for_place_keeper = False
    """

    @staticmethod
    def produce_shift_pattern_tuples_subset(shift_order,nodes_list,ibnodes):

        ibnodes_reduced=FatTreeAlg.reduce_ibnodes_by_list(nodes_list,ibnodes)
        subset=FatTreeAlg.associate_nodes_with_pattern(shift_order,ibnodes_reduced)
        return subset


    @staticmethod
    def reduce_ibnodes_by_list(nodes_list,ibnodes):
        ibnodes_reduced =[]
        for node in nodes_list:
            for ibnode in ibnodes:
                if node[0]==ibnode[0]:
                    ibnodes_reduced.append(ibnode)
        return ibnodes_reduced


    @staticmethod
    def associate_nodes_with_pattern(shift_order,ibnodes_reduced):
        shift_final =[]
        for node in shift_order:
            was_node=False
            for ibnode in ibnodes_reduced:
                #print("COMPARING "+ hex(node.ports[0].PortGUID) + " and " + hex(ibnode[2]) +"\n")
                if node.ports[0].PortGUID == ibnode[2]:
                    shift_final.append((ibnode[0], ibnode[1]))
                    was_node=True
            if was_node == False:
                shift_final.append(("dummy",-1))



        return shift_final


'''
    def get_all_hcas_and_keepers(network_metadata):
        hcas_keepers_list = []
            for meta in network_metadata:
                if type(meta.node).__name__ == "Switch":
                    for port in meta.node.ports:
                        neighbor_meta = FatTreeAlg.find_node_meta_by_GUID(port.NeighborGUID, network_metadata)
                        if type(neighbor_meta.node).__name__ == "Hca":
                            hcas_keepers_list.append(neighbor_meta.node)
                        pass
                    for keeper in meta.placeKeepers:
                        hcas_keepers_list.append(keeper)
            return hcas_keepers_list
'''




