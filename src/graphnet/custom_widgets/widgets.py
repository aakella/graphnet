import sys
import typing

from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import QWidget
import json

from static.colors import colors
from static.network_item_types import NetworkItemTypes
from core.route_manager import RouteManager


class CQPushButton(QtWidgets.QPushButton):
    # custom push button with a custom design
    # default buttons dont look good on a graphicsview canvas
    def __init__(self, text, size):
        super(CQPushButton, self).__init__(text)
        self.setStyleSheet(
            '''    
            QPushButton {
                background-color: rgba(140, 140, 140, 0.6); 
                padding: 5px 10px 5px 10px; 
                border-radius: 5px;
                border: 1px solid #424242;
                height: 20px;
                width: 80px;
                font-size: 15px;
            }

            QPushButton:hover {
                background-color: #999999;
            }

            QPushButton:pressed {
                background-color: #919191;
            }
            '''
        )


class CQCheckBox(QtWidgets.QCheckBox):
    # custom checkbox that acts as a tab switcher button
    # internally is a checkbox (toggle-able) but looks like a regular button
    # added EdgeIndex to create a button group, edge buttons have curved edges on one side
    # 0 or 2 depending on left or right edge
    # middle buttons index 1 are square
    # setting multiple middle buttons with 2 edge buttons creates a tab switcher 'array' that looks good
    def __init__(self, text, EdgeIndex):
        self.ext = ''
        super(CQCheckBox, self).__init__(text)
        if EdgeIndex == 0:
            self.ext = '''
            QCheckBox {
                background-color: rgba(140, 140, 140, 0.6); 
                padding: 5px 15px 5px 10px; 
                border-top-left-radius: 5px;
                border-bottom-left-radius: 5px;
                border: 1px solid #424242;
            } '''

        elif EdgeIndex == 1:
            self.ext = '''
          QCheckBox {
                background-color: rgba(140, 140, 140, 0.6); 
                padding: 5px 15px 5px 10px; 
                border-top: 1px solid #424242;
                border-bottom: 1px solid #424242;
            }
            '''
        elif EdgeIndex == 2:
            self.ext = '''
            QCheckBox {
                background-color: rgba(140, 140, 140, 0.6); 
                padding: 5px 15px 5px 10px; 
                border-top-right-radius: 5px;
                border-bottom-right-radius: 5px;
                border: 1px solid #424242;
            }
            '''
        else:
            self.ext = '''
            QCheckBox {
                background-color: rgba(140, 140, 140, 0.6); 
                padding: 5px 15px 5px 10px; 
                border-radius: 5px;
                # border: 1px solid #424242;
            }
            '''
        self.setStyleSheet(self.ext +
                           '''    
            QCheckBox:hover {
                background-color: #999999;
            }

            QCheckBox:pressed {
                background-color: rgba(59, 59, 59, 0.6);
                color: #d6d6d6;
            }

            QCheckBox:checked {
                background-color: rgba(59, 59, 59, 0.6);
                color: #d6d6d6;
            }

            QCheckBox:indicator {
                width: 0px;
                height: 0px;
            }

            '''
                           )


class CQGraphicsView(QtWidgets.QGraphicsView):
    # custom graphics view class that can handle mouse and keyboard events for shortcuts
    # also sets custom cursor icons when certain tools are active
    zoomSignal = QtCore.pyqtSignal()
    panSignal = QtCore.pyqtSignal()
    arrowSignal = QtCore.pyqtSignal()
    jumpSignal = QtCore.pyqtSignal()

    def __init__(self):
        super().__init__()
        self.zoomMode = False
        self.dragMode = False
        self.scaleInFactor = 1.25
        self.scaleOutFactor = 1 / self.scaleInFactor
        self.currentScale = 1
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.prevCenter = None
        self.isControl = True
        self.oldZoom = 0.1
        self.hasJumped = False

        # custom icons
        self.zoomInCursor = QtGui.QCursor(QtGui.QIcon("assets/images/icons/zoom_in.png").pixmap(24, 24), 0, 0)
        self.zoomOutCursor = QtGui.QCursor(QtGui.QIcon("assets/images/icons/zoom_out.png").pixmap(24, 24), 0, 0)
        self.zoomCursor = QtGui.QCursor(QtGui.QIcon("assets/images/icons/zoom.png").pixmap(24, 24), 0, 0)

    def fitView(self, item):
        self.fitInView(item.rect())

    def remap(self):
        return self.mapToScene(0, 0)

    # detect shortcut key presses
    def keyPressEvent(self, event) -> None:
        if event.key() == QtCore.Qt.Key_Control:
            self.zoomSignal.emit()
            self.zoom_cursor()
            self.zoomMode = True
        if event.key() == QtCore.Qt.Key_Alt:
            self.panSignal.emit()
            self.dragMode = True
            self.setInteractive(False)
            self.setDragMode(CQGraphicsView.ScrollHandDrag)
        if event.key() == QtCore.Qt.Key_C:
            if self.zoomMode:
                print("exiting...")
                sys.exit()
        if event.key() == QtCore.Qt.Key_Q:
            self.jumpSignal.emit()
            self.oldZoom = self.currentScale

    def reset_cursor(self):
        self.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))

    def zoom_cursor(self):
        self.setCursor(self.zoomCursor)

    # release keys - disable shortcuts
    def keyReleaseEvent(self, event) -> None:
        self.arrowSignal.emit()
        self.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        if event.key() == QtCore.Qt.Key_Control:
            self.zoomMode = False
        if event.key() == QtCore.Qt.Key_Alt:
            self.dragMode = False
            self.setInteractive(True)
            self.setDragMode(CQGraphicsView.NoDrag)

    # detect scroll wheel movements
    # if zoom is enabled a postive angle adds to the zoom
    # negative angle subtracts to the zoom
    def wheelEvent(self, a0: QtGui.QWheelEvent) -> None:

        if self.zoomMode:
            self.setTransformationAnchor(QtWidgets.QGraphicsView.NoAnchor)
            self.setResizeAnchor(QtWidgets.QGraphicsView.NoAnchor)
            oldPos = self.mapToScene(a0.pos())
            zoomFactor = self.scaleInFactor if a0.angleDelta().y() > 0 else self.scaleOutFactor
            self.scale(zoomFactor, zoomFactor)

            # reset scale
            newPos = self.mapToScene(a0.pos())
            delta = newPos - oldPos
            self.translate(delta.x(), delta.y())


class CQToolBar(QtWidgets.QToolBar):
    # custom tool bar with custom QSS/look
    # default looks bad when floating on a graphics view
    def __init__(self):
        super(CQToolBar, self).__init__()
        self.oldCursor = QtGui.QCursor(QtCore.Qt.ArrowCursor)
        self.defaultCursor = QtGui.QCursor(QtCore.Qt.ArrowCursor)
        self.setStyleSheet(
            '''
            QToolBar {
                background-color: rgba(140, 140, 140, 0.9); 
                padding: 5px 5px 5px 5px; 
                border-radius: 5px;
                spacing: 10px;
            }
            
            QToolButton {
                background-color: rgba(140, 140, 140, 0.0);  
                border-radius: 5px;
            }
            
            QToolButton:checked {
                background-color: rgba(97, 97, 97, 1);
            }   
            
            QToolButton:hover {
                background-color: rgba(97, 97, 97, 0.3)
            }
            '''
        )

    # change cursor when hovering over the toolbar
    def enterEvent(self, a0: QtCore.QEvent) -> None:
        self.oldCursor = self.cursor()
        self.parentWidget().setCursor(self.defaultCursor)

    def leaveEvent(self, a0: QtCore.QEvent) -> None:
        self.parentWidget().setCursor(self.oldCursor)


class CQCircleLabel(QtWidgets.QGraphicsEllipseItem):
    # port label custom item
    # used to set labels to ports, useful for following links and routes
    def __init__(self, x, y, r, text):
        super(CQCircleLabel, self).__init__(x, y, r, r)
        self.x = x
        self.y = y
        self.r = r
        self.text = text
        self.isConnected = True
        self.defaultColor = colors[NetworkItemTypes.SWITCH]["hover"]
        self.setBrush(self.defaultColor)
        self.font = QtGui.QFont()
        self.font.setPixelSize(12)
        self.setZValue(100)
        self.offsetX = [-3, -7]
        self.offsetY = [4, 4]

    # custom painter to set text and center align
    def paint(self, painter: QtGui.QPainter, option: 'QStyleOptionGraphicsItem',
              widget: typing.Optional[QWidget] = ...) -> None:
        super(CQCircleLabel, self).paint(painter, option, widget)
        painter.setFont(self.font)
        if len(self.text) == 1:
            painter.drawText(self.x + (self.r / 2) + self.offsetX[0], self.y + (self.r / 2) + self.offsetY[0],
                             self.text)
        elif len(self.text) == 2:
            painter.drawText(self.x + (self.r / 2) + self.offsetX[1], self.y + (self.r / 2) + self.offsetY[1],
                             self.text)

    # helper methods to set different colors
    def mark(self):
        self.setBrush(colors["general"]["mark"])

    def unmark(self):
        self.unhighlight()

    def highlight(self):
        self.setBrush(colors[NetworkItemTypes.LINK]["highlight"])

    def unhighlight(self):
        self.setBrush(self.defaultColor)

    def setConnected(self, c):
        self.isConnected = c
        if not self.isConnected:
            self.setBrush(QtGui.QColor(100, 100, 100))


class LinePen(QtGui.QPen):
    # custom pen class to easily set color and dash patterns if needed
    def __init__(self, color=QtCore.Qt.black, stroke=1, dash_pattern=False):
        super().__init__(QtCore.Qt.black)
        self.setBrush(color)
        self.setWidth(stroke)
        if dash_pattern:
            space = 3
            dashes = list((2, space))
            self.setDashPattern(dashes)


class PaintBox(QtWidgets.QPushButton):
    # select color icon for the settings panel
    def __init__(self, fill_color, txt=""):
        super(PaintBox, self).__init__(txt)
        self.fill_color = fill_color
        self.setStyleSheet('background-color: %s' % self.fill_color.name())


class SettingsItem:
    # settings item, work in progress
    def __init__(self, name, category, default):
        self.name = name
        self.category = category
        self.default = default
        self.setting = default
        self.temporary = None

    def save(self, new_s):
        self.setting = self.temporary
        self.temporary = None

    def update(self, up_t):
        self.temporary = up_t

    def reset(self):
        self.setting = self.default


class HelpWindow(QtWidgets.QMainWindow):
    # launches a dialog - help window
    # has icons and text descriptions for tools
    def __init__(self, parent):
        super(HelpWindow, self).__init__(parent)
        QtWidgets.QMainWindow.__init__(self, None, QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.Tool | QtCore.Qt.Dialog)
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.CustomizeWindowHint)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowMaximizeButtonHint)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowMinimizeButtonHint)
        self.setWindowModality(QtCore.Qt.WindowModal)
        self.setWindowTitle("Help")
        self.font = QtGui.QFont()
        self.font.setFamily(self.font.defaultFamily())
        self.font.setPixelSize(10)
        self.setFont(self.font)
        self.central_widget = QtWidgets.QWidget()
        self.setCentralWidget(self.central_widget)
        self.parentLayout = QtWidgets.QVBoxLayout()
        self.mainLayout = QtWidgets.QVBoxLayout()
        self.central_widget.setLayout(self.parentLayout)
        self.groupBox = QtWidgets.QGroupBox("Help")
        self.setStyleSheet('background: #4c4c4c; color: white;')

        self.mouseIcon = QtGui.QIcon("assets/images/icons/selection.png")
        self.zoomIcon = QtGui.QIcon("assets/images/icons/zoom.png")
        self.moveIcon = QtGui.QIcon("assets/images/icons/pan.png")
        self.hcaIcon = QtGui.QIcon("assets/images/icons/showhcas.png")
        self.deselectIcon = QtGui.QIcon("assets/images/icons/deselect.png")

        self.mouseTool = QtWidgets.QLabel("Use the selection tool to select objects and view details")
        self.mouseToolIcon = QtWidgets.QLabel()
        self.mouseToolIcon.setPixmap(self.mouseIcon.pixmap(QtCore.QSize(24, 24)))

        self.zoomTool = QtWidgets.QLabel("Use the zoom tool to zoom into and out of the network")
        self.zI = QtWidgets.QLabel()
        self.zI.setPixmap(self.zoomIcon.pixmap(QtCore.QSize(24, 24)))

        self.moveTool = QtWidgets.QLabel("Use the move tool to pan and move network around")
        self.mI = QtWidgets.QLabel()
        self.mI.setPixmap(self.moveIcon.pixmap(QtCore.QSize(24, 24)))

        self.hcaTool = QtWidgets.QLabel("Use the HCA Toggle Button to enable or disable HCA rendering when a leaf "
                                        "switch is selected")
        self.hI = QtWidgets.QLabel()
        self.hI.setPixmap(self.hcaIcon.pixmap(QtCore.QSize(24, 24)))

        self.deselectTool = QtWidgets.QLabel("Use the deselect tool to remove all current selections")
        self.dI = QtWidgets.QLabel("Use the deselect tool to remove all current selections")
        self.dI.setPixmap(self.deselectIcon.pixmap(QtCore.QSize(24, 24)))

        self.shortcuts = QtWidgets.QLabel("\n\nShortcuts\nUse CTRL + Scroll to Zoom In and Zoom out\nUse ALT + Drag "
                                          "to pan")

        # insert layout
        self.mainLayout.addWidget(self.mouseToolIcon)
        self.mainLayout.addWidget(self.mouseTool)

        self.mainLayout.addWidget(self.zI)
        self.mainLayout.addWidget(self.zoomTool)

        self.mainLayout.addWidget(self.mI)
        self.mainLayout.addWidget(self.moveTool)

        self.mainLayout.addWidget(self.hI)
        self.mainLayout.addWidget(self.hcaTool)

        self.mainLayout.addWidget(self.dI)
        self.mainLayout.addWidget(self.deselectTool)
        self.mainLayout.addWidget(self.shortcuts)
        self.groupBox.setLayout(self.mainLayout)
        self.parentLayout.addWidget(self.groupBox)


class CQPopUpSettings(QtWidgets.QMainWindow):
    # popup window for settings
    # work in progress, can be used to set colors and other parameters for the network
    def __init__(self, parent):
        super(CQPopUpSettings, self).__init__(parent)
        QtWidgets.QMainWindow.__init__(self, None, QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.Tool | QtCore.Qt.Dialog)
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.CustomizeWindowHint)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowMaximizeButtonHint)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowMinimizeButtonHint)
        self.setWindowModality(QtCore.Qt.WindowModal)
        self.font = QtGui.QFont()
        self.font.setFamily(self.font.defaultFamily())
        self.font.setPixelSize(10)
        self.setFont(self.font)

        self.central_widget = QtWidgets.QWidget()
        self.mainLayout = QtWidgets.QHBoxLayout()
        self.sidePanel = QtWidgets.QVBoxLayout()

        self.all_settings = {}

        self.settingsList = QtWidgets.QListWidget()
        self.mainLayout.addWidget(self.settingsList)
        self.settingsList.addItem("Colors")

        self.all_settings["colors"] = [SettingsItem("Switch Color", "colors",
                                                    colors[NetworkItemTypes.SWITCH]["default"]),
                                       SettingsItem("Switch Hover", "colors",
                                                    colors[NetworkItemTypes.SWITCH]["hover"]),
                                       SettingsItem("Switch Selected", "colors",
                                                    colors[NetworkItemTypes.SWITCH]["selected"]),
                                       SettingsItem("Switch Highlight", "colors",
                                                    colors[NetworkItemTypes.SWITCH]["highlight"])
                                       ]

        self.settingsList.addItem("Sizes")
        self.settingsList.addItem("Network")
        self.settingsList.addItem("Files")
        self.settingsList.setFixedWidth(100)
        self.setGeometry(QtCore.QRect(200, 200, 600, 400))

        self.central_widget.setLayout(self.mainLayout)
        self.mainLayout.addLayout(self.sidePanel)

        self.groupBox = QtWidgets.QGroupBox("Color Pallet")
        self.colorsPanel = QtWidgets.QVBoxLayout()
        self.colorsPanel.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.groupBox.setLayout(self.colorsPanel)

        self.sidePanel.addLayout(self.colorsPanel)
        self.sidePanel.addWidget(self.groupBox)
        self.colors_panel()

        self.setCentralWidget(self.central_widget)
        self.setWindowTitle("Preferences")
        self.settingsList.clicked.connect(self.listc)

    def listc(self):
        pass

    def colors_panel(self):
        for setting in self.all_settings["colors"]:
            x = QtWidgets.QLabel(setting.name)
            tLayout = QtWidgets.QHBoxLayout()
            tLayout.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
            tLayout.addWidget(x)
            tLayout.addWidget(PaintBox(setting.default))
            self.colorsPanel.addLayout(tLayout)


class AppSettings:
    # load settings from JSON
    # experimental, not used
    def __init__(self):
        self.all_settings = {}
        # self.load_settings()

    def load_settings(self):
        with open('assets/app/settings.json') as f:
            self.all_settings = json.load(f)


class ArrowOverlay(QtWidgets.QGraphicsLineItem):
    # calculates a series of mid points for a network link to show routing direction
    # angled lines drawn by calculating the line's slope
    # 45 degrees for 1 side and 135 for the other
    # these angles need to be flipped for negative and postive slopes
    # done using a simple if condition
    # mid point is average of 2 points in x and y
    # ratio-ed points can be calculated with a loop
    def __init__(self, nc, isUp, simple=False):
        self.nc = nc
        self.arrows = []
        super(ArrowOverlay, self).__init__(self.nc.x1, self.nc.y1, self.nc.x2, self.nc.y2)
        self.setZValue(200)
        self.limit = 30
        self.isUp = isUp
        self.interval = 100
        # 135 FOR UP - POSITIVE SLOPE

        if self.nc.slope > 0:
            self.upAngle = 45
            self.downAngle = 135
        else:
            self.upAngle = 135
            self.downAngle = 45

        if self.isUp:
            self.calculate_arrows(self.upAngle) if not simple else self.calculate_arrows_simple(self.upAngle)
        else:
            self.calculate_arrows(self.downAngle) if not simple else self.calculate_arrows_simple(self.downAngle)

    # simple arrows, only 2
    def calculate_arrows_simple(self, a):
        self.arrows = []
        x1 = (3/4) * self.nc.x1 + (1/4) * self.nc.x2
        y1 = (3/4) * self.nc.y1 + (1/4) * self.nc.y2
        self.arrows.append((self.getLine(x1, y1, 50, a), self.getLine(x1, y1, 50, -a)))

        x1 = (3 / 4) * self.nc.x2 + (1 / 4) * self.nc.x1
        y1 = (3 / 4) * self.nc.y2 + (1 / 4) * self.nc.y1
        self.arrows.append((self.getLine(x1, y1, 50, a), self.getLine(x1, y1, 50, -a)))

    # multiple arrows, upto n
    # loop through and calculate 'ratio' points
    def calculate_arrows(self, a):
        self.arrows = []
        for i in range(1, self.limit):
            x1 = (i / self.limit) * self.nc.x2 + ((self.limit - i) / self.limit) * self.nc.x1
            y1 = (i / self.limit) * self.nc.y2 + ((self.limit - i) / self.limit) * self.nc.y1
            self.arrows.append((self.getLine(x1, y1, 50, a), self.getLine(x1, y1, 50, -a)))

    # get line to draw at an angle
    def getLine(self, x, y, l, a):
        aLine = QtCore.QLineF()
        aLine.setP1(QtCore.QPointF(x, y))
        aLine.setLength(l)
        aLine.setAngle(a - self.nc.line_angle)
        return aLine

    # custom painting for calculated lines
    def paint(self, painter: QtGui.QPainter, option: 'QStyleOptionGraphicsItem',
              widget: typing.Optional[QWidget] = ...) -> None:
        painter.setBrush(QtCore.Qt.NoBrush)
        painter.setBrush(QtCore.Qt.NoBrush)
        painter.setPen(LinePen(stroke=20, dash_pattern=False, color=QtCore.Qt.red))
        for arrow in self.arrows:
            painter.drawLine(arrow[0])
            painter.drawLine(arrow[1])
        # painter.drawLine(self.getLine(self.nc.x_mid, self.nc.y_mid, 100, -75))


class ConflictsManagerWindow(QtWidgets.QMainWindow):
    # conflict manager, dialog box with list boxes
    # general QT elements to display shift values and routes
    conflictManagerClosed = QtCore.pyqtSignal()
    routeSelected = QtCore.pyqtSignal()

    def __init__(self, parent):
        #internals
        self.conflict_manager = None
        super(ConflictsManagerWindow, self).__init__(parent)
        QtWidgets.QMainWindow.__init__(self, None, QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.Tool | QtCore.Qt.Dialog)
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.CustomizeWindowHint)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowMaximizeButtonHint)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowMinimizeButtonHint)
        self.font = QtGui.QFont()
        self.font.setFamily(self.font.defaultFamily())
        self.font.setPixelSize(10)
        self.setFont(self.font)
        self.currentSelectedRoute = None

        self.central_widget = QtWidgets.QWidget()
        self.mainLayout = QtWidgets.QHBoxLayout()
        self.sidePanel = QtWidgets.QVBoxLayout()
        self.searchGroupLayout = QtWidgets.QVBoxLayout()

        self.shiftListLabel = QtWidgets.QLabel("Shift Values")
        self.shiftList = QtWidgets.QListWidget()
        self.shiftLayout = QtWidgets.QVBoxLayout()
        self.shiftLayout.addWidget(self.shiftListLabel)
        self.shiftLayout.addWidget(self.shiftList)
        self.shiftList.itemSelectionChanged.connect(self.list_selection_changed)

        self.mainLayout.addLayout(self.shiftLayout)
        self.shiftList.setFixedWidth(100)
        self.error = False
        self.setGeometry(QtCore.QRect(200, 200, 600, 400))

        self.central_widget.setLayout(self.mainLayout)
        self.mainLayout.addLayout(self.sidePanel)

        self.searchGroup = QtWidgets.QGroupBox("Search Options")
        self.selectGroup = QtWidgets.QGroupBox("Conflicting Routes")
        self.infoGroup = QtWidgets.QGroupBox("Details")

        self.shiftSearchLayout = QtWidgets.QHBoxLayout()
        self.shiftSearchButton = QtWidgets.QPushButton("Jump")
        self.shiftSearchSpinBox = QtWidgets.QSpinBox()
        self.shiftSearchSpinBox.setRange(0, 500)
        self.helpString = "Select a shift value and click jump to go there"
        self.errorString = "Can't Jump: no conflicts for that shift value"
        self.errorLabel = QtWidgets.QLabel(self.helpString)
        s = "no conflicts selected"
        self.infoLabel = QtWidgets.QLabel(s)
        self.errorLabel.setStyleSheet('color: black; font-size: 10px')

        self.shiftSearchLayout.addWidget(self.shiftSearchSpinBox, 4)
        self.shiftSearchLayout.addWidget(self.shiftSearchButton, 1)
        self.shiftSearchLayout.setAlignment(QtCore.Qt.AlignTop)

        self.searchGroupLayout.addLayout(self.shiftSearchLayout)
        self.searchGroupLayout.addWidget(self.errorLabel)
        self.searchGroupLayout.setAlignment(QtCore.Qt.AlignTop)

        self.infoLayout = QtWidgets.QVBoxLayout()
        self.infoLayout.addWidget(self.infoLabel)
        self.infoGroup.setLayout(self.infoLayout)

        self.selectNodeList = QtWidgets.QListWidget()
        self.selectRouteList = QtWidgets.QListWidget()
        self.selectRouteList.itemSelectionChanged.connect(self.route_selection_changed)
        self.nodeListLabel = QtWidgets.QLabel("Nodes")
        self.routeListLabel = QtWidgets.QLabel("Routes")

        self.selectListLayout = QtWidgets.QHBoxLayout()
        self.selectLayout = QtWidgets.QVBoxLayout()
        self.selectLabelLayout = QtWidgets.QHBoxLayout()

        self.selectLabelLayout.addWidget(self.nodeListLabel)
        self.selectLabelLayout.addWidget(self.routeListLabel)

        self.selectListLayout.addWidget(self.selectNodeList)
        self.selectListLayout.addWidget(self.selectRouteList)

        self.selectLayout.addLayout(self.selectLabelLayout)
        self.selectLayout.addLayout(self.selectListLayout)
        self.selectGroup.setLayout(self.selectLayout)

        self.searchGroup.setLayout(self.searchGroupLayout)
        self.sidePanel.addWidget(self.searchGroup, 1)
        self.sidePanel.addWidget(self.selectGroup, 3)
        self.sidePanel.addWidget(self.infoGroup, 1)

        self.shiftSearchButton.clicked.connect(self.jump_to_conflict)

        self.setCentralWidget(self.central_widget)
        self.setWindowTitle("Conflicts Manager")

    # called when a list item is clicked
    def list_selection_changed(self):
        self.selectNodeList.clear()
        self.selectRouteList.clear()
        self.currentSelectedRoute = None
        self.routeSelected.emit()
        conflict = self.conflict_manager.conflicts[self.shiftList.currentRow()]
        self.selectNodeList.addItem(str(conflict.nodeGUID))
        for route in conflict.conflictingRoutes:
            self.selectRouteList.addItem(str(route[0]) + " to " + str(route[1]))

    # get info for the current route to add it to the details box
    def get_node_info(self):
        if self.currentSelectedRoute:
            s = "Source: " + str(self.currentSelectedRoute.routeFinder.sourceGUID) + "\n"
            i = 1
            for hop in self.currentSelectedRoute.simpleRouteList:
                s += "Hop " + str(i) + ": " + str(hop[0]) + " port " + str(hop[1]) + "\n"
                i += 1
            s += "Destination: " + str(self.currentSelectedRoute.routeFinder.destinationGUID) + "\n"
            return s[:-1]
        else:
            return ""

    # called when a route is selected
    def route_selection_changed(self):
        self.currentSelectedRoute = self.conflict_manager.routes[self.shiftList.currentRow()]\
            [self.selectRouteList.currentRow()]
        self.infoLabel.setText(self.get_node_info())
        self.routeSelected.emit()

    # search for the conflict in the list
    def jump_to_conflict(self):
        conflict = self.shiftList.findItems(str(self.shiftSearchSpinBox.value()), QtCore.Qt.MatchExactly)
        if len(conflict) <= 0:
            self.search_error(True)
            return
        if self.error:
            self.search_error(False)

        self.shiftList.setCurrentItem(conflict[0])

    # populate the list with conflicts extracted from conflicts.pickle
    def populate_conflicts(self, conflict_manager):
        self.conflict_manager = conflict_manager
        if len(self.conflict_manager.conflicts) == 0:
            self.infoLabel.setText("This network has no shift conflicts. \nClose this window.");
            return
        for conflict in self.conflict_manager.conflicts:
            self.shiftList.addItem(str(conflict.shift_distance))

    # show an error if search value doesnt exist in the shift list
    def search_error(self, yn):
        self.error = True
        if yn:
            self.errorLabel.setText(self.errorString)
            self.errorLabel.setStyleSheet('color: red; font-size: 10px')
        else:
            self.errorLabel.setText(self.helpString)
            self.errorLabel.setStyleSheet('color: black; font-size: 10px')

    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:
        self.conflictManagerClosed.emit()


class RouteManagerWindow(QtWidgets.QMainWindow):
    # window for managing routes
    # basic QT widgets
    routeManagerClosed = QtCore.pyqtSignal()
    routeSelected = QtCore.pyqtSignal()
    clearScreen = QtCore.pyqtSignal()
    routeAdded = QtCore.pyqtSignal()
    listentingForSelection = QtCore.pyqtSignal()

    # create layouts and set texts and stuff
    def __init__(self, parent):
        #internals
        self.routes_list = []
        QtWidgets.QMainWindow.__init__(self, None, QtCore.Qt.WindowStaysOnTopHint)

        super(RouteManagerWindow, self).__init__(parent)
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.Tool | QtCore.Qt.Dialog)
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.CustomizeWindowHint)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowMaximizeButtonHint)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowMinimizeButtonHint)
        self.font = QtGui.QFont()
        self.font.setFamily(self.font.defaultFamily())
        self.font.setPixelSize(10)
        self.setFont(self.font)
        self.currentSelectedRoute = None
        self.sourceListening = False
        self.destListening = False
        self.currentSource = None
        self.currentDestination = None
        self.error = True

        self.network = None
        self.routing_tables = None

        self.central_widget = QtWidgets.QWidget()
        self.mainLayout = QtWidgets.QVBoxLayout()
        self.createRouteLayout = QtWidgets.QVBoxLayout()

        self.central_widget.setLayout(self.mainLayout)

        self.mainGroup = QtWidgets.QGroupBox("Add/View Routes")
        self.activeRoutesGroup = QtWidgets.QGroupBox("Active Routes")
        self.infoGroup = QtWidgets.QGroupBox("Details")

        self.addRouteLayout = QtWidgets.QHBoxLayout()
        self.addButton = QtWidgets.QPushButton("Add")
        self.sourceListener = QtWidgets.QPushButton("Set Source")
        self.destinationListener = QtWidgets.QPushButton("Set Destination")
        self.sourceListener.clicked.connect(self.set_source)
        self.destinationListener.clicked.connect(self.set_destination)

        self.helpString = "set a source, destination and click add"
        self.errorString = "set both source and destination nodes before adding"
        self.errorLabel = QtWidgets.QLabel(self.helpString)
        self.infoLabel = QtWidgets.QLabel("no active routes")
        self.errorLabel.setStyleSheet('color: black; font-size: 10px')

        self.addRouteLayout.addWidget(self.sourceListener, 4)
        self.addRouteLayout.addWidget(self.destinationListener, 4)
        self.addRouteLayout.addWidget(self.addButton, 1)
        self.addButton.clicked.connect(self.add_route)
        self.addRouteLayout.setAlignment(QtCore.Qt.AlignTop)

        self.createRouteLayout.addLayout(self.addRouteLayout)
        self.createRouteLayout.addWidget(self.errorLabel)
        self.createRouteLayout.setAlignment(QtCore.Qt.AlignTop)

        self.activeRoutesList = QtWidgets.QListWidget()
        self.clearListRoutesButton = QtWidgets.QPushButton("Clear Routes From List")
        self.clearScreenRoutesButton = QtWidgets.QPushButton("Clear Routes On Screen")
        self.clearListRoutesButton.clicked.connect(self.clear_list)
        self.clearScreenRoutesButton.clicked.connect(self.clear_screen)

        self.activeRoutesList.itemSelectionChanged.connect(self.route_selection_changed)
        self.activeRoutesLayout = QtWidgets.QVBoxLayout()
        self.activeRoutesLayout.addWidget(self.activeRoutesList)
        self.activeRoutesLayout.addWidget(self.clearListRoutesButton)
        self.activeRoutesLayout.addWidget(self.clearScreenRoutesButton)
        self.activeRoutesGroup.setLayout(self.activeRoutesLayout)

        self.infoLayout = QtWidgets.QVBoxLayout()
        self.infoLayout.addWidget(self.infoLabel)
        self.infoGroup.setLayout(self.infoLayout)

        self.mainGroup.setLayout(self.createRouteLayout)
        self.mainLayout.addWidget(self.mainGroup, 1)
        self.mainLayout.addWidget(self.activeRoutesGroup, 3)
        self.mainLayout.addWidget(self.infoGroup, 1)

        self.setCentralWidget(self.central_widget)
        self.setWindowTitle("Routing Manager")

    # clear the screen when the button is clicked
    def clear_screen(self):
        self.activeRoutesList.clearSelection()
        self.infoLabel.setText("no active routes")
        self.clearScreen.emit()

    # empty the list when the button is clicked
    def clear_list(self):
        self.routes_list = []
        self.activeRoutesList.clear()

    # set the network object
    def set_network(self, nt):
        self.network = nt

    # set the routing tables
    def set_routing_tables(self, rt):
        self.routing_tables = rt

    # get info about the node
    # show it in the details panel
    def get_node_info(self):
        if self.currentSelectedRoute:
            s = "Source: " + str(self.currentSelectedRoute.routeFinder.sourceGUID) + "\n"
            i = 1
            for hop in self.currentSelectedRoute.simpleRouteList:
                s += "Hop " + str(i) + ": " + str(hop[0]) + " port " + str(hop[1]) + "\n"
                i += 1
            s += "Destination: " + str(self.currentSelectedRoute.routeFinder.destinationGUID) + "\n"
            return s[:-1]
        else:
            return ""

    # set source node + some edge case checking to avoid errors
    def set_source(self):
        if self.destListening:
            self.errorString = "already listening for a destination"
            self.search_error(True)
            return
        if self.sourceListening:
            self.sourceListening = False
            self.sourceListener.setText("Set Source")
        else:
            self.sourceListening = True
            self.sourceListener.setText("Listening...")
            self.listentingForSelection.emit()

    # set source node + some edge case checking to avoid errors
    # set text to listening when active
    def set_destination(self):
        if self.sourceListening:
            self.errorString = "already listening for a source"
            self.search_error(True)
            return
        if self.destListening:
            self.destListening = False
            self.destinationListener.setText("Set Destination")
        else:
            self.destListening = True
            self.destinationListener.setText("Listening...")
            self.listentingForSelection.emit()

    # called after the node is selected after the source is set
    def finish_source(self, obj):
        if self.error:
            self.error = False
            self.search_error(False)
        self.currentSource = obj
        self.sourceListening = False
        self.sourceListener.setText("Source Set")

    # called after the node is selected after the destination is set
    def finish_destination(self, obj):
        if self.error:
            self.error = False
            self.search_error(False)
        self.currentDestination = obj
        self.destListening = False
        self.destinationListener.setText("Destination Set")

    # add a router manager object to the list and initialize it
    # will be drawn by the main window when selected
    def add_route(self):
        if not self.currentDestination or not self.currentSource:
            self.search_error(True)
            return
        self.error = False if self.error else True
        route_manager = RouteManager(False)
        route_manager.set_network(self.network)
        route_manager.set_routing_tables(self.routing_tables)
        route_manager.setSource(self.currentSource)
        route_manager.setDestination(self.currentDestination)
        route_manager.start()
        route_manager.set_simple_route_list()
        self.activeRoutesList.addItem(str(self.currentSource.guid) + " to " +  str(self.currentDestination.guid))
        self.routes_list.append(route_manager)
        self.routeAdded.emit()
        self.destinationListener.setText("Set Destination")
        self.sourceListener.setText("Set Source")
        self.currentSource = None
        self.currentDestination = None

    # called when a list object is clicked
    def route_selection_changed(self):
        if len(self.routes_list) == 0:
            self.infoLabel.setText("no active routes")
            return
        self.currentSelectedRoute = self.routes_list[self.activeRoutesList.currentRow()]
        self.infoLabel.setText(self.get_node_info())
        self.routeSelected.emit()

    # used if any error occurs
    def search_error(self, yn):
        self.error = True
        if yn:
            self.errorLabel.setText(self.errorString)
            self.errorLabel.setStyleSheet('color: red; font-size: 10px')
        else:
            self.errorLabel.setText(self.helpString)
            self.errorLabel.setStyleSheet('color: black; font-size: 10px')

    # close the rt manager
    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:
        self.routeManagerClosed.emit()


