from PyQt5 import QtWidgets, QtGui, QtCore


class CQDropShadow(QtWidgets.QGraphicsDropShadowEffect):
    # drop shadow effect, custom class with tested values
    # can be applied to any pyqt widget
    def __init__(self):
        super(CQDropShadow, self).__init__()
        self.setBlurRadius(30)
        self.setXOffset(0)
        self.setYOffset(0)