from PyQt5 import QtWidgets, QtGui, QtCore

# Qt object for a 'network' link
# NOT BEING USED FOR NOW


class NetworkLink(QtWidgets.QGraphicsLineItem):
    # simplified network link object (NOT USED ANYMORE)
    def __init__(self, x1, y1, x2, y2, color=QtCore.Qt.darkGray):
        super().__init__(x1, y1, x2, y2)
        self.setPen(LinePen(stroke=1, dash_pattern=False, color=color))

    def recolor_link(self, color, stroke=1, dash=False):
        self.setPen(LinePen(stroke=stroke, dash_pattern=dash, color=color))
        self.update()


class LinePen(QtGui.QPen):
    # simplified link pen object (NOT USED ANYMORE)
    def __init__(self, color=QtCore.Qt.black, stroke=1, dash_pattern=False):
        super(LinePen, self).__init__(color)
        self.setWidth(stroke)
        if dash_pattern:
            space = 3
            dashes = list((2, space))
            self.setDashPattern(dashes)
