from PyQt5 import QtWidgets, QtCore, QtGui
import typing

from custom_widgets.widgets import CQCircleLabel
from static.network_item_types import NetworkItemTypes
from static.colors import colors
from network_objects.network_connection import NetworkConnection


class NetworkItem(QtWidgets.QGraphicsRectItem):
    # represents a Switch or a HCA on the screen
    # also has a lot of parameters to memory chain into other network items
    # useful for adding new features in the future
    def __init__(self, x, y, w, h, max_ports, nid, index, layer_id, node_type: NetworkItemTypes, name):
        super().__init__(x, y, w, h)
        self.wid = None
        self.isMarked = False
        self.portLabelRadius = 20
        self.internal_object = None
        self.ports_list = []
        self.all_ports_x_coordinates = []
        self.all_ports_table = {}
        self.isPortConnected = []
        self.topPortLabels = []
        self.bottomPortLabels = []
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.name = name
        self.node_type = node_type
        self.offset = 10
        self.all_ports_y_coordinate_bottom = self.y + self.h
        self.all_ports_y_coordinate_top = self.y
        self.max_ports = max_ports
        self.layer_id = layer_id
        self.guid = nid
        self.index = index
        self.txt_offset = (self.h/2) + 6
        self.portLabelOffset = 10
        self.font = QtGui.QFont()
        self.defaultColor = colors[node_type]["default"]
        self.setBrush(self.defaultColor)
        self.setAcceptHoverEvents(True)
        self.setFlag(QtWidgets.QGraphicsItem.ItemIsSelectable)
        self.calculate_port_coordinates()
        self.create_port_labels()
        self.topLabelsDrawn = False
        self.bottomLabelsDrawn = False
        self.all_hcas = []
        self.hca_objects = []
        self.hca_connections = []
        self.label = None
        for i in range(0, self.max_ports):
            self.ports_list.append(None)

    # set a reference to the parent widget
    def set_widget(self, wid):
        self.wid = wid

    # ports dictionary, populate with nodes
    # if a port is not connected = Null
    def populate_port_dictionary(self):
        for port in self.max_ports:
            self.all_ports_table[str(port.PortGUID)]

    # lables for ports, used to show port numbers
    def create_port_labels(self):
        l = len(self.all_ports_x_coordinates)-1
        if l == 0:
            self.topPortLabels.append(
                CQCircleLabel((self.x + (self.w/2) - 10),
                              self.all_ports_y_coordinate_top - self.portLabelOffset,
                              self.portLabelRadius, "1"))
        for i in range(0, l):
            self.topPortLabels.append(
                CQCircleLabel(self.all_ports_x_coordinates[i] - self.portLabelOffset, self.all_ports_y_coordinate_top - self.portLabelOffset,
                              self.portLabelRadius, str(i+1)))
            self.bottomPortLabels.append(
                CQCircleLabel(self.all_ports_x_coordinates[i] - 10, self.all_ports_y_coordinate_bottom - 15,
                              self.portLabelRadius, str(i+1)))

    # port coordinates for each port
    # basic math / algebra
    def calculate_port_coordinates(self):
        dist = (self.w - (2 * self.offset)) // self.max_ports
        for i in range(int(self.x + self.offset), int(self.x + self.w - self.offset), dist):
            # self.all_ports_x_coordinates.insert(0, i)
            self.all_ports_x_coordinates.append(i)
            self.isPortConnected.append(False)
        if len(self.all_ports_x_coordinates) < 2:
            self.all_ports_x_coordinates[0] = (self.x + (self.w/2))

    # redraw the whole object
    # recalculate port coords and stuff
    def redraw(self, x, y, w, h):
        self.x = int(x)
        self.y = int(y)
        self.w = int(w)
        self.h = int(h)
        self.all_ports_x_coordinates = []
        self.calculate_port_coordinates()
        self.topPortLabels = []
        self.bottomPortLabels = []
        self.create_port_labels()
        self.setRect(x, y, w, h)

    def add_connection(self, conn: NetworkConnection):
        self.ports_list.append(conn)

    # custom painting for text
    def paint(self, painter: QtGui.QPainter, option: 'QStyleOptionGraphicsItem',
              widget: typing.Optional[QtWidgets.QWidget] = ...) -> None:
        super(NetworkItem, self).paint(painter, option, widget)
        self.font.setPixelSize(15)
        painter.setFont(self.font)
        painter.drawText(self.x + self.offset, self.y + self.txt_offset, self.name)
        # i = 0
        # for x in self.all_ports_x_coordinates:
        #     painter.drawText(x, self.y+90, str(i))
        #     i += 1

    # some helper methods to mark the nodes
    def annotate(self, txt):
        self.label = QtWidgets.QLabel(txt)
        self.label.setStyleSheet('font-size: 200px; color: #FFFFFF; background-color: rgba(0, 0, 0, 0)')
        self.label.setGeometry(QtCore.QRect(self.x, self.y + self.h + 10, self.w + 1000, self.h + 100))
        if self.scene() is not None:
            self.scene().addWidget(self.label)
            return self.label, True
        else:
            return self.label, False

    # add connections to the global connections object
    def append_connection(self, idx):
        self.all_connections[idx] = True

    def get_all_connections(self):
        return list(self.all_connections.keys())

    def mark(self):
        self.isMarked = True
        self.setBrush(colors["general"]["mark"])
        self.update()

    def custom_mark(self, color):
        self.isMarked = True
        self.setBrush(color)
        self.update()

    def unmark(self):
        self.isMarked = False
        self.set_unselected()

    # overriding event methods to mark / highlight on user interaction
    def hoverEnterEvent(self, event: 'QGraphicsSceneHoverEvent') -> None:
        if not self.isSelected() and not self.isMarked:
            self.setBrush(colors[self.node_type]["hover"])
            self.update()

    def hoverLeaveEvent(self, event: 'QGraphicsSceneHoverEvent') -> None:
        if not self.isSelected() and not self.isMarked:
            self.setBrush(self.defaultColor)
            self.update()

    def set_selected(self):
        self.setBrush(colors[self.node_type]["selected"])
        self.update()

    def set_unselected(self):
        self.setBrush(colors[self.node_type]["default"])
        self.update()
