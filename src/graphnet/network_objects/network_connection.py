import math

from PyQt5 import QtWidgets, QtGui
import typing
from static.colors import colors
from custom_widgets.widgets import LinePen, ArrowOverlay
from static.network_item_types import NetworkItemTypes

# Qt object for a 'network' link


class NetworkConnection(QtWidgets.QGraphicsLineItem):
    # custom QT object created from a QGraphicsLineItem
    # A LOT of parameters / vars to store a link's state
    # takes extra memory but would be easy to manipulate the state - color location, etc.
    # and also memory chain into other network objects, through the source and destination objects
    def __init__(self, n1, n2, port1, port2, cid, color=QtGui.QColor(127, 127, 127)):
        self.y1 = n1.all_ports_y_coordinate_bottom
        self.y2 = n2.all_ports_y_coordinate_top
        self.x1 = n1.all_ports_x_coordinates[port1]
        self.connect_id = cid
        self.x2 = n2.all_ports_x_coordinates[port2]
        self.slope = (self.y2 - self.y1)/(self.x2 - self.x1)
        self.x_mid = (self.x1 + self.x2)/2
        self.y_mid = (self.y1 + self.y2)/2
        self.source = n1
        self.source_port = port1
        self.destination = n2
        self.destination_port = port2
        self.source.isPortConnected[self.source_port] = True
        self.destination.isPortConnected[self.destination_port] = True
        super().__init__(self.x1, self.y1, self.x2, self.y2)
        self.color = color
        self.setPen(LinePen(stroke=2, dash_pattern=False, color=color))
        self.setFlag(QtWidgets.QGraphicsItem.ItemIsSelectable)
        self.setAcceptHoverEvents(True)
        self.sourcePortLabel = self.source.bottomPortLabels[self.source_port]
        self.destinationPortLabel = self.destination.topPortLabels[self.destination_port]
        self.defaultStroke = 2
        self.extraBoldStroke = 20
        self.selectedStroke = 6
        self.hoverStroke = 11
        self.hoverSelectedStroke = 10
        self.isHighlight = False
        self.isMarked = False
        self.arrow = None
        self.intercept = 0
        self.line_angle = (math.atan(self.slope))/math.pi * 180
        self.get_intercept()

    # generate up or down arrows, if called
    # used for routing
    # created on demand instead of at object creation
    def generate_arrows(self, isUp, simple=False):
        self.arrow = ArrowOverlay(self, isUp, simple=simple)
        return self.arrow

    # adds port labels
    # since they can be on either sides (up or down) they are created dynamically
    def add_all_labels(self, wid):
        if self.destination.node_type == NetworkItemTypes.HCA:
            wid.add_network_item(self.sourcePortLabel)
            return
        if not self.source.bottomLabelsDrawn:
            self.source.bottomLabelsDrawn = True
            wid.add_network_items(self.source.bottomPortLabels)
        if self.destination.node_type == NetworkItemTypes.LEAF_SWITCH:
            wid.add_network_item(self.destinationPortLabel)
            return
        if not self.destination.topLabelsDrawn:
            self.destination.topLabelsDrawn = True
            wid.add_network_items(self.destination.topPortLabels)

    # custom painter to avoid QT's default dotted line selection
    def paint(self, painter: QtGui.QPainter, option: 'QStyleOptionGraphicsItem', widget: typing.Optional[QtWidgets.QWidget] = ...) -> None:
        sog = QtWidgets.QStyleOptionGraphicsItem(option)
        sog.state &= QtWidgets.QStyle.State_None
        super(NetworkConnection, self).paint(painter, sog, widget)

    # different highlight methods
    def mark(self):
        self.isMarked = True
        self.setPen(LinePen(stroke=self.extraBoldStroke, dash_pattern=False,
                            color=colors["general"]["mark"]))
        self.sourcePortLabel.mark()
        self.destinationPortLabel.mark()

    def unmark(self):
        self.isMarked = False
        self.set_unselected()

    def highlight(self):
        self.setPen(LinePen(stroke=self.selectedStroke, dash_pattern=False,
                            color=colors[NetworkItemTypes.LINK]["highlight"]))
        self.sourcePortLabel.highlight()
        self.destinationPortLabel.highlight()
        self.isHighlight = True

    def unhighlight(self):
        self.set_unselected()

    def set_selected(self):
        self.setPen(LinePen(stroke=self.selectedStroke, dash_pattern=False, color=colors[NetworkItemTypes.LINK]["selected"]))
        self.sourcePortLabel.highlight()
        self.destinationPortLabel.highlight()
        self.isHighlight = True

    def set_unselected(self):
        self.setPen(LinePen(stroke=self.defaultStroke, dash_pattern=False, color=colors[NetworkItemTypes.LINK]["default"]))
        self.sourcePortLabel.unhighlight()
        self.destinationPortLabel.unhighlight()
        self.isHighlight = False

    def recolor_link(self, color, stroke=2, dash=False):
        self.setPen(LinePen(stroke=stroke, dash_pattern=dash, color=color))
        self.update()

    # custom event methods to highlight or indicate selections, etc
    def hoverEnterEvent(self, event: 'QGraphicsSceneHoverEvent') -> None:
        stroke = self.hoverStroke
        if self.isHighlight:
            stroke = self.hoverSelectedStroke
        self.setPen(LinePen(stroke=stroke, dash_pattern=False, color=self.pen().color()))
        self.update()

    def hoverLeaveEvent(self, event: 'QGraphicsSceneHoverEvent') -> None:
        w = self.defaultStroke
        if self.isMarked:
            w = self.extraBoldStroke
        elif self.isHighlight:
            w = self.selectedStroke
        if self.isSelected():
            w = self.hoverSelectedStroke
        self.setPen(LinePen(stroke=w, dash_pattern=False, color=self.pen().color()))
        self.update()

    # math methods used for algebra calcualtion of position, if needed to place items on the line, etc
    def get_intercept(self):
        self.intercept = self.y_mid - (self.slope * self.x_mid)

    def get_y_with_intercept(self, x, c):
        return (self.slope * x) + self.intercept + c

    def get_y_coordinate_with_x(self, x):
        return self.y1 + (self.slope * (x - self.x1))

    def get_x_coordinate_with_y(self, y):
        return ((y - self.y1) / self.slope) + self.x1

