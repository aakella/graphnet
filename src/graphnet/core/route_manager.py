import pickle

from static import local_paths
from core.de_pickler import RouteFinder


class RouteManager:
    # route manager, manages routing
    # outer wrapper for RouteFinder with extra functionality
    # has status Flags for extra information
    # NEED TO FIX - inefficient - each manager stores a network and routing table reference
    def __init__(self, reloadFiles=True):
        self.sourceListening = False
        self.destinationListening = False
        self.routingInProgress = False
        self.routingDone = False
        self.marked = []
        self.proxyLabels = []
        self.routeList = []
        self.simpleRouteList = []
        self.source = None
        self.destination = None
        self.network = None
        self.routingTables = None
        self.reloadFiles = reloadFiles
        self.conflict_object = None

        if self.reloadFiles:
            self.load_network()
            self.load_routing_tables()

        self.routeFinder = None
        self.resetState = False
        self.goingUp = True

    # setters and getters for some internal params
    def setSource(self, src):
        self.source = src.internal_object

    def setDestination(self, dst):
        self.destination = dst.internal_object

    def set_network(self, net):
        self.network = net

    def set_routing_tables(self, tab):
        self.routingTables = tab

    def load_network(self):
        with open(local_paths.network_path, "rb") as filehandle:
            self.network = pickle.load(filehandle)

    def load_routing_tables(self):
        with open(local_paths.routing_tables_path, "rb") as filehandle:
            self.routingTables = pickle.load(filehandle)

    # reset route , but keep source and destination objects
    def reset(self):
        self.sourceListening = False
        self.destinationListening = False
        self.routingInProgress = False
        self.routingDone = False
        self.source = None
        self.destination = None
        self.marked = []
        self.proxyLabels = []
        self.routeList = []
        self.simpleRouteList = []

    # start routing and set flags
    def start(self):
        self.routingInProgress = True
        self.routingDone = False
        self.routeFinder = RouteFinder(self.source, self.destination, self.network, self.routingTables)
        self.simpleRouteList = []
        self.routeList = []
        self.routeFinder.find_route()
        self.routingDone = True
        self.routingInProgress = False

    # convert a route list to a simple list with only port and GUID
    def set_simple_route_list(self):
        self.routeList = self.routeFinder.network_usage_list
        # print("Source: " + str(self.source.ports[0].PortGUID))
        for entry in self.routeList:
            self.simpleRouteList.append((entry.switch_GUID, entry.switch_port_no))
        # for entry in self.simpleRouteList:
        #     print(entry)
        # print("Destination: " + str(self.destination.ports[0].PortGUID))

