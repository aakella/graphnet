import pickle
import sys

from static import local_paths
from core.route_manager import RouteManager
sys.path.append('internal_networking')


class ConflictsManager:
    # as the name suggests, it takes care of conflicts and how thyere rendered
    # takes a conflict and creates a new routermanager for each
    # the routermanager is used to draw routes on the screen
    def __init__(self):
        self.conflicts = []
        self.routes = []
        self.node_dict = {}
        self.network = []
        self.routingTables = []
        self.load_network()
        self.load_routing_tables()
        self.load_conflicts()

    def load_node_dict(self, dct):
        self.node_dict = dct

    def load_conflicts(self):
        with open(local_paths.conflicts_path, "rb") as filehandle:
            conflicts = pickle.load(filehandle)
            if not conflicts:
                self.conflicts = []
                return
            self.conflicts = conflicts

    def load_network(self):
        with open(local_paths.network_path, "rb") as filehandle:
            self.network = pickle.load(filehandle)

    def load_routing_tables(self):
        with open(local_paths.routing_tables_path, "rb") as filehandle:
            self.routingTables = pickle.load(filehandle)

    def create_routes(self):
        if self.node_dict is None:
            return
        for conflict in self.conflicts:
            tmp = []
            for route in conflict.conflictingRoutes:
                src = self.node_dict.get(route[0])
                dst = self.node_dict.get(route[1])
                route_manager = RouteManager(False)
                route_manager.set_network(self.network)
                route_manager.set_routing_tables(self.routingTables)
                route_manager.setSource(src)
                route_manager.setDestination(dst)
                route_manager.start()
                route_manager.set_simple_route_list()
                route_manager.conflict_object = conflict
                tmp.append(route_manager)
            self.routes.append(tmp)

    def print_conflicts(self):
        print(self.conflicts)
        print(self.routes)
