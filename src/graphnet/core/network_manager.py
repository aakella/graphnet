import random

from PyQt5 import QtCore

from internal_networking.Hca import Hca
from internal_networking.Switch import Switch
from network_objects.network_connection import NetworkConnection
from network_objects.network_item import NetworkItem
from static.network_item_types import NetworkItemTypes
from network_objects.network_link import NetworkLink

item_type = {
    NetworkItemTypes.ROOT_SWITCH: "R_SW",
    NetworkItemTypes.LEAF_SWITCH: "L_SW",
    NetworkItemTypes.SWITCH: "SW",
    NetworkItemTypes.HCA: "HCA",
    NetworkItemTypes.LINK: None,
    NetworkItemTypes.NONE: None
}


class NetworkManager(QtCore.QObject):
    # brains of the app, uses math and algebra to calculate coordinates for each switch of the network
    # also has a lot of copies and lists of network objects for easy access and feature additions

    resizeScene = QtCore.pyqtSignal()

    def __init__(self):
        # set some defaults in the init method
        super(NetworkManager, self).__init__()
        self.connections_list = []
        self.layer_mat = []
        self.connections = {}
        self.depth = 0
        self.node_count = 0
        self.next_y = 1000
        self.max_width = 100
        self.max_height = 100
        self.node_dict = {}
        self.leng = 1000
        self.height = 50
        self.incr = self.leng + 100
        self.hca_length = 300
        self.hca_height = 50
        self.hca_x = 0
        self.hca_y = 2000
        self.current_idx = 0
        self.mainWidget = None
        self.network = None
        self.hca_table = {}
        self.hca_to_layer = {}
        self.hca_connections = []
        self.left_extreme = 0
        self.top_extreme = 0

    def set_network(self, network):
        self.network = network
        self.generate_hca_table()

    # generate hca table for all hcas in the network
    # helps reduce search time
    # wihtout a hashtable each HCA search would be a loop of ~300 iterations
    def generate_hca_table(self):
        if self.network is None:
            return
        # hash table for the HCAs - for easy access
        for switch in self.network.leaf_switches:
            self.hca_table[switch.NodeGUID] = []
            for hca in self.network.hcas:
                if hca.ports[0].NeighborGUID == switch.NodeGUID:
                    self.hca_table[switch.NodeGUID].append(hca)

    # helper functions
    def set_widget(self, wid):
        self.mainWidget = wid

    def add_empty_layer(self, node_type):
        self.layer_mat.append([])
        self.current_idx += 1
        return self.current_idx-1

    # functions that generates GUIDs for items without a set GUID
    def generate_guids(self, leng):
        l = []
        for i in range(0, leng):
            gen = random.randint(100000, 200000)
            while gen in self.node_dict:
                gen = random.randint(100000, 200000)
            self.node_dict[gen] = 0
            l.append(gen)
        return l

    def generate_guid(self):
        gen = random.randint(100000, 200000)
        while gen in self.node_dict:
            gen = random.randint(100000, 200000)
        return gen

    # get object type
    def get_type(self, obj):
        if isinstance(obj, Switch):
            if obj.is_root_switch():
                return NetworkItemTypes.ROOT_SWITCH
            elif not obj.is_root_switch():
                return NetworkItemTypes.LEAF_SWITCH
            else:
                return NetworkItemTypes.SWITCH
        elif isinstance(obj, Hca):
            return NetworkItemTypes.HCA
        else:
            return NetworkItemTypes.NONE

    # generate connections with a simple loop
    # maps the internal node objects to a NetworkItem / connection
    def generate_connections(self, idx1, idx2):
        for i in range(0, len(self.layer_mat[idx1])):
            for j in range(0, len(self.layer_mat[idx1][i].internal_object.ports)):
                self.add_connection(self.layer_mat[idx1][i].internal_object.NodeGUID,
                                    self.layer_mat[idx1][i].internal_object.ports[j].NeighborGUID,
                                    self.layer_mat[idx1][i].internal_object.ports[j].PortNo - 1, self.layer_mat[idx1][i].internal_object.ports[j].NeighborPortNo - 1,
                                    self.connections_list)

    # add a layer from a list of Node objects
    # calculates coordinates and assigns links, GUID, names, etc.
    def add_layer_from_list(self, network_list):
        num = len(network_list)
        if num <= 0:
            return
        node_type = self.get_type(network_list[0])
        ports = network_list[0].max_ports
        guid_list = []
        is_root = False
        for item in network_list:
            if isinstance(item, Switch):
                guid_list.insert(0, item.NodeGUID)
            else:
                guid_list.append(self.generate_guid())
        l_id = self.add_layer(num, ports, node_type, guid_list)
        for i in range(0, len(self.layer_mat[l_id])):
            self.layer_mat[l_id][i].internal_object = network_list[i]

        if node_type == NetworkItemTypes.LEAF_SWITCH:
            for node in network_list:
                self.set_hcas(self.hca_table.get(node.NodeGUID), self.node_dict[node.NodeGUID])

    # add layer, with extra data, without a list
    def add_layer(self, num, ports, node_type: NetworkItemTypes, guid_list):
        lyr = []
        x, y = 300, self.next_y
        self.next_y += self.incr*4
        for i in range(0, num):
            # temp_node = NetworkItem(x, y, radius, gen, i, self.depth)
            if y < self.left_extreme:
                y = self.left_extreme
            temp_node = NetworkItem(x, y, self.leng, self.height, ports, guid_list[i], i, self.depth, node_type,
                                    item_type[node_type] + "-" + str(guid_list[i]))
            temp_node.set_widget(self.mainWidget)
            lyr.append(temp_node)
            self.node_dict[guid_list[i]] = temp_node
            self.node_count += 1
            x += self.incr
        self.layer_mat.append(lyr)
        self.depth += 1
        if y > self.max_height:
            self.max_height += self.height + 100
            self.resizeScene.emit()
        if x > self.max_width:
            self.max_width = x - self.leng + 100
            self.resizeScene.emit()
        return self.depth-1

    # set HCAs
    # since HCAs are extra layers which only correspond to one of the leaf switches
    # they need to be drawn and stored separately
    def set_hcas(self, hca_list, currentnode):
        # self.hca_x = currentnode.x + (self.hca_length / 2)
        self.hca_x = (currentnode.x + (self.leng / 2)) - (self.hca_length / 2 * len(hca_list))
        self.hca_y = currentnode.y + 2000
        currentnode.all_hcas = hca_list
        i = 0
        lyr = []
        for hca in reversed(hca_list):
            thca = NetworkItem(self.hca_x, self.hca_y, self.hca_length, self.hca_height,
                               hca.max_ports, hca.ports[0].PortGUID, i, currentnode.layer_id + 1, NetworkItemTypes.HCA,
                               "HCA-" + str(hca.ports[0].PortGUID))
            self.node_dict[hca.ports[0].PortGUID] = thca
            thca.internal_object = hca
            conn = self.add_connection(currentnode.guid, thca.guid, i, 0, self.hca_connections)
            thca.hca_connections.append(conn)
            currentnode.hca_connections.append(conn)
            currentnode.ports_list[hca.ports[0].NeighborPortNo] = conn
            # self.hca_conns.insert(0, conn)
            i += 1
            self.hca_x += self.hca_length + 100
            lyr.append(thca)
        self.hca_to_layer[currentnode.guid] = len(self.layer_mat)
        self.layer_mat.append(lyr)

    # calculate offset for a layer and recenter the layer
    # to make sure it looks like a tree, instead of a set of stairs
    # ----
    # -------
    # CONVERT THIS ^ into
    #    ----
    # ----------
    def re_calculate(self, id1, id2):
        center2 = (self.layer_mat[id2][0].x + self.layer_mat[id2][len(self.layer_mat[id2])-1].x)/2
        center1 = (self.layer_mat[id1][0].x + self.layer_mat[id1][len(self.layer_mat[id1])-1].x)/2
        offset = center2 - center1
        x = self.layer_mat[id1][0].x
        y = self.layer_mat[id1][0].y
        for node in self.layer_mat[id1]:
            node.redraw((x+offset), y, self.leng, self.height)
            x += self.incr

    # add layer
    def get_layer(self, lid):
        return self.layer_mat[lid]

    # add a connection object to the list
    # generate IDs because connections dont have GUIDs
    def add_connection(self, id1, id2, port1, port2, connections_list):
        connect_id = random.randint(1000000, 2000000)
        while connect_id in self.connections:
            connect_id = random.randint(1000000, 2000000)
        no1, no2 = None, None
        if id1 in self.node_dict and id2 in self.node_dict:
            no1 = self.node_dict[id1]
            no2 = self.node_dict[id2]
        else:
            return False
        temp_con = NetworkConnection(no1, no2, port1, port2, str(connect_id))
        temp_con.add_all_labels(self.mainWidget)
        self.connections[connect_id] = temp_con
        temp_con.source.ports_list[port1] = temp_con
        temp_con.destination.ports_list[port2] = temp_con
        temp_con.setZValue(-1)
        connections_list.append(temp_con)
        return temp_con

    # port labels
    def get_port_labels(self):
        labels = []
        for link in self.connections_list:
            labels.append(link.topSourcePortLabPortLabel)
            labels.append(link.bottomSourcePortLabel)
        return labels

    def add_link(self, id1, id2):
        connect_id = str(id1) + "." + str(id2)
        no1, no2 = None
        if id1 in self.node_dict and id2 in self.node_dict:
            no1 = self.node_dict[id1]
            no2 = self.node_dict[id2]
        else:
            return False

        if connect_id not in self.connections:
            self.connections[connect_id] = NetworkLink(no1.x, no1.y, no2.x, no2.y)
            no1.append_connection(connect_id)
            no2.append_connection(connect_id)
        return True

    # OLD link generator NOT USED ANYMORE
    def add_link(self, l1, i1, l2, i2):

        no1, no2 = None, None
        if l1 < len(self.layer_mat) and l2 < len(self.layer_mat) and i1 < len(self.layer_mat[l1]) and i2 < len(self.layer_mat[l2]):
            no1 = self.layer_mat[l1][i1]
            no2 = self.layer_mat[l2][i2]
        else:
            return False

        connect_id = str(no1.nid) + "." + str(no2.nid)
        no1_off = no1.r/2
        no2_off = no2.r/2
        self.connections[connect_id] = NetworkLink(no1.x + no1_off, no1.y + no1_off, no2.x + no2_off, no2.y + no2_off)
        no1.append_connection(connect_id)
        no2.append_connection(connect_id)
        return True

    # OLD STUFF
    def get_connections(self):
        con = []
        for key in self.connections.keys():
            con.append(self.connections[key])
        return con

    def get_connection(self, conn_id):
        if conn_id in self.connections:
            return self.connections[conn_id]
        else:
            return -1