import pickle

from static import local_paths
from internal_networking.FatTreeAlg import FatTreeAlg
from internal_networking.Hca import *
from internal_networking.RoutingTable import RoutingTable
from internal_networking.Switch import *
from internal_networking.TrafficEntry import TrafficEntry, Function


class UnPickleNetwork:
    # uses some pickling and logic to create internal data strucutres (lists)
    # that help the network manager calculate and place objects on screen
    def __init__(self):
        self.all_switches = {}
        self.all_switches_list = []
        self.root_switches = []
        self.leaf_switches = []
        self.hcas = []
        self.network = None
        with open(local_paths.network_path, "rb") as filehandle:
            network = pickle.load(filehandle)
            self.network = network

        for item in network:
            if isinstance(item, Switch):
                self.all_switches[str(item.NodeGUID)] = item
                self.all_switches_list.append(item)
            if isinstance(item, Hca):
                self.hcas.append(item)

        for item in self.all_switches_list:
            self.root_switches.append(item) if item.is_root_switch() else self.leaf_switches.append(item)
        self.print_net()

    def print_net(self):
        print(str(len(self.root_switches)) + " Root Switches")
        print(str(len(self.leaf_switches)) + " Leaf Switches")
        print(str(len(self.hcas)) + " HCAs")
        print(str(len(self.network)) + " Nodes")


class RouteFinder:
    # route finder class that finds a route between a source and a destination
    # it stores the route until its destroyed
    # it also finds routes based on FatTreeAlg from the internal_networking module
    def __init__(self, source_node, dest_node, network, routing_tables):
        self.source_node = source_node
        self.dest_node = dest_node
        self.sourceGUID = None
        self.destinationGUID = None
        self.network = network
        self.routing_tables = routing_tables
        self.network_usage_list = []

    # main routing method recursively finds a route with a while loop
    # stops when it reaches the destination
    def find_route(self):
        source_GUID = self.source_node.ports[0].PortGUID
        self.sourceGUID = source_GUID
        current_switch = FatTreeAlg.find_node_by_GUID(self.source_node.ports[0].NeighborGUID, self.network)
        dest_GUID = self.dest_node.ports[0].PortGUID
        self.destinationGUID = dest_GUID

        while True:
            current_switch_lft_entry = RoutingTable.find_lft_dest_entry(self.routing_tables, current_switch.NodeGUID,
                                                                        dest_GUID)

            hop_remote_GUID = current_switch_lft_entry.next_hop_GUID
            if (hop_remote_GUID == dest_GUID):
                break
            else:
                hop_port_no_current_switch = current_switch_lft_entry.local_hop_port_no
                hop_port_no_remote_switch = current_switch_lft_entry.remote_hop_port_no
                # current_switch_GUID=
                local_entry = TrafficEntry(source_GUID=source_GUID, destination_GUID=dest_GUID,
                                           switch_GUID=current_switch.NodeGUID,
                                           switch_port_no=hop_port_no_current_switch,
                                           function=Function.SENDING)
                remote_entry = TrafficEntry(source_GUID=source_GUID, destination_GUID=dest_GUID,
                                            switch_GUID=hop_remote_GUID, switch_port_no=hop_port_no_remote_switch,
                                            function=Function.RECEIVING)

                current_switch = FatTreeAlg.find_node_by_GUID(hop_remote_GUID, self.network)

                self.network_usage_list.append(local_entry)
                self.network_usage_list.append(remote_entry)
