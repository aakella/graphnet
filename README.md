
# GraphNet
_A Network Visualisation GUI for the LHCb DAQ (EventBuilder) Network_

## About

The supercomputer that is currently constructed at the LHCb will allegedly be 
the largest real-time data acquisition system in the world in 2021 and operates at very high speeds (up to 32 Tb/s). 
To avoid losing critical physics data through the network and to ascertain its high-speed real-time operation, 
the network has to be monitored carefully.

This repository contains code for a GUI application that can visualize the DAQ network, the routing configuration in the switches, 
and conflicts with relevant details in the network for a given input traffic pattern. 
The end product will be used to monitor the high-speed network in real-time.


## Google Summer of Code



This project was made as a part of Google Summer of Code 2021, under CERN-HSF. <br>

![img.png](src/graphnet/assets/images/readme/gsoc_logo.png) <br>

#### Google Project Page - [click here](https://summerofcode.withgoogle.com/projects/#5684154231947264)

![img.png](src/graphnet/assets/images/readme/cern_logo.png)
![img.png](src/graphnet/assets/images/readme/lhcb_logo.png)

#### CERN-HSF Project Description - [click here](https://hepsoftwarefoundation.org/gsoc/2021/proposal_LHCbVisualizer.html)

## Run the Project!

To run the project locally, read the instructions below. <br>
This project was built using Python 3.9

### Packages

You need the following packages (and their dependencies) to run GraphNet

* PyQT5
  ```sh
  pip install PyQt5
  ```
* Pickle
  ```sh
  pip install pickle-mixin
  ```
### Built With

[PyQT5](https://pypi.org/project/PyQt5/) <br>
(additional libraries: [pickle](https://pypi.org/project/pickle5/) used for storing objects in files)

### Installation

1. clone this project (git)
   ```sh
   git clone https://gitlab.cern.ch/aakella/graphnet.git
   ```
That's it! <br>
To run the project, follow the Run instructions below.


## Running GraphNet

### The GraphNet GUI

The main GUI can be launched with no effort, by just running the `main.py` file.
(src/graphnet/main.py)

```sh
python main.py
```
Running the above command will open the GraphNet Launcher: 
![img.png](src/graphnet/assets/images/readme/graphnet_ss_launcher.png)


This repo/project already contains test network files. <br> Launching the main 
window (Launch GraphNet) will work without any errors.

Custom network files can also be used. To do so, click on select network files. <br>
Doing so will reveal textboxes, where required file paths can be entered.
![img.png](src/graphnet/assets/images/readme/graphnet_ss_setfiles.png)

By default, the boxes are filled with test file paths. <br>
Click on `Save and Launch` to run GraphNet with the new, custom files.

### The fat_tree_routing script
GraphNet does not do any of the routing, conflict detection, etc. 
It only takes in pre-compiled object files that holds network data. <br>
It then uses a different set of algorithms to visualise the network and perform operations
on it.

To compile object files (.pickle) that can be used with GraphNet, the included internal_networking 
module can be used. <br>
This module is a fork of [Rafal Dominik Krawczyk's fat_tree_routing project](https://gitlab.cern.ch/rkrawczy/fat_tree_routing), 
with a few edits (to dump pickle files and add some data)

To generate new network files, run ParamParser.py. This can be done by running it as a module:
```sh
py -m internal_networking.ParamParser
```
The required parameters are available on the main project's page.

To generate 'fresh' files and use them for GraphNet, run the following command:
It uses input files that are included in this repo. (Generates an error-free complete network)
```shell
py -m internal_networking.ParamParser -v -nit "i" -ni "./assets/internal_network_files/topo/new_lhcb/ibnetdiscover.txt" -gl "./assets/internal_network_files/routing/new_lhcb/guid2lid" -t -rl "./assets/internal_network_files/routing/new_lhcb/roots.txt" -so "./assets/internal_network_files/routing/new_lhcb/opensm-ftree-ca-order.dump" -r
```

To generate a file with a network that has missing links use this command:

```shell
py -m internal_networking.ParamParser -v -nit "i" -ni "./assets/internal_network_files/topo/new_lhcb/incomplete/ibnetdiscover.txt" -gl "./assets/internal_network_files/routing/new_lhcb/guid2lid" -t -rl "./assets/internal_network_files/routing/new_lhcb/roots.txt" -so "./assets/internal_network_files/routing/new_lhcb/opensm-ftree-ca-order.dump" -r
```
This can be used to test the conflicts tab of the GUI, which otherwise would display an empty list if the input is error free

## Working

After launching the main window, this screen shows up.
![img.png](src/graphnet/assets/images/readme/graphnet_main.png)
* The toolbar helps with navigation
* The view-switcher tabs open different windows that can be used to render 
  routing algorithms and conflict visualisations
* The box at the top right shows info about the current selected switch, node, link, HCA, etc.
    




