import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="graphnet",
    version="1.4.0",
    author="Anurag Akella, Rafal Dominik Krawczyk",
    author_email="aakella@cern.ch",
    description="GraphNet GUI for visualising the LHCb DAQ Event Builder Network",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.cern.ch/aakella/graphnet",
    project_urls={
        "Bug Tracker": "https://gitlab.cern.ch/aakella/graphnet/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
)